﻿using Identity.DbContexts;
using Identity.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Identity.Managers
{
    public class ApplicationGroupManager
    {
        private ApplicationGroupStore _groupStore;
        private ApplicationGroupDbContext _dbcontext;
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<IdentityRole> _roleManager;

        public ApplicationGroupManager(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, ApplicationGroupStore groupStore, ApplicationGroupDbContext dbcontext)
        {
            _groupStore = groupStore;
            _dbcontext = dbcontext;
            _userManager = userManager;
            _roleManager = roleManager;
        }


        public IQueryable<ApplicationGroup> Groups
        {
            get
            {
                return _groupStore.GetAllGroups;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sucursalId"></param>
        /// <param name="applicationUserId"></param>
        /// <param name="applicationGroupId"></param>
        /// <returns></returns>
        public async Task<IdentityResult> CreateGrupoUsuarioSucursalAsync(int sucursalId, string applicationUserId, string applicationGroupId)
        {
            _dbcontext.GroupUserSucursal.Add(
                new GroupUserSucursal()
                {
                    ApplicationUserId = applicationUserId,
                    ApplicationGroupId = applicationGroupId,
                    SucursalId = sucursalId
                }
            );
            await _dbcontext.SaveChangesAsync();
            return IdentityResult.Success;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sucursalId"></param>
        /// <param name="applicationUserId"></param>
        /// <param name="applicationGroupId"></param>
        /// <returns></returns>
        public async Task<IdentityResult> RemoveGrupoUsuarioSucursalAsync(int sucursalId, string applicationUserId, string applicationGroupId)
        {
            GroupUserSucursal groupUserSucursal = await _dbcontext.GroupUserSucursal.FirstOrDefaultAsync( gus => 
                gus.ApplicationUserId == applicationUserId &&
                gus.ApplicationGroupId == applicationGroupId &&
                gus.SucursalId == sucursalId
            );

            if (groupUserSucursal == null)
            {
                throw new Exception("No existe una asignacion para ese usuario, grupo y sucursal");
            }

            _dbcontext.GroupUserSucursal.Remove(groupUserSucursal);

            await _dbcontext.SaveChangesAsync();
            return IdentityResult.Success;
        }

        public async Task<IdentityResult> CreateGroupAsync(string Name, string Descripcion)
        {
            ApplicationGroup grupoEncontrado = await _groupStore.FindByNameAsync(Name);
            bool existeGrupo = grupoEncontrado != null;

            if (existeGrupo)
            {
                throw new Exception("El grupo con ese nombre ya existe");
            }

            ApplicationGroup group = new ApplicationGroup()
            {
                Id = Guid.NewGuid().ToString(),
                ApplicationRoles = new List<ApplicationGroupRole>(),
                ApplicationUsers = new List<ApplicationUserGroup>(),
                Name = Name,
                Description = Descripcion,
            };
            await _groupStore.CreateAsync(group);
            return IdentityResult.Success;
        }

        /// <summary>
        /// Actualiza (Elimina y Agrega) roles a un grupo.
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="rolesNameAdd"></param>
        /// <param name="rolesNameRemove"></param>
        /// <returns>Devuelve el resultado de proceso</returns>
        public async Task<IdentityResult> UpdateGroupRolesAsync(string groupId, IEnumerable<string> rolesNameAdd, IEnumerable<string> rolesNameRemove)
        {

            bool existenRolesEnAmbasColecciones = rolesNameAdd.Any(rna => rolesNameRemove.Where(rnr => rna != null && rnr != null && rna.Trim() == rnr.Trim()).Count() > 0);
            if (existenRolesEnAmbasColecciones)
            {
                throw new Exception("Lo sentimos no se puede eliminar y agregar un rol al mismo tiempo, revise la listas.");
            }

            // Obtener Nombre de Roles Existentes en el grupo
            ApplicationGroup thisGroup = await this.FindByIdAsync(groupId);
            if (thisGroup == null)
            {
                throw new Exception("No se encontró ningun grupo para el id ingresado");
            }

            List<string> roleNames = await _roleManager.Roles
                .Where(r => thisGroup.ApplicationRoles.Any(ar => ar.ApplicationRoleId == r.Id))
                .Select<IdentityRole, string>(r => r.Name)
                .ToListAsync();
            // Agregar sin duplicar roles nuevos
            roleNames.AddRange(rolesNameAdd);

            //EliminarDuplicados
            roleNames = roleNames.Distinct().ToList();

            // Eliminar roles del listado.
            roleNames.RemoveAll(rn =>
                rolesNameRemove.Any(rnr =>
                        rnr != null &&
                        rnr != null &&
                        rnr.Trim() == rn.Trim()
                    )
            );

            // Llamamos al Metodo Ya Probado que realiza la tarea de crear nuevamente todos los permisos.
            return await this.SetGroupRolesAsync(groupId, roleNames.ToArray());

        }

        /// <summary>
        /// Limpia la vinculacion actual de grupos y roles. Luego carga los roles enviados en el array. 
        /// </summary>
        /// <param name="groupId"> Identificador del grupo </param>
        /// <param name="roleNames"> Arreglo de nombres de roles </param>
        /// <returns>Devuelve el resultado de proceso</returns>
        public async Task<IdentityResult> SetGroupRolesAsync(string groupId, params string[] roleNames)
        {
            // Clear all the roles associated with this group:
            ApplicationGroup thisGroup = await this.FindByIdAsync(groupId);
            thisGroup.ApplicationRoles.Clear();
            await _dbcontext.SaveChangesAsync();

            // Add the new roles passed in:
            var newRoles = _roleManager.Roles
                            .Where(r => roleNames.Any(n => n == r.Name));
            foreach (var role in newRoles)
            {
                thisGroup.ApplicationRoles.Add(new ApplicationGroupRole
                {
                    ApplicationGroupId = groupId,
                    ApplicationRoleId = role.Id
                });
            }
            await _dbcontext.SaveChangesAsync();

            // Reset the roles for all affected users:
            foreach (var groupUser in thisGroup.ApplicationUsers)
            {
                await this.RefreshUserGroupRolesAsync(groupUser.ApplicationUserId);
            }
            return IdentityResult.Success;
        }


        public async Task<IdentityResult> SetUserGroupsAsync(
            string userId, params string[] groupIds)
        {
            // Clear current group membership:
            var currentGroups = await this.GetUserGroupsAsync(userId);
            foreach (var group in currentGroups)
            {
                group.ApplicationUsers
                    .Remove(group.ApplicationUsers
                    .FirstOrDefault(gr => gr.ApplicationUserId == userId
                ));
            }
            await _dbcontext.SaveChangesAsync();

            // Add the user to the new groups:
            foreach (string groupId in groupIds)
            {
                var newGroup = await this.FindByIdAsync(groupId);
                newGroup.ApplicationUsers.Add(new ApplicationUserGroup
                {
                    ApplicationUserId = userId,
                    ApplicationGroupId = groupId
                });
            }
            await _dbcontext.SaveChangesAsync();

            await this.RefreshUserGroupRolesAsync(userId);
            return IdentityResult.Success;
        }

        public async Task<IdentityResult> RefreshUserGroupRolesAsync(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ArgumentNullException("User");
            }
            // Remove user from previous roles:
            var oldUserRoles = await _userManager.GetRolesAsync(user);
            if (oldUserRoles.Count > 0)
            {
                await _userManager.RemoveFromRolesAsync(user, oldUserRoles.ToArray());
            }

            // Find the roles this user is entitled to from group membership:
            var newGroupRoles = await this.GetUserGroupRolesAsync(userId);

            // Get the damn role names:
            var allRoles = await _roleManager.Roles.ToListAsync();
            var addTheseRoles = allRoles
                .Where(r => newGroupRoles.Any(gr => gr.ApplicationRoleId == r.Id
            ));
            var roleNames = addTheseRoles.Select(n => n.Name).ToArray();

            // Add the user to the proper roles
            await _userManager.AddToRolesAsync(user, roleNames);

            return IdentityResult.Success;
        }


        public async Task<IdentityResult> DeleteGroupAsync(string groupId)
        {
            var group = await this.FindByIdAsync(groupId);
            if (group == null)
            {
                throw new ArgumentNullException("User");
            }

            var currentGroupMembers = (await this.GetGroupUsersAsync(groupId)).ToList();
            // remove the roles from the group:
            group.ApplicationRoles.Clear();

            // Remove all the users:
            group.ApplicationUsers.Clear();

            // Remove the group itself:
            _dbcontext.ApplicationGroup.Remove(group);

            await _dbcontext.SaveChangesAsync();

            // Reset all the user roles:
            foreach (var user in currentGroupMembers)
            {
                await this.RefreshUserGroupRolesAsync(user.Id);
            }
            return IdentityResult.Success;
        }

        public async Task<IdentityResult> UpdateGroupAsync(ApplicationGroup group)
        {
            await _groupStore.UpdateAsync(group);
            var grupoActualizado = await FindByIdAsync(group.Id);
            foreach (var groupUser in grupoActualizado.ApplicationUsers)
            {
                await this.RefreshUserGroupRolesAsync(groupUser.ApplicationUserId);
            }
            return IdentityResult.Success;
        }

        public async Task<IdentityResult> ClearUserGroupsAsync(string userId)
        {
            return await this.SetUserGroupsAsync(userId, new string[] { });
        }


        public async Task<IEnumerable<ApplicationGroup>> GetUserGroupsAsync(string userId)
        {
            //var result = new List<ApplicationGroup>();

            var Groups = _dbcontext.ApplicationGroup
                .Include(g => g.ApplicationRoles)
                .Include(g => g.ApplicationUsers);

            var userGroups = (from g in Groups
                              where g.ApplicationUsers
                                .Any(u => u.ApplicationUserId == userId)
                              select g).ToListAsync();
            return await userGroups;
        }

        public async Task<IEnumerable<IdentityRole>> GetGroupRolesAsync(string groupId)
        {
            var group = await _dbcontext.ApplicationGroup.Include(g => g.ApplicationRoles).FirstOrDefaultAsync(g => g.Id == groupId);
            var roles = await _roleManager.Roles.ToListAsync();
            var groupRoles = (
                    from r in roles
                    where @group.ApplicationRoles.Any(ap => ap.ApplicationRoleId == r.Id)
                    select r
                ).ToList();
            return groupRoles;
        }

        public async Task<IEnumerable<ApplicationUser>> GetGroupUsersAsync(string groupId)
        {
            var group = await this.FindByIdAsync(groupId);
            var users = new List<ApplicationUser>();
            foreach (var groupUser in group.ApplicationUsers)
            {
                var user = await _userManager.Users
                    .FirstOrDefaultAsync(u => u.Id == groupUser.ApplicationUserId);
                users.Add(user);
            }
            return users;
        }

        public async Task<IEnumerable<ApplicationGroupRole>> GetUserGroupRolesAsync(
            string userId)
        {
            var userGroups = await this.GetUserGroupsAsync(userId);
            var userGroupRoles = new List<ApplicationGroupRole>();
            foreach (var group in userGroups)
            {
                userGroupRoles.AddRange(group.ApplicationRoles.ToArray());
            }
            return userGroupRoles;
        }


        public async Task<ApplicationGroup> FindByIdAsync(string id)
        {
            return await _groupStore.FindByIdAsync(id);
        }

    }
}
