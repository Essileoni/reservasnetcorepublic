﻿using Identity.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Identity.Managers
{
    //public class GroupManager<TGroup> : IDisposable where TGroup : class
    public class ApplicationGroupStore : IDisposable
    {
        //private bool _disposed;
        public DbContext Context { get; private set; }
        public DbSet<ApplicationGroup> DbEntitySet { get; private set; }
        public bool DisposeContext { get; set; } = false;
        
        public ApplicationGroupStore(DbContext context)
        {
            if(context == null)
            {
                throw new ArgumentNullException("Contexto para GroupManager nulo");
            }
            this.DbEntitySet = context.Set<ApplicationGroup>();
            this.Context = context;
        }

        public IQueryable<ApplicationGroup> GetAllGroups
        {
            get
            {
                return this.DbEntitySet;
            }
        }

        public virtual async Task CreateAsync(ApplicationGroup group)
        {
            this.ThrowIfDisposed();
            if (group == null)
            {
                throw new ArgumentNullException("CreateAsync -> group");
            }

            ApplicationGroup grupoOnDB = await this.DbEntitySet.FirstOrDefaultAsync<ApplicationGroup>(g => g.Name == group.Name);
            if(grupoOnDB != null)
            {
                throw new Exception("El grupo " + group.Name + " ya existe");
            }

            this.DbEntitySet.Add(group);
            await this.Context.SaveChangesAsync();
        }

        public virtual async Task DeleteAsync(ApplicationGroup group)
        {
            this.ThrowIfDisposed();
            if (group == null)
            {
                throw new ArgumentNullException("group");
            }
            this.DbEntitySet.Remove(group);
            await this.Context.SaveChangesAsync();
        }

        public Task<ApplicationGroup> FindByIdAsync(string groupId)
        {
            this.ThrowIfDisposed();
            return this.DbEntitySet
                .Include(g => g.ApplicationUsers)
                .Include(g=> g.ApplicationRoles)
                .FirstOrDefaultAsync(g => g.Id == groupId);
        }

        public Task<ApplicationGroup> FindByNameAsync(string groupName)
        {
            this.ThrowIfDisposed();
            return this.DbEntitySet.FirstOrDefaultAsync<ApplicationGroup>(g => g.Name.ToUpper() == groupName.ToUpper());
        }

        public virtual async Task UpdateAsync(ApplicationGroup group)
        {
            this.ThrowIfDisposed();
            if (group == null)
            {
                throw new ArgumentNullException("GroupManager -> UpdateAsync");
            }
            this.DbEntitySet.Update(group);
            await this.Context.SaveChangesAsync();
        }

        // DISPOSE STUFF: ===============================================
        
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void ThrowIfDisposed()
        {
            if (this.DisposeContext == true)
            {
                throw new ObjectDisposedException("GroupManager");
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (this.DisposeContext && disposing && this.Context != null)
            {
                this.Context.Dispose();
            }
            this.DisposeContext = true;
            this.Context = null;
            this.DbEntitySet = null;
        }
    }
}
