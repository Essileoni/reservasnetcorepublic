﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Identity.Entities
{
    public class ApplicationGroupRole
    {
        [Required]
        public string ApplicationGroupId { get; set; }
        [Required]
        public string ApplicationRoleId { get; set; }
    }
}
