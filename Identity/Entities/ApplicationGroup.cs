﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Identity.Entities
{
    public class ApplicationGroup
    {
        [Key]
        public string Id { get; set; }
        [Required]
        [MaxLength(256)]
        public string Name { get; set; }
        [Required]
        [MaxLength(2048)]
        public string Description { get; set; }
        //public bool HayQueValidarSucursal { get; set; } = false;

        public ICollection<ApplicationGroupRole> ApplicationRoles { get; set; }
        public ICollection<ApplicationUserGroup> ApplicationUsers { get; set; }
        public ICollection<GroupUserSucursal> GroupsUsersSucursales { get; set; }
        
    }
}
