﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace Identity.Entities
{
    public class ApplicationUserRole : IdentityUserRole<string>
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public virtual ApplicationUser User { get; set; }
        [Required]
        public virtual IdentityRole Role { get; set; }
        
    }
}