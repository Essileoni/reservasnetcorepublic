﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Identity.Entities
{
    public class GroupUserSucursal
    {
        public string ApplicationGroupId { get; set; }
        public string ApplicationUserId { get; set; }
        public int SucursalId { get; set; }
    }
}
