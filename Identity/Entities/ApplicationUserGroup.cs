﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Identity.Entities
{
    public class ApplicationUserGroup
    {
        // [Key] : No es posible usar anotations para generar claves primarias compuestas por dos campos
        public string ApplicationUserId { get; set; }

        // [Key] : No es posible usar anotations para generar claves primarias compuestas por dos campos
        public string ApplicationGroupId { get; set; }

        //public ApplicationGroup ApplicationGroup { get; set; }
    }
}
