﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Identity.Entities
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [MaxLength(200)]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(250)]
        public string LastName { get; set; }

        /// <summary>
        /// Un usuario puede tener una sucursal predeterminada.
        /// </summary>
        [Required]
        public int SucursalPredeterminadaId { get; set; }

        public ICollection<GroupUserSucursal> GroupsUsersSucursales { get; set; }
        
    }
}
