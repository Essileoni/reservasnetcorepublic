﻿using Identity.DbContexts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Identity.Data
{
    /// <summary>
    /// Permite la migración por comandos de PackageManagerConsole
    /// </summary>
    public class ApplicationGroupDbContextFactory : IDesignTimeDbContextFactory<ApplicationGroupDbContext>
    {
        public ApplicationGroupDbContext CreateDbContext(string[] args)
        {
            var dbContext = new ApplicationGroupDbContext(new DbContextOptionsBuilder<ApplicationGroupDbContext>().UseSqlServer(
               new ConfigurationBuilder()
                   .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), $"appsettings.json"))
                   .Build()
                   .GetConnectionString("AuthDatabaseConnection")
               ).Options);

            dbContext.Database.Migrate();
            return dbContext;
        }
    }
}
