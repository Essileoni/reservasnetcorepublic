﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Identity.Migrations
{
    public partial class TableGroupsRolesUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ApplicationGroup",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: false),
                    Description = table.Column<string>(maxLength: 2048, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationGroup", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationGroupRole",
                columns: table => new
                {
                    ApplicationGroupId = table.Column<string>(nullable: false),
                    ApplicationRoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationGroupRole", x => new { x.ApplicationGroupId, x.ApplicationRoleId });
                    table.ForeignKey(
                        name: "FK_ApplicationGroupRole_ApplicationGroup_ApplicationGroupId",
                        column: x => x.ApplicationGroupId,
                        principalTable: "ApplicationGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ApplicationUserGroup",
                columns: table => new
                {
                    ApplicationUserId = table.Column<string>(nullable: false),
                    ApplicationGroupId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApplicationUserGroup", x => new { x.ApplicationGroupId, x.ApplicationUserId });
                    table.ForeignKey(
                        name: "FK_ApplicationUserGroup_ApplicationGroup_ApplicationGroupId",
                        column: x => x.ApplicationGroupId,
                        principalTable: "ApplicationGroup",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ApplicationGroupRole");

            migrationBuilder.DropTable(
                name: "ApplicationUserGroup");

            migrationBuilder.DropTable(
                name: "ApplicationGroup");
        }
    }
}
