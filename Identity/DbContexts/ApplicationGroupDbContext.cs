﻿using Identity.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Identity.DbContexts
{
    public class ApplicationGroupDbContext : DbContext
    {
        public ApplicationGroupDbContext(DbContextOptions<ApplicationGroupDbContext> options) 
            : base(options)
        {}

        public DbSet<ApplicationGroup> ApplicationGroup { get; set; }
        public DbSet<ApplicationUserGroup> ApplicationUserGroup { get; set; }
        public DbSet<ApplicationGroupRole> ApplicationGroupRole { get; set; }
        public DbSet<GroupUserSucursal> GroupUserSucursal { get; set; }

        /// <summary>
        /// Permite durante la migración crear tablas con 2 claves primarias. 
        /// </summary>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUserGroup>()
                .HasKey(ug => new { ug.ApplicationGroupId, ug.ApplicationUserId });

            modelBuilder.Entity<ApplicationGroupRole>()
                .HasKey(gr => new { gr.ApplicationGroupId, gr.ApplicationRoleId });

            modelBuilder.Entity<GroupUserSucursal>( groupUserSucursal => { 
                groupUserSucursal.HasKey(gus => new { gus.ApplicationGroupId, gus.ApplicationUserId, gus.SucursalId });
            });
        }
    }
}
