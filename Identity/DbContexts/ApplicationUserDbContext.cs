﻿using Identity.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;


namespace Identity.DbContexts
{
    public class ApplicationUserDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationUserDbContext(DbContextOptions<ApplicationUserDbContext> options) 
            : base(options)
        {}

        /// <summary>
        /// Permite durante la migración crear tablas con 2 claves primarias. 
        /// </summary>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<GroupUserSucursal>()
                .HasKey(gr => new { gr.ApplicationGroupId, gr.ApplicationUserId, gr.SucursalId });
        }

    }
}
