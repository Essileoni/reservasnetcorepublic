﻿using System;
using System.Collections.Generic;

namespace Authentication.Api.Models
{
    /// <summary>
    /// Resultado base para todas las consultas
    /// </summary>
    public class BaseResult
    {
        /// <summary>
        /// Mensaje
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Si el resultado devuelve la api es uno correcto o un error.
        /// </summary>
        public bool IsOk { get; set; } = true;

        /// <summary>
        /// Para setear los textos del contenido de una pagina si se especifica el archivo .json
        /// </summary>
        public Dictionary<string, string> InfoContent { get; set; }

        /// <summary>
        /// Injecta una exception. en caso de una, devuelve el mensaje de la misma.
        /// </summary>
        /// <param name="exception"></param>
        public void SetException(Exception exception)
        {
            IsOk = false;
            Message = exception.Message;
        }
    }
}
