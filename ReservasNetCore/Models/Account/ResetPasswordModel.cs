﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasNetCore.Models.Account
{
    public class ResetPasswordModel : LoginModel
    {
        public string Token { get; set; }

    }
}
