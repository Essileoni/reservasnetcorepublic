﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasNetCore.Models.Account
{
    public class RolModel
    {
        public string nombre { get; set; }
        public Boolean activo { get; set; }
    }
}
