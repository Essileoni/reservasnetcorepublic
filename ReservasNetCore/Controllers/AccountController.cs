﻿
//using Common.Extensions;
//using Identity.Entities;
//using Microsoft.AspNetCore.Authorization;
//using Microsoft.AspNetCore.Identity;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.Extensions.Configuration;
//using Microsoft.Extensions.Logging;
//using Microsoft.IdentityModel.Tokens;
//using ReservasNetCore.Extensions;
//using ReservasNetCore.Models.Account;
//using System;
//using System.Collections.Generic;
//using System.IdentityModel.Tokens.Jwt;
//using System.IO;
//using System.Linq;
//using System.Security.Claims;
//using System.Security.Cryptography;
//using System.Text;
//using System.Threading.Tasks;

//namespace ReservasNetCore.Controllers
//{
//    [Produces("application/json")]
//    [Route("api/auth/[controller]")]
//    [ApiController]
//    [AllowAnonymous]
//    //[Authorize]
//    public class AccountController : ControllerBase
//    {
//        private readonly UserManager<ApplicationUser> _userManager;
//        private readonly SignInManager<ApplicationUser> _signInManager;
//        private readonly RoleManager<IdentityRole> _roleManager;
//        private readonly IConfiguration _configuration;
//        private readonly ILogger<AccountController> _logger;

//        private readonly string UserNotFindMsg = "El usuario no existe";
//        private readonly string RolesAreNull = "Los roles son nulos";

//        public AccountController(
//          UserManager<ApplicationUser> userManager,
//          SignInManager<ApplicationUser> signInManager,
//          RoleManager<IdentityRole> roleManager,
//          IConfiguration configuration,
//          ILogger<AccountController> logger
//        )
//        {
//            _userManager = userManager; // check
//            _signInManager = signInManager; //check
//            _roleManager = roleManager;
//            _configuration = configuration; // check
//            _logger = logger;
//        }

//        /// <summary>
//        /// Devuelve un token de autorizacion para un usuario y contraseña validos.
//        /// </summary>
//        /// <param name="username">Nombre de usuario</param>
//        /// <param name="password">Contraseña</param>
//        /// <returns>string</returns>
//        [HttpPost]
//        [Route("Login")]
//        [AllowAnonymous]
//        public async Task<IActionResult> CreateToken([FromBody] LoginModel loginModel)
//        {
//            try
//            {
//                if (ModelState.IsValid)
//                {
//                    var loginResult = await _signInManager.PasswordSignInAsync(loginModel.Username, loginModel.Password, isPersistent: false, lockoutOnFailure: false);

//                    if (!loginResult.Succeeded)
//                    {
//                        return BadRequest("No fué posible loguearse al sistema");
//                    }

//                    var user = await _userManager.FindByNameAsync(loginModel.Username);

//                    if (user == null)
//                    {
//                        throw new Exception(UserNotFindMsg);
//                    }

//                    string token = GetToken(user);
//                    return Ok("bearer " + token);
//                }
//                return BadRequest(ModelState);
//            }
//            catch (Exception e)
//            {
//                return getExcepcionFormateada(e);
//            }


//        }



//        ///// <summary>
//        ///// Verifica si el token de autorización es válido y a la vez genera uno nuevo.
//        ///// </summary>
//        ///// <returns>string</returns>
//        //[HttpPost]
//        //[Route("RefreshToken")] // para testear si el token funciona.
//        ////[Authorize]
//        //[AllowAnonymous]
//        //public async Task<IActionResult> RefreshToken()
//        //{
//        //    try
//        //    {
//        //        var user = await _userManager.FindByNameAsync(
//        //            User.Identity.Name ??
//        //            User.Claims.Where(c => c.Properties.ContainsKey("unique_name")).Select(c => c.Value).FirstOrDefault()
//        //        );

//        //        if (user == null)
//        //        {
//        //            throw new Exception(UserNotFindMsg);
//        //        }

//        //        string token = GetToken(user);
//        //        return Ok("bearer " + token);
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        return getExcepcionFormateada(e);
//        //    }


//        //}

//        ///// <summary>
//        ///// Regisun usuario
//        ///// </summary>
//        ///// <param name="Username">Nombre de usuario</param>
//        ///// <param name="Password">Contraseña (8 caracteres min, al menos una mayuscula, al menos una minuscula, al menos un numero, un caracter especial ej: ,.!@#$%^&* )</param>
//        ///// <param name="Email">Email</param>
//        ///// <param name="FirstName">Nombres</param>
//        ///// <param name="LastName">Apellidos</param>
//        ///// <param name="PasswordConfirmation">Confirmacion de la contraseña</param>
//        ///// <returns>string</returns>
//        //[HttpPost]
//        //[Route("Register")]
//        ////[Authorize(Roles = "AccountControllerRegister")]
//        //[AllowAnonymous]
//        //public async Task<IActionResult> Register([FromBody] RegisterModel registerModel)
//        //{
//        //    try
//        //    {
//        //        // El modelo valida por anotation antes de el siguiente if.
//        //        if (ModelState.IsValid)
//        //        {
//        //            var user = new ApplicationUser
//        //            {
//        //                //TODO: Use Automapper instaed of manual binding  

//        //                UserName = registerModel.Username,
//        //                FirstName = registerModel.FirstName,
//        //                LastName = registerModel.LastName,
//        //                Email = registerModel.Email
//        //            };

//        //            var identityResult = await this._userManager.CreateAsync(user, registerModel.Password);

//        //            if (identityResult.Succeeded)
//        //            {
//        //                await _signInManager.SignInAsync(user, isPersistent: false);
//        //                string token = GetToken(user);
//        //                return Ok("Usuario Creado");
//        //            }
//        //            else
//        //            {
//        //                return BadRequest(identityResult.Errors);
//        //            }
//        //        }
//        //        return BadRequest(ModelState);
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        return getExcepcionFormateada(e);
//        //    }

//        //}

//        ///// <summary>
//        ///// Resetea la contraseña a un usuario
//        ///// </summary>
//        ///// <param name="Username">Nombre de usuario</param>
//        ///// <param name="Token">token autorizante enviado al momento de solicitar el reseteo</param>
//        ///// <param name="Password">Contraseña (8 caracteres min, al menos una mayuscula, al menos una minuscula, al menos un numero, un caracter especial ej: ,.!@#$%^&* )</param>
//        ///// <returns>string</returns>
//        //[HttpPatch]
//        //[Route("ResetPassword")]
//        ////[Authorize(Roles = "AccountControllerResetPassword")]
//        //[AllowAnonymous]
//        //public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordModel resetPasswordModel)
//        //{

//        //    try
//        //    {
//        //        ApplicationUser user = _userManager.Users.FirstOrDefault(u => u.UserName == resetPasswordModel.Username);
//        //        if (user == null)
//        //        {
//        //            throw new Exception(UserNotFindMsg);
//        //        }

//        //        resetPasswordModel.Token = await _userManager.GeneratePasswordResetTokenAsync(user);

//        //        IdentityResult identityResult = await _userManager.ResetPasswordAsync(user, resetPasswordModel.Token, resetPasswordModel.Password);
//        //        if (!identityResult.Succeeded)
//        //        {
//        //            throw new Exception(identityResult.Errors.GetStringErrors());
//        //        }
//        //        return Ok("La contraseña se reseteó con éxito");
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        return getExcepcionFormateada(e);
//        //    }

//        //}


//        ///// <summary>
//        ///// Cambia la contraseña de un usuario
//        ///// </summary>
//        ///// <param name="Username">Nombre de usuario</param>
//        ///// <param name="Password">Contraseña actual</param>
//        ///// <param name="NewPassword">Contraseña (8 caracteres min, al menos una mayuscula, al menos una minuscula, al menos un numero, un caracter especial ej: ,.!@#$%^&* )</param>
//        ///// <param name="NewPasswordConfirmation">Confirmacion de la contraseña nueva</param>
//        ///// <returns>string</returns>
//        //[HttpPatch]
//        //[Route("ChangePassword")]
//        ////[Authorize(Roles = "AccountControllerChangePassword")]
//        //[AllowAnonymous]
//        //public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordModel changePasswordModel)
//        //{

//        //    try
//        //    {
//        //        if (changePasswordModel.NewPassword != changePasswordModel.NewPasswordConfirmation)
//        //        {
//        //            throw new Exception("La nueva contraseña y su confirmación no coincide");
//        //        }

//        //        if (changePasswordModel.Password == changePasswordModel.NewPassword)
//        //        {
//        //            throw new Exception("La nueva contraseña es identica a la anterior");
//        //        }

//        //        ApplicationUser user = _userManager.Users.FirstOrDefault(u => u.UserName == changePasswordModel.Username);

//        //        if (user == null)
//        //        {
//        //            throw new Exception(UserNotFindMsg);
//        //        }

//        //        IdentityResult identityResult = await _userManager.ChangePasswordAsync(user, changePasswordModel.Password, changePasswordModel.NewPassword);

//        //        if (!identityResult.Succeeded)
//        //        {
//        //            throw new Exception(identityResult.Errors.GetStringErrors());
//        //        }

//        //        return Ok("Cambio de contraseña correcto");
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        return getExcepcionFormateada(e);

//        //    }

//        //}



//        ///// <summary>
//        ///// Agrega roles al usuario
//        ///// </summary>
//        ///// <param name="Username">Nombre de usuario</param>
//        ///// <param name="Roles">Arreglo de roles para agregar.</param>
//        ///// <returns>string</returns>
//        //[HttpPost]
//        ////[Authorize(Roles = "AccountControllerAddRolesToUser")]
//        //[AllowAnonymous]
//        //[Route("AddRolesToUser")]
//        //public async Task<IActionResult> AddRolesToUser(AddRolesToUserModel addRolesToUserModel)
//        //{
//        //    try
//        //    {
//        //        if (addRolesToUserModel.Roles == null)
//        //        {
//        //            throw new Exception(RolesAreNull);
//        //        }

//        //        var identityUser = _userManager.Users.FirstOrDefault(u => u.UserName == addRolesToUserModel.Username);

//        //        if (identityUser == null)
//        //        {
//        //            throw new Exception(UserNotFindMsg);
//        //        }

//        //        foreach (string r in addRolesToUserModel.Roles)
//        //        {
//        //            var identityRole = await _roleManager.FindByNameAsync(r);

//        //            if (identityRole == null)
//        //            {
//        //                CreateRolePrivate(r);
//        //            }
//        //        }

//        //        //IEnumerable<string> roles = addRolesToUserModel.Roles.Select<RolModel,string>(r => r.nombre);
//        //        IEnumerable<string> roles = addRolesToUserModel.Roles;

//        //        var identityResult = await _userManager.AddToRolesAsync(identityUser, roles);

//        //        if (!identityResult.Succeeded)
//        //        {
//        //            throw new Exception(identityResult.Errors.GetStringErrors());
//        //        }

//        //        return Ok("Los roles fueron agregados con éxito");
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        return getExcepcionFormateada(e);
//        //    }

//        //}

//        ///// <summary>
//        ///// Elimina roles del usuario
//        ///// </summary>
//        ///// <param name="Username">Nombre de usuario</param>
//        ///// <param name="Roles">Arreglo de roles para eliminar.</param>
//        ///// <returns>string</returns>
//        //[HttpDelete]
//        ////[Authorize(Roles = "AccountControllerRemoveRolesToUser")]
//        //[AllowAnonymous]
//        //[Route("RemoveRolesToUser")]
//        //public async Task<IActionResult> RemoveRolesToUser(AddRolesToUserModel removeRolesToUserModel)
//        //{
//        //    try
//        //    {
//        //        if (removeRolesToUserModel.Roles == null)
//        //        {
//        //            throw new Exception(RolesAreNull);
//        //        }

//        //        var identityUser = _userManager.Users.FirstOrDefault(u => u.UserName == removeRolesToUserModel.Username);

//        //        if (identityUser == null)
//        //        {
//        //            throw new Exception(UserNotFindMsg);
//        //        }

//        //        foreach (string r in removeRolesToUserModel.Roles)
//        //        {
//        //            var identityRole = await _roleManager.FindByNameAsync(r);

//        //            if (identityRole == null)
//        //            {
//        //                CreateRolePrivate(r);
//        //            }
//        //        }

//        //        //IEnumerable<string> roles = removeRolesToUserModel.Rolesk.Select<RolModel, string>(r => r.nombre);
//        //        IEnumerable<string> roles = removeRolesToUserModel.Roles;

//        //        var identityResult = await _userManager.RemoveFromRolesAsync(identityUser, roles);

//        //        if (!identityResult.Succeeded)
//        //        {
//        //            throw new Exception(identityResult.Errors.GetStringErrors());
//        //        }

//        //        return Ok("Los roles fueron agregados con éxito");
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        return getExcepcionFormateada(e);
//        //    }

//        //}

//        ///// <summary>
//        ///// Agrega un rol manualmente a la base.
//        ///// </summary>
//        ///// <param name="role">Nombre del rol a agregar</param>
//        ///// <returns>string</returns>
//        //[HttpPost]
//        ////[Authorize(Roles = "AccountControllerCreateRole")]
//        //[AllowAnonymous]
//        //[Route("CreateRole")]
//        //public async Task<IActionResult> CreateRole(string role)
//        //{
//        //    return await CreateRolePrivate(role);
//        //}

//        //private async Task<IActionResult> CreateRolePrivate(string role)
//        //{
//        //    try
//        //    {
//        //        var identityRole = await _roleManager.FindByNameAsync(role);
//        //        if (identityRole == null)
//        //        {
//        //            identityRole = new IdentityRole(role);
//        //            var idtRes = await _roleManager.CreateAsync(identityRole);
//        //            if (!idtRes.Succeeded)
//        //            {
//        //                throw new Exception(idtRes.Errors.GetStringErrors());
//        //            }
//        //        }
//        //        return Ok("El rol se creo correctamente");
//        //    }
//        //    catch (Exception e)
//        //    {
//        //        return getExcepcionFormateada(e);
//        //    }
//        //}

//        ///// <summary>
//        ///// Devuelve el listado de todos los roles y su asignacion a los usuarios
//        ///// </summary>
//        ///// <param name="Username">Nombre del usuario</param>
//        ///// <returns>Listado de roles</returns>
//        ///// 
//        //[HttpPost]
//        ////[Authorize(Roles = "AccountControllerGetRolesForUser")]
//        //[AllowAnonymous]
//        //[Route("GetRolesForUser")]
//        //public async Task<IActionResult> GetRolesForUser(string UserName)
//        //{
//        //    try
//        //    {
//        //        IEnumerable<RolModel> roles = new RolModel[] { };

//        //        return Ok(roles);
//        //    }
//        //    catch (Exception e)
//        //    {

//        //        return getExcepcionFormateada(e);
//        //    }

//        //}


//        ///// <summary>
//        ///// Devuelve el listado de todos los roles y su asignacion a los usuarios
//        ///// </summary>
//        ///// <param name="Username">Nombre del usuario</param>
//        ///// <returns>Listado de roles</returns>
//        ///// 
//        //[HttpPost]
//        ////[Authorize(Roles = "AccountControllerGetAllRoles")]
//        //[AllowAnonymous]
//        //[Route("GetAllRoles")]
//        //public async Task<IActionResult> GetAllRoles()
//        //{
//        //    try
//        //    {
//        //        IEnumerable<RolModel> roles = new RolModel[] { };

//        //        return Ok(roles);
//        //    }
//        //    catch (Exception e)
//        //    {

//        //        return getExcepcionFormateada(e);
//        //    }

//        //}


//        private IActionResult getExcepcionFormateada(Exception e)
//        {
//            return BadRequest(BadRequest(e.Message + e.InnerException != null ? " " + e.InnerException : " "));
//        }

//        private String GetToken(IdentityUser user)
//        {

//            var utcNow = DateTime.UtcNow;

//            using (RSA privateRsa = RSA.Create())
//            {
//                try
//                {
//                    privateRsa.FromXmlFile(Path.Combine(Directory.GetCurrentDirectory(),
//                        "Keys",
//                         this._configuration.GetValue<String>("Tokens:PrivateKey")
//                         ));
//                    var privateKey = new RsaSecurityKey(privateRsa);
//                    SigningCredentials signingCredentials = new SigningCredentials(privateKey, SecurityAlgorithms.RsaSha256);

//                    var claims = new Claim[]
//                        {
//                        new Claim(JwtRegisteredClaimNames.Sub, user.Id),
//                        new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
//                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
//                        new Claim(JwtRegisteredClaimNames.Iat, utcNow.ToString())
//                        };

//                    //var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.configuration.GetValue<String>("Tokens:Key")));
//                    //var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

//                    var jwt = new JwtSecurityToken(
//                        signingCredentials: signingCredentials,
//                        claims: claims,
//                        notBefore: utcNow,
//                        expires: utcNow.AddSeconds(this._configuration.GetValue<int>("Tokens:Lifetime")),
//                        audience: this._configuration.GetValue<String>("Tokens:Audience"),
//                        issuer: this._configuration.GetValue<String>("Tokens:Issuer")
//                        );

//                    return new JwtSecurityTokenHandler().WriteToken(jwt);
//                }
//                catch (Exception e)
//                {
//                    throw e;
//                }


//            }

//        }

//    }


//}
