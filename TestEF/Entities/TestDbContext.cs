﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestEF.Entities
{
    public class TestDbContext : DbContext
    {
        //Constructor para Inyección de dependecia
        public TestDbContext(DbContextOptions<TestDbContext> options)
            : base(options)
        { }

        public DbSet<Post> Posts { get; set; }
        private DbSet<Blog> Blogs { get;  set; }

        public IQueryable<Blog> QBlogs
        {
            get
            {
                return this.Blogs.Include(b => b.Posts);
            }
        }

    }
}
