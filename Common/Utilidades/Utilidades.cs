﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Linq;

namespace Common.Utilidades
{
    public static class Utilidades
    {
        public static bool CheckDatabaseExists(string baseConnectionString, string databaseName)
        {
            baseConnectionString = removeInitialCatalog(baseConnectionString);
            using (var connection = new SqlConnection(baseConnectionString))
            {
                using (var command = new SqlCommand($"SELECT db_id('{databaseName}')", connection))
                {
                    connection.Open();
                    bool result = command.ExecuteScalar() != DBNull.Value;
                    connection.Close();
                    return (result);
                }
            }
        }

        public static string removeInitialCatalog(string baseConnectionString)
        {
            var arreglo = baseConnectionString.Split(';').ToList();
            var palabra = arreglo.FirstOrDefault(p => p.ToLower().Replace(" ","").Contains("initialcatalog"));
            if(palabra == null || palabra.Trim().Length == 0)
            {
                palabra = "";
            }
            return baseConnectionString.Replace(palabra, "");
        }


        public static bool CheckDatabaseExists(string connectionString)
        {
            //connectionString = removeInitialCatalog(connectionString);

            string databaseName = GetInitialCatalog(connectionString);

            using (var connection = new SqlConnection(connectionString))
            {
                using (var command = new SqlCommand($"SELECT db_id('{databaseName}')", connection))
                {
                    connection.Open();
                    bool result = command.ExecuteScalar() != DBNull.Value;
                    connection.Close();
                    return (result);
                }
            }
        }

        public static string GetInitialCatalog(string connectionString)
        {
            string catalogName = "";
            List<string> lista = connectionString.Split(';').ToList();
            string initialCatalogDirty = lista.FirstOrDefault(p => p.ToLower().Replace(" ", "").Contains("initialcatalog"));
            List<string> arregloInitialCatalog = initialCatalogDirty.Split('=').ToList();
            if (arregloInitialCatalog.Count() == 2)
            {
                catalogName = arregloInitialCatalog[1];
            }
            else
            {
                throw new Exception("the connectionString not contains any initial catalog");
            }

            return catalogName;
        }
    }
}
