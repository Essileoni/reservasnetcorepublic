﻿namespace ReservasApi.Models.Sucursal
{
    public class SucursalParaUnRolModel
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}
