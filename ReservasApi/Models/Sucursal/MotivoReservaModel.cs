﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Sucursal
{
    /// <summary>
    /// Entidad que representa que es lo que se va a hacer con la unidad de reserva.
    /// </summary>
    public class MotivoReservaModel
    {
        public int? Id { get; set; }
        /// <summary>
        /// Descripcion del motivo
        /// </summary>
        public string Descripcion { get; set; }
        /// <summary>
        /// Permite ponerle precio a la reserva y en algunos casos seleccionar multiples canchas en una misma reserva.
        /// </summary>
        public bool EsReservaCustom { get; set; }
        /// <summary>
        /// Tiempo, en minutos, inicial probable para el tipo motivo de reserva seleccionado
        /// </summary>
        public int TiempoEstandar { get; set; }
        /// <summary>
        /// Color Css para identificar el motivo de la reserva
        /// </summary>
        public string Color { get; set; }
    }
}
