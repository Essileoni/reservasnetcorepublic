﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Sucursal
{
    public class ReservaEstaLiberadaParaUnaFechaHoraCanchaModel
    {
        public DateTime FechaHoraComienzo { get; set; }
        public DateTime FechaHoraFin { get; set; }
        public int UnidadDeReservaId { get; set; }
    }
}
