﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Sucursal
{
    /// <summary>
    /// Unidad Mínima con los datos de la reserva
    /// </summary>
    public class UnidadDeReservaModel
    {
        public int Id { get; set; }
        public IEnumerable<PrecioPorHoraModel> PreciosPorHora { get; set; }
        /// <summary>
        /// Leve descripcion representativa que identifique visualmente a las unidad
        /// </summary>
        public string Descripcion { get; set; }
        /// <summary>
        /// True: Unidad Disponible. False: Unidad Suspendida
        /// </summary>
        public bool Activo { get; set; }
        /// <summary>
        /// Describe el tipo de unidad que se reserva. Cancha, Consultorio, Oficina. Etc.
        /// </summary>
        public string Tipo { get; set; }
    }
}
