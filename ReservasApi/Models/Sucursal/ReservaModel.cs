﻿using Identity.Entities;
using Reservas.Entidades.Sucursal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Sucursal
{
    /// <summary>
    /// Modelo de reserva para request y response;
    /// </summary>
    public class ReservaModel
    {
        /// <summary>
        /// Id de una reserva guardada. Devuelta en un action Get.
        /// </summary>
        public long? Id { get; set; }
        /// <summary>
        /// Motivo por el cual se originó la reserva. Devuelto en un action Get.
        /// </summary>
        public MotivoReservaModel Motivo { get; set; }
        /// <summary>
        /// ID del Motivo por el cual se originó la reserva. Seteado en un action Post/Patch
        /// </summary>
        public int? MotivoId { get; set; }
        /// <summary>
        /// Momento en que se crea la reserva.
        /// </summary>
        public DateTime FechaHoraCreacion { get; set; }
        /// <summary>
        /// Momento en el que debería comenzar la reserva.
        /// </summary>
        public DateTime FechaHoraComienzo { get; set; }
        /// <summary>
        /// Momento en el que debería finalizar la reserva.
        /// </summary>
        public DateTime FechaHoraFin { get; set; }
        /// <summary>
        /// Registro del momento en el que comenzó realmente una reserva.
        /// </summary>
        public DateTime? FechaHoraComienzoReal { get; set; }
        /// <summary>
        /// Registro del momento en el que finalizó una reserva y se desocupó la unidad de reserva.
        /// </summary>
        public DateTime? FechaHoraFinReal { get; set; }
        /// <summary>
        /// Registro del momento en el que cerró la reserva, y ya no va a tener más cambios, ni consumos.
        /// </summary>
        public DateTime? FechaHoraCierre { get; set; }
        /// <summary>
        /// Monto con el que se seño la unidad de reserva para una fecha y hora. Debe descontarse en el pago final
        /// </summary>
        public decimal SeniaTotal { get; set; }
        /// <summary>
        /// Deuda total a saldar por los integrantes de la reserva
        /// </summary>
        public decimal DeudaTotal { get; set; }
        /// <summary>
        /// Descuento aplicado al momento de crear la reserva. Se resta del total a pagar por la misma.
        /// </summary>
        public decimal Descuento { get; set; }
        /// <summary>
        /// Importe al momento de finalizada la reserva.
        /// </summary>
        public decimal ImporteUnidadReservada { get; set; }
        /// <summary>
        /// Representa el monto real a abonar al momento de la reserva. Se descuentan senias, saldos, descuentos, etc.
        /// </summary>
        public decimal TotalReserva { get; set; }
        /// <summary>
        /// Anotación que puede dejar un usuario del sistema al momento de reservar.
        /// </summary>
        public string Comentario { get; set; }
        /// <summary>
        /// Indica si el registro de la reserva fue para un día feriado
        /// </summary>
        public bool EsFeriado { get; set; }
        ///// <summary>
        ///// Id del usuario que creo la reserva, se utiliza guardar en el historial. 
        ///// </summary>
        //public string UsuarioId { get; set; }
        /// <summary>
        /// Usuario con datos filtrados que se envia al cliente en una consulta Get. No se setea desde el front-end nunca (NO post).
        /// </summary>
        public ApplicationUserModel Usuario { get; set; }
        /// <summary>
        /// Personas que forman parte o responden por la reserva. Seteado en un action Post/Patch
        /// </summary>
        public IList<ClienteReservaModel> ClientesReserva { get; set; } // Usar siempre IList para evitar problemas de automapper.
        /// <summary>
        /// Unidad Reservada por un usuario para una fecha y hora dada. Devuelta en un action Get. Debe ser eliminada en el Post.
        /// </summary>
        public UnidadDeReserva UnidadDeReserva { get; set; }
        /// <summary>
        /// Id de la unidad de reserva. Seteada en un action Post/Patch.
        /// </summary>
        public int? UnidadDeReservaId { get; set; }
        /// <summary>
        /// Colección de reservas deserializadas de formato StringJSON. Guardadas antes de ser editadas. Devuelta en un action Get.  Debe ser eliminada en el Post.
        /// </summary>
        public IEnumerable<ReservaModel> HistorialReservas { get; set; }
        /// <summary>
        /// Estado de la reserva. [eliminada | cerrada]
        /// </summary>
        [RegularExpression("(eliminada|cerrada)", ErrorMessage = "Solo puede ingresar eliminada, cerrada")]
        public string Estado { get; set; }


    }
        
    /// <summary>
    /// Modelo de ApplicationUser para request y response
    /// </summary>
    public class ApplicationUserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    

    

    

    
}
