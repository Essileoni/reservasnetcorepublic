﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Sucursal
{
    /// <summary>
    /// Registro de un precio para un rango de tiempo.
    /// </summary>
    public class PrecioPorHoraModel
    {
        public int Id { get; set; }
        /// <summary>
        /// Cota inicial con su valor incluido
        /// </summary>
        public decimal HoraInicio { get; set; }
        /// <summary>
        /// Cota final sin incluir
        /// </summary>
        public decimal HoraFin { get; set; }
        /// <summary>
        /// Hora inicial: Cantidad de horas desde la 00:00 del dia indicado. Ej: 18,5 = 18:30; Ej2: 27,25 = 3:00 pasada las 00:00 del día siguiente.
        /// </summary>
        public decimal HoraInicioAdaptada { get; set; }
        /// <summary>
        /// Hora final: Cantidad de horas desde la 00:00 del dia indicado. Ej: 18,5 = 18:30; Ej2: 27,25 = 3:00 pasada las 00:00 del día siguiente.
        /// </summary>
        public decimal HoraFinAdaptada { get; set; }
        /// <summary>
        /// Fecha en caso de eliminación.
        /// </summary>
        public DateTime? Eliminado { get; set; }
        /// <summary>
        /// Posibilidades - 'lunes','martes','miercoles','jueves','viernes','sabado','domingo','feriado'
        /// </summary>
        //[RegularExpression("(lunes|martes|miercoles|jueves|viernes|sabado|domingo|feriado)", ErrorMessage = "Solo puede ingresar lunes, martes, miercoles, jueves, viernes, sabado, domingo, feriado")]
        public string DiaSemana { get; set; }
        public decimal PrecioUnitarioPorHora { get; set; }

        public int UnidadDeReservaId { get; set; }

    }


}
