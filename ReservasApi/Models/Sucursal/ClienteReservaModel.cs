﻿using ReservasApi.Models.Empresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Sucursal
{
    /// <summary>
    /// Entidad que relaciona a un cliente en una reserva
    /// </summary>
    public class ClienteReservaModel
    {
        public int Id { get; set; }
        /// <summary>
        /// Id del cliente vinculado a la reserva. Los clientes son del negocio y no de la sucursal.
        /// </summary>
        public int? ClienteId { get; set; }
        /// <summary>
        /// Cliente vinculado a la reserva. Los clientes son del negocio y de la sucursal. Solo se devuelve en metodo Get.
        /// </summary>
        public ClienteModel Cliente { get; set; }
        /// <summary>
        /// Estado de asistencia del cliente vinculado a una reserva
        /// </summary>
        public string EstadoCumplimiento { get; set; } = null;
        /// <summary>
        /// Comentario cargado por el usuario al cancelar una reserva por motivos ajenos al cliente.
        /// </summary>
        public string ComentarioBaja { get; set; }
        /// <summary>
        /// La reserva puede marcarse como fija para ofrecer ofrecer en el futuro otra reserva a la misma hora.
        /// </summary>
        public bool EsReservaFija { get; set; }
        /// <summary>
        /// Sumatoria de seniaSaldo + seniaAdicional.
        /// </summary>
        public decimal Senia { get; set; }
        /// <summary>
        /// Saldo a favor usado en la seña.
        /// </summary>
        //public decimal SeniaSaldo { get; set; }
        /// <summary>
        /// Senia adicional ingresada manualmente.
        /// </summary>
        //public decimal SeniaAdicional { get; set; }
        /// <summary>
        /// Senia adicional ingresada manualmente.
        /// </summary>
        public decimal SaldoAFavor { get; set; }
        /// <summary>
        /// Saldo negativo a pagar al momento del cierre de la reserva.
        /// </summary>
        public decimal Deuda { get; set; }
        /// <summary>
        /// Fracción del descuento que beneficia al cliente.
        /// </summary>
        public decimal DescuentoReservaParte { get; set; }
        /// <summary>
        /// Fracción del importe a pagar por el cliente
        /// </summary>
        public decimal ImporteUnidadReservaParte { get; set; }
        /// <summary>
        /// Registra si el usuario el sistema decide o no usar el saldo. Por defecto false.
        /// </summary>
        public bool UsarSaldo { get; set; } = false;
        /// <summary>
        /// Dinero o billete fisico que presentó el cliente al momento de pagar.
        /// </summary>
        public decimal ClienteAbonoCon { get; set; }

        ///// <summary>
        ///// Reserva obtenida al consultar las reservas para un cliente.
        ///// </summary>
        //public long ReservaId { get; set; }

        /// <summary>
        /// Reserva obtenida al consultar las reservas para un cliente.
        /// </summary>
        //public ReservaModel Reserva { get; set; } CUIDADO si uso esta propiedad tengo que nulearla antes de se entregada a la salida.
    }
}
