﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Account
{
    public class SucursalGrupoUsuarioModel
    {
        public string ApplicationGroupId { get; set; }
        public string ApplicationUserId { get; set; }
        public int SucursalId { get; set; }
        public bool? Activar { get; set; }
    }
}
