﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Account
{
    public class UpdateRolesToGroupModel : SetRolesToGroupModel
    {
        public string[] RolesRemove { get; set; }
    }
}
