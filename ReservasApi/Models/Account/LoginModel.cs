﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Account
{
    public class LoginModel
    {
        [Required]
        [StringLength(100)]
        public String Username { get; set; }

        [Required]
        [StringLength(50)]
        public String Password { get; set; }
    }
}
