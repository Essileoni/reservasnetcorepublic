﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Account
{
    public class SucursalModel
    {
        public int Id { get; set; }
        /// <summary>
        /// Nombre de la sucursal, editable
        /// </summary>
        [MaxLength(128)]
        public string Nombre { get; set; }
        [MaxLength(256)]
        public string Calle { get; set; }
        [MaxLength(64)]
        public string Numero { get; set; }
        [MaxLength(2048)]
        public string ComoLlegar { get; set; }
        /// <summary>
        /// Telefonos separados por una pipe/pipa/barra |
        /// </summary>
        public string Telefonos { get; set; }
        [MaxLength(256)]
        public string Coordenadas { get; set; }
    }
}
