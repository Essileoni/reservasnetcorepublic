﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Account
{
    public class SetUserGroupsModel
    {
        public string Username { get; set; }
        public string[] GroupsIDs { get; set; }
    }
}
