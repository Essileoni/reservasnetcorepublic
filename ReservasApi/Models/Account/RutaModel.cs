﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Account
{
    public class RutaModel
    {
        public string Url { get; set; } = null;
        public string Descripcion { get; set; } = null;
        public IEnumerable<RutaModel> SubRutas { get; set; } = null;
    }
}
