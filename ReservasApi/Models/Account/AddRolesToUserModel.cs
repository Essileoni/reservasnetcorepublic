﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Account
{
    public class AddRolesToUserModel
    {
        public string Username { get; set; }
        //public IEnumerable<RolModel> Roles { get; set; }
        public IEnumerable<string> Roles { get; set; }
    }
}
