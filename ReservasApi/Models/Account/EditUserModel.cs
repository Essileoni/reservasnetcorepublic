﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Account
{
    public class EditUserModel 
    {
        public string Id { get; set; }

        [Required]
        [StringLength(200)]
        public String FirstName { get; set; }

        [Required]
        [StringLength(250)]
        public String LastName { get; set; }


        [EmailAddress(ErrorMessage = "El formato del email es incorrecto.")]
        public String Email { get; set; }

        [Required]
        [StringLength(100)]
        public virtual String Username { get; set; }


        /// <summary>
        /// Un usuario puede tener una sucursal predeterminada.
        /// </summary>
        [Required]
        public int SucursalPredeterminadaId { get; set; }
    }
}
