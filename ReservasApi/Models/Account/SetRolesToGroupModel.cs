﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Account
{
    public class SetRolesToGroupModel
    {
        public string[] Roles { get; set; }
        public string GroupId { get; set; }
    }
}
