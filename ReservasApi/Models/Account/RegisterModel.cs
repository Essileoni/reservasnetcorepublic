﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Account
{
    public class RegisterModel : LoginModel
    {

        [Required]
        [StringLength(200)]
        public String FirstName { get; set; }

        [Required]
        [StringLength(250)]
        public String LastName { get; set; }

        
        [EmailAddress(ErrorMessage ="El formato del email es incorrecto.")]
        public String Email { get; set; }

        /// <summary>
        /// Un usuario puede tener una sucursal predeterminada.
        /// </summary>
        [Required]
        public int SucursalPredeterminadaId { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "La contraseña y su confirmación no coinciden.")]
        [StringLength(50)]
        [RegularExpression("^((?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])|(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[^a-zA-Z0-9])|(?=.*?[A-Z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])|(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^a-zA-Z0-9])).{8,}$", ErrorMessage = "La contraseña debe tener al menos 8 caracteres, una combinacion de mayúsculas, minúsculas, números y algún caracter especial (e.j.  ,.!@#$%^&*). ")]
        public String PasswordConfirmation { get; set; }

    }
}
