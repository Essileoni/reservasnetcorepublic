﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Models.Empresa
{
    public class ClienteModel
    {
        public int Id { get; set; }
        /// <summary>
        /// Nombre y/o Apellido
        /// </summary>
        [Required(ErrorMessage = "Debe ingresar un nombre completo representativo")]
        [MinLength(1,ErrorMessage = "El nombre tiene que tener más de 10 caracteres")]
        public string NombreYApellido { get; set; }

        private string _Email;
        /// <summary>
        /// Email con formato válido o vacio.
        /// </summary>
        [EmailAddress(ErrorMessage = "Ingrese un formato de email válido")]
        public string Email { get { return _Email; } set { _Email = string.IsNullOrWhiteSpace(value) ? null : value; } }
        
        [RegularExpression("(eliminado)", ErrorMessage = "Solo puede ingresar estado eliminado")]
        public string Estado { get; set; }
        /// <summary>
        /// Si no es un celular y es un telefono particular sin 15 delante.
        /// </summary>
        public bool EsTelefonoFijo { get; set; }
        /// <summary>
        /// Numero de telefono o celular sin el 15
        /// </summary>
        [Required(ErrorMessage = "Debe ingresar el número de teléfono")]
        [MinLength(6, ErrorMessage = "El teléfono de 6 a 8 números.")]
        [MaxLength(8, ErrorMessage = "El teléfono de 6 a 8 números.")]
        public string TelefonoNumero { get; set; }
        /// <summary>
        /// Codigo del area sin el 0
        /// </summary>
        [Required(ErrorMessage = "Debe ingresar el área del teléfono")]
        [MinLength(2, ErrorMessage = "El código de area debe contener de 2 a 4 números")]
        [MaxLength(4, ErrorMessage = "El código de area debe contener de 2 a 4 números")]
        public string TelefonoArea { get; set; }
        public string Documento { get; set; }
        public string TipoDocumento { get; set; }
        /// <summary>
        /// Infracciones por faltar a la reserva o cancelar sin anticipación.
        /// </summary>
        public int InfraccionesGraves { get; set; }
        /// <summary>
        /// Infracciones por cancelar la reserva con poca anticipación
        /// </summary>
        public int Infracciones { get; set; }
        /// <summary>
        /// Cantidad de veces que asistió la reserva (Y pagó por ella)
        /// </summary>
        public int Cumplimientos { get; set; }
        /// <summary>
        /// Saldo a favor o en contra que le quedó al cliente despues del pago de una reserva.
        /// </summary>
        public decimal Saldo { get; set; }
    }
}
