﻿/*
USE [SampleIdentity_ValidarSucursalesAlAutorizar]
GO
/****** Object:  User [local]    Script Date: 27/5/2020 22:37:01 ******/
CREATE USER [local] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [SANCRISTOBAL\SileoniE]    Script Date: 27/5/2020 22:37:01 ******/
CREATE USER [SANCRISTOBAL\SileoniE] FOR LOGIN [SANCRISTOBAL\SileoniE] WITH DEFAULT_SCHEMA=[dbo]
GO
ALTER ROLE [db_owner] ADD MEMBER [local]
GO
/****** Object:  Table [dbo].[ApplicationGroup]    Script Date: 27/5/2020 22:37:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationGroup](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[Description] [nvarchar](2048) NOT NULL,
 CONSTRAINT [PK_ApplicationGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicationGroupRole]    Script Date: 27/5/2020 22:37:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationGroupRole](
	[ApplicationGroupId] [nvarchar](450) NOT NULL,
	[ApplicationRoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_ApplicationGroupRole] PRIMARY KEY CLUSTERED 
(
	[ApplicationGroupId] ASC,
	[ApplicationRoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicationUserGroup]    Script Date: 27/5/2020 22:37:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationUserGroup](
	[ApplicationUserId] [nvarchar](450) NOT NULL,
	[ApplicationGroupId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_ApplicationUserGroup] PRIMARY KEY CLUSTERED 
(
	[ApplicationGroupId] ASC,
	[ApplicationUserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 27/5/2020 22:37:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 27/5/2020 22:37:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 27/5/2020 22:37:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 27/5/2020 22:37:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](450) NOT NULL,
	[ProviderKey] [nvarchar](450) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 27/5/2020 22:37:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 27/5/2020 22:37:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[FirstName] [nvarchar](200) NOT NULL,
	[LastName] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 27/5/2020 22:37:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](450) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[ApplicationGroup] ([Id], [Name], [Description]) VALUES (N'0545c2e3-83e0-4503-993c-9ef881d18b32', N'string', N'string')
GO
INSERT [dbo].[ApplicationGroup] ([Id], [Name], [Description]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'AccountManager', N'string')
GO
INSERT [dbo].[ApplicationGroup] ([Id], [Name], [Description]) VALUES (N'784790b6-5f84-408a-8faf-c0b1e4f6f672', N'Grupo4', N'Grupo4 para test')
GO
INSERT [dbo].[ApplicationGroup] ([Id], [Name], [Description]) VALUES (N'9c7caaa6-d65d-4e22-b8e6-829b9412b4dd', N'Grupo5', N'Grupo5 para test')
GO
INSERT [dbo].[ApplicationGroup] ([Id], [Name], [Description]) VALUES (N'aa3f18a1-b5f6-41e7-8dba-b4ab3770cef3', N'Grupo1', N'Grupo para test')
GO
INSERT [dbo].[ApplicationGroup] ([Id], [Name], [Description]) VALUES (N'b24e8255-c58c-4e07-9eb6-2a1e62c2d22b', N'Grupo2', N'Grupo2 para test')
GO
INSERT [dbo].[ApplicationGroup] ([Id], [Name], [Description]) VALUES (N'e16a8133-b988-4ba5-87d1-726103a6a0bc', N'G2', N'G2')
GO
INSERT [dbo].[ApplicationGroup] ([Id], [Name], [Description]) VALUES (N'e83c6d21-3f64-49c5-abcb-9d0cdd2c71f5', N'ValidarSucursalesAlAutorizarUserGroup', N'asdasdfasdf')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'0545c2e3-83e0-4503-993c-9ef881d18b32', N'2e2314f4-898a-4b53-9800-b14dfe3fc6b4')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'0545c2e3-83e0-4503-993c-9ef881d18b32', N'8bcb4e9a-4811-4329-8326-f930f6deec6a')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'02825fdc-a26c-4477-86eb-b22e7fc5d798')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'073f7bb8-eeeb-4fe3-ad1c-2ecb21e4ff10')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'0873f142-95a3-4de8-80fa-e84cf5652d04')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'0db97655-cc2b-483b-a8c5-7e3b0557cd81')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'282c930e-690f-4304-9787-abc720f78bfd')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'2e2314f4-898a-4b53-9800-b14dfe3fc6b4')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'304832d6-9b4a-418d-9874-e3fa161b5dfa')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'307c18e0-64bc-49f6-956a-a646a87f7508')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'36001df7-2827-42dd-b25e-6b08abe2121b')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'3f8612c5-4d4f-423b-8ecc-1a73eeccf084')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'4014ef56-723b-4f89-a7bd-636d46ab45a4')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'404aca8a-b344-4266-9b86-7afd80cc3cde')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'41a6fac4-c40f-4d22-ab1b-11197918712a')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'451564f1-d00a-412e-9e5c-f8a8a176a360')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'49d2a712-528b-49f5-bc67-aad1a8530031')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'4d718d2e-da39-4ea5-a5b6-841d2110b46c')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'5115527d-5b50-4901-9b8d-25eed0c1f073')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'51b98fc8-186f-483d-850f-03c474d9d088')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'57a63675-ad46-480f-a337-1a50b20cfc5f')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'5bc49e81-0390-42f4-b959-920f3e19587d')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'5c671aa4-a0d2-4b1e-9e6b-2e5c38d9f7c9')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'632058c4-4d7a-48f2-8fe3-af53267c3d23')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'67d70ca8-ecb6-4c9c-bca1-da5df9d67613')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'6c971b7a-5ce6-42f9-8f7c-b3086ef26484')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'6e258433-307b-4d81-8f9d-01c2795d2b90')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'7393ae5d-24c6-4353-8e02-04609780076f')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'7658b525-5c6c-44cf-bad7-85b207d3d45e')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'7ea72615-21e9-45bd-bb62-b60e920214fd')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'835e38a6-64b6-4822-ba46-188f255eadb6')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'85463746-6899-47b8-93cc-60970a1c1cf8')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'88987f53-9b3e-4e72-b4a3-c018df0318ca')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'8bcb4e9a-4811-4329-8326-f930f6deec6a')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'8ceec5ce-40c1-4c31-97c3-ca353878802d')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'9000a680-7439-4f50-9b9e-d4245a67d48e')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'92abd6ae-9542-496c-bc4a-4d8f32602507')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'9b4e624e-379c-44fc-9b9a-07573ec8fcea')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'9c53184e-5b4c-4d21-ab1b-86c81ab257c6')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'9efb396d-b0dd-400c-ab90-5ebde347ba3b')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'ab8801de-623f-4bab-b8d8-4d1c773a5317')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'b0e8575a-9b86-4e2b-a640-739968d24b7e')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'b280033e-6e70-4157-8b8b-34a31badc483')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'b7369edf-22a6-4eb4-a748-13c93d99c1a3')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'b8264e63-cec9-4f41-a57a-56dda11dc4fe')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'bea0db2d-213f-4e1d-9d42-639282b27c00')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'c24b288c-673b-4887-98cc-a1ed87f8b30f')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'c2e95810-d3aa-452f-a1e1-27836a705b3d')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'c528a391-f14f-4d95-a0b5-5b574f753325')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'c8aed49f-4217-44a5-8305-a95e61433e5f')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'ca10e263-4562-41cb-83d3-f33c1f01aec2')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'cfabb23d-4a99-4dad-82d0-7d8bc98af645')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'd49f67b8-7b03-461f-a909-df3b29975a38')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'da735498-1865-49a8-88cd-3050da546679')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'ddc63f4f-1067-46b4-b7e9-3e92d9474d8f')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'dde01428-32e9-45da-aeb6-65250470078c')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'e63782df-6049-447c-8177-248fa198017a')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'e85d4735-a615-4c80-83bb-61de0f2349f5')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'e9752aa7-c471-45c2-9921-9b15351f9dee')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'ebe5b90f-aced-438e-86a4-c5d3cb3ecc1a')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'ec7f7d63-908e-4317-a2e3-b84f290dadee')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'edcb48b1-dffa-49f0-9136-0b15a5b5797a')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'f3d52374-5bf8-48c1-a5ca-649bc431f70b')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'f65704c2-ae0d-4919-90af-8f33d59e4aa3')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'f9ece714-b85f-45ad-8458-7bdfb5f5c62b')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'6377cd98-fd64-4f72-8a97-c625f831a472', N'fc338aa5-c74a-4a69-9b9d-930c1339ad54')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'aa3f18a1-b5f6-41e7-8dba-b4ab3770cef3', N'6e258433-307b-4d81-8f9d-01c2795d2b90')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'b24e8255-c58c-4e07-9eb6-2a1e62c2d22b', N'6e258433-307b-4d81-8f9d-01c2795d2b90')
GO
INSERT [dbo].[ApplicationGroupRole] ([ApplicationGroupId], [ApplicationRoleId]) VALUES (N'e83c6d21-3f64-49c5-abcb-9d0cdd2c71f5', N'7393ae5d-24c6-4353-8e02-04609780076f')
GO
INSERT [dbo].[ApplicationUserGroup] ([ApplicationUserId], [ApplicationGroupId]) VALUES (N'3e5a46aa-acc9-43e6-a796-d563038a6bc0', N'0545c2e3-83e0-4503-993c-9ef881d18b32')
GO
INSERT [dbo].[ApplicationUserGroup] ([ApplicationUserId], [ApplicationGroupId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'6377cd98-fd64-4f72-8a97-c625f831a472')
GO
INSERT [dbo].[ApplicationUserGroup] ([ApplicationUserId], [ApplicationGroupId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'6377cd98-fd64-4f72-8a97-c625f831a472')
GO
INSERT [dbo].[ApplicationUserGroup] ([ApplicationUserId], [ApplicationGroupId]) VALUES (N'1f77c63d-9620-4730-be63-a325531f553a', N'e83c6d21-3f64-49c5-abcb-9d0cdd2c71f5')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'02825fdc-a26c-4477-86eb-b22e7fc5d798', N'AccountControllerCreateToken', N'ACCOUNTCONTROLLERCREATETOKEN', N'276484f4-5626-4fc8-b09f-f4c7e8c94bf3')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'073f7bb8-eeeb-4fe3-ad1c-2ecb21e4ff10', N'SucursalControllerObtenerPreciosUnidadReserva', N'SUCURSALCONTROLLEROBTENERPRECIOSUNIDADRESERVA', N'3e7b2cbb-e41f-4391-9246-32cd59bb91bc')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'0873f142-95a3-4de8-80fa-e84cf5652d04', N'EmpresaControllerTest', N'EMPRESACONTROLLERTEST', N'bd4087d6-25d7-4739-8e6c-6bbadf29219f')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'0db97655-cc2b-483b-a8c5-7e3b0557cd81', N'SucursalControllerCrearUnidadReserva', N'SUCURSALCONTROLLERCREARUNIDADRESERVA', N'5fbf4779-0d57-49e7-aca2-90c5253403ff')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'1db7fb71-2035-4caa-bb10-64dc8f599f46', N'AccountControllerUpdateRolesSucursalParaGroup', N'ACCOUNTCONTROLLERUPDATEROLESSUCURSALPARAGROUP', N'2859108c-e44c-45e8-938a-0f9dbc9c8f6d')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'1e89cab7-39b4-4c59-83df-94845c88e44c', N'SucursalControllerGetSucursalesParaUnRol', N'SUCURSALCONTROLLERGETSUCURSALESPARAUNROL', N'38205125-0531-40c3-a241-bc9b925c9301')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'282c930e-690f-4304-9787-abc720f78bfd', N'AccountControllerGetAllGroups', N'ACCOUNTCONTROLLERGETALLGROUPS', N'0bef9ed9-a743-48de-9ce9-48e46529e7cf')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'2e2314f4-898a-4b53-9800-b14dfe3fc6b4', N'SucursalControllerGetReserva', N'SUCURSALCONTROLLERGETRESERVA', N'beb30b11-3f89-427e-a4c5-b891c4d1a419')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'304832d6-9b4a-418d-9874-e3fa161b5dfa', N'AccountControllerDeleteGroup', N'ACCOUNTCONTROLLERDELETEGROUP', N'851c25f0-ac5e-4829-b4d7-5aeafa216e20')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'307c18e0-64bc-49f6-956a-a646a87f7508', N'SucursalControllerModificarReserva', N'SUCURSALCONTROLLERMODIFICARRESERVA', N'02ee4c13-ffd0-4413-ab18-d0c646b7d959')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'36001df7-2827-42dd-b25e-6b08abe2121b', N'SucursalControllerCrearReserva', N'SUCURSALCONTROLLERCREARRESERVA', N'8a9a1462-c7e4-498c-ae2e-7196c5f48c6b')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'3f8612c5-4d4f-423b-8ecc-1a73eeccf084', N'SucursalControllerCrearMotivoReserva', N'SUCURSALCONTROLLERCREARMOTIVORESERVA', N'56931867-8a0d-459a-88e2-0b340ba5d56d')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'4014ef56-723b-4f89-a7bd-636d46ab45a4', N'SucursalControllerObtenerPreciosPorHora', N'SUCURSALCONTROLLEROBTENERPRECIOSPORHORA', N'd4eeeaf8-cea9-4b68-9627-aa3d0ebfd725')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'404aca8a-b344-4266-9b86-7afd80cc3cde', N'SucursalControllerTestThrowUnhandled', N'SUCURSALCONTROLLERTESTTHROWUNHANDLED', N'a45c0b35-7f89-431d-98c6-5674736c9585')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'41a6fac4-c40f-4d22-ab1b-11197918712a', N'SucursalControllerObtenerMotivosReserva', N'SUCURSALCONTROLLEROBTENERMOTIVOSRESERVA', N'6e1dcc99-ec48-4a0a-a956-74e611c4687b')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'451564f1-d00a-412e-9e5c-f8a8a176a360', N'AccountControllerCreateRole', N'ACCOUNTCONTROLLERCREATEROLE', N'4a0e6f4b-3e17-45b1-b429-8f1c0aee881b')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'49d2a712-528b-49f5-bc67-aad1a8530031', N'AccountControllerGetUsers', N'ACCOUNTCONTROLLERGETUSERS', N'60063eb4-e6e9-4ef5-b528-0ec386c09803')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'4d718d2e-da39-4ea5-a5b6-841d2110b46c', N'EmpresaController', N'EMPRESACONTROLLER', N'3ac97e1d-ffea-439f-8295-01ab3d25e2f1')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'5115527d-5b50-4901-9b8d-25eed0c1f073', N'AccountControllerGetUserLogin', N'ACCOUNTCONTROLLERGETUSERLOGIN', N'0fa4d153-ac9d-4c5c-8979-da59709ebfb1')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'51b98fc8-186f-483d-850f-03c474d9d088', N'SucursalControllerObtenerUnidadesReserva', N'SUCURSALCONTROLLEROBTENERUNIDADESRESERVA', N'9bc6826f-9e4e-41cd-9f31-8f1b97a44336')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'55fa187c-5203-4626-9c2f-f48f4fbae990', N'ValidarRolSucursalAttribute', N'VALIDARROLSUCURSALATTRIBUTE', N'04dca295-8cb9-47f2-af00-6855fffc206e')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'57a63675-ad46-480f-a337-1a50b20cfc5f', N'EmpresaControllerEliminarCliente', N'EMPRESACONTROLLERELIMINARCLIENTE', N'bf3a2945-76cc-41af-af98-21bec7700473')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'5bc49e81-0390-42f4-b959-920f3e19587d', N'AccountControllerGetRolesForUser', N'ACCOUNTCONTROLLERGETROLESFORUSER', N'061e79cf-b8dc-4cb5-adbd-b4eac17b5b2a')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'5c671aa4-a0d2-4b1e-9e6b-2e5c38d9f7c9', N'AccountController', N'ACCOUNTCONTROLLER', N'299c2370-bd66-4021-a65d-f846e20c6e7a')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'632058c4-4d7a-48f2-8fe3-af53267c3d23', N'SucursalControllerTestInclude', N'SUCURSALCONTROLLERTESTINCLUDE', N'906cba2f-b8bf-421a-8613-09291a05d64f')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'67d70ca8-ecb6-4c9c-bca1-da5df9d67613', N'ValuesControllerPut', N'VALUESCONTROLLERPUT', N'18f4cab6-2077-4aa9-92e8-554ca8538073')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'6c971b7a-5ce6-42f9-8f7c-b3086ef26484', N'AccountControllerGetGroupRoles', N'ACCOUNTCONTROLLERGETGROUPROLES', N'bd0f103a-84c1-4df6-a51e-ad291551ba86')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'6e258433-307b-4d81-8f9d-01c2795d2b90', N'AccountControllerGetUserGroups', N'ACCOUNTCONTROLLERGETUSERGROUPS', N'72e966d3-16cf-4687-9dc3-d91105f7a830')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'7393ae5d-24c6-4353-8e02-04609780076f', N'AccountParametersServices', N'ACCOUNTPARAMETERSSERVICES', N'c956247a-9fc2-4bb8-ae6a-baa9371a42fd')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'7658b525-5c6c-44cf-bad7-85b207d3d45e', N'SucursalControllerGetReservaEstaLiberadaParaUnaFechaHoraCancha', N'SUCURSALCONTROLLERGETRESERVAESTALIBERADAPARAUNAFECHAHORACANCHA', N'46c0a160-c404-46bd-9634-b71959604bea')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'7ea72615-21e9-45bd-bb62-b60e920214fd', N'AccountControllerGetAllRoles', N'ACCOUNTCONTROLLERGETALLROLES', N'45e8083c-c617-412d-84be-4cce46f1342d')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'835e38a6-64b6-4822-ba46-188f255eadb6', N'SucursalControllerGetReservasParaUnaFecha', N'SUCURSALCONTROLLERGETRESERVASPARAUNAFECHA', N'73f5b32f-5eef-477f-859d-1eff732956ee')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'85463746-6899-47b8-93cc-60970a1c1cf8', N'SucursalControllerGetReservasEliminadasParaUnaFecha', N'SUCURSALCONTROLLERGETRESERVASELIMINADASPARAUNAFECHA', N'1d79d41e-824c-4834-920e-a20e83ed0c15')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'88987f53-9b3e-4e72-b4a3-c018df0318ca', N'SucursalControllerModificarPrecioPorHora', N'SUCURSALCONTROLLERMODIFICARPRECIOPORHORA', N'6b1884e0-49dd-4291-b3de-1df42c98fbc9')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'8bcb4e9a-4811-4329-8326-f930f6deec6a', N'AccountControllerRefreshToken', N'ACCOUNTCONTROLLERREFRESHTOKEN', N'97b49f24-4ad0-418f-ab6f-6a79e1305a52')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'8ceec5ce-40c1-4c31-97c3-ca353878802d', N'ValuesControllerDelete', N'VALUESCONTROLLERDELETE', N'4faa1ea2-c28c-44ff-963d-a547a59fcd92')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'8d631b51-8d4c-4094-869d-85fede3e9be7', N'MiCulpa', N'MICULPA', N'540c3338-265f-484e-88b3-a09c690fe275')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'9000a680-7439-4f50-9b9e-d4245a67d48e', N'SucursalControllerCrearPrecioPorHora', N'SUCURSALCONTROLLERCREARPRECIOPORHORA', N'3c24a52a-24c6-4d7d-8b58-5d5cffdae13f')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'92abd6ae-9542-496c-bc4a-4d8f32602507', N'SucursalControllerObtenerUnidadReserva', N'SUCURSALCONTROLLEROBTENERUNIDADRESERVA', N'34a0130a-5c4f-44c8-8f85-9fb8287ce6c7')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'9b4e624e-379c-44fc-9b9a-07573ec8fcea', N'EmpresaControllerEditarCliente', N'EMPRESACONTROLLEREDITARCLIENTE', N'965d35c9-3607-446b-937e-bb6c93717d0c')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'9c53184e-5b4c-4d21-ab1b-86c81ab257c6', N'IAccountParametersServices', N'IACCOUNTPARAMETERSSERVICES', N'aa148746-7fbd-4009-8d69-e3be0530522f')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'9efb396d-b0dd-400c-ab90-5ebde347ba3b', N'AccountControllerChangePassword', N'ACCOUNTCONTROLLERCHANGEPASSWORD', N'1c260733-823b-4e28-8d4a-a3f307bbda98')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'ab8801de-623f-4bab-b8d8-4d1c773a5317', N'AccountControllerGetGroupUsers', N'ACCOUNTCONTROLLERGETGROUPUSERS', N'1e551dcf-7144-47fd-b4ea-2be799a014f7')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'b0e8575a-9b86-4e2b-a640-739968d24b7e', N'AccountControllerSetRolesToGroup', N'ACCOUNTCONTROLLERSETROLESTOGROUP', N'38057f70-8871-4b91-bbe8-2d486ba0526b')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'b280033e-6e70-4157-8b8b-34a31badc483', N'SucursalControllerCrearBase', N'SUCURSALCONTROLLERCREARBASE', N'385da633-b832-46c7-8029-c5a8179b5931')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'b7369edf-22a6-4eb4-a748-13c93d99c1a3', N'AccountControllerRemoveRolesToUser', N'ACCOUNTCONTROLLERREMOVEROLESTOUSER', N'd4abccf3-3e94-4ce9-9ed7-2dbaf5baf711')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'b8264e63-cec9-4f41-a57a-56dda11dc4fe', N'ValuesControllerPost', N'VALUESCONTROLLERPOST', N'dbed2e0e-fafc-4b79-bc4d-2a6f3f74b99b')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'bea0db2d-213f-4e1d-9d42-639282b27c00', N'AccountControllerAddRolesToUser', N'ACCOUNTCONTROLLERADDROLESTOUSER', N'cb75cb29-b9b9-4431-ad0c-8235698152ad')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'c24b288c-673b-4887-98cc-a1ed87f8b30f', N'SucursalControllerObtenerUnidadesReservaList', N'SUCURSALCONTROLLEROBTENERUNIDADESRESERVALIST', N'3c890906-b8b6-4162-a8d0-fe6ca075dde7')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'c2e95810-d3aa-452f-a1e1-27836a705b3d', N'AccountControllerCreateGroup', N'ACCOUNTCONTROLLERCREATEGROUP', N'93293524-b693-45ea-be4c-02bd9a7edc21')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'c528a391-f14f-4d95-a0b5-5b574f753325', N'EmpresaControllerGetCliente', N'EMPRESACONTROLLERGETCLIENTE', N'9652369d-d30f-4e0e-917c-dd4df2f71c98')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'c8aed49f-4217-44a5-8305-a95e61433e5f', N'AccountControllerEditGroup', N'ACCOUNTCONTROLLEREDITGROUP', N'a5601527-3ca4-4de9-8d15-78d674a717e4')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'ca10e263-4562-41cb-83d3-f33c1f01aec2', N'AccountControllerEditUser', N'ACCOUNTCONTROLLEREDITUSER', N'de90a1ad-eb0c-4d8a-96d7-3f9075b8d8d2')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'cfabb23d-4a99-4dad-82d0-7d8bc98af645', N'SucursalController', N'SUCURSALCONTROLLER', N'9596326e-cd73-46b9-bd3b-9efd019d90a6')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'd48ec73a-e2ce-4baf-8033-bd8e6647c2c9', N'AccountControllerUpdateRolesForGroup', N'ACCOUNTCONTROLLERUPDATEROLESFORGROUP', N'196a5e66-eee3-423d-a1c1-e65aebb5a30f')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'd49f67b8-7b03-461f-a909-df3b29975a38', N'AccountControllerDeleteUser', N'ACCOUNTCONTROLLERDELETEUSER', N'9e84605f-a7d1-48b3-b134-b19a4fb78ffa')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'da735498-1865-49a8-88cd-3050da546679', N'AccountControllerRegister', N'ACCOUNTCONTROLLERREGISTER', N'911cb028-42cd-429d-9ab4-156b5cccff9f')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'dc40754c-ac50-4f60-b04e-0033926a1714', N'SucursalControllerEliminarPrecioPorHora', N'SUCURSALCONTROLLERELIMINARPRECIOPORHORA', N'31d6ffb4-ae91-4056-bd85-c41c889482f0')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'ddc63f4f-1067-46b4-b7e9-3e92d9474d8f', N'EmpresaControllerBuscarCliente', N'EMPRESACONTROLLERBUSCARCLIENTE', N'3fa36284-34c3-4e86-948e-2a486ccf0ad1')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'dde01428-32e9-45da-aeb6-65250470078c', N'EmpresaControllerCrearCliente', N'EMPRESACONTROLLERCREARCLIENTE', N'631328fe-5da9-40da-af78-bf791c9a9c54')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'e63782df-6049-447c-8177-248fa198017a', N'AccountControllerGetGroup', N'ACCOUNTCONTROLLERGETGROUP', N'25e9f8e6-5a58-4064-97d3-3934740cc030')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'e85d4735-a615-4c80-83bb-61de0f2349f5', N'SucursalControllerEliminarReserva', N'SUCURSALCONTROLLERELIMINARRESERVA', N'6abb9238-71d3-4fd1-91ad-489052c2a8a9')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'e9752aa7-c471-45c2-9921-9b15351f9dee', N'AccountControllerUpdateRoles', N'ACCOUNTCONTROLLERUPDATEROLES', N'ae62430a-e208-4264-bd9e-6d8944020fb7')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'ebe5b90f-aced-438e-86a4-c5d3cb3ecc1a', N'ValuesController', N'VALUESCONTROLLER', N'945e17d7-8a41-4b6f-9e35-aab5a50e2ac7')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'ec7f7d63-908e-4317-a2e3-b84f290dadee', N'EmpresaControllerActualizarSemaforoCliente', N'EMPRESACONTROLLERACTUALIZARSEMAFOROCLIENTE', N'0862944b-ee17-4977-ab39-e4aded6dcddc')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'edcb48b1-dffa-49f0-9136-0b15a5b5797a', N'ValuesControllerGet', N'VALUESCONTROLLERGET', N'419661bf-8626-4a3d-b701-e34205dcd8c8')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'f1bbb914-a604-49af-9966-303d77ab27c2', N'AccountControllerObtenerRutas', N'ACCOUNTCONTROLLEROBTENERRUTAS', N'cc3d0557-505a-464e-afca-ee827bd6c2f5')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'f3d52374-5bf8-48c1-a5ca-649bc431f70b', N'AccountControllerSetUserGroups', N'ACCOUNTCONTROLLERSETUSERGROUPS', N'94c8032a-00f8-49bc-8adb-0a59d08071ca')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'f65704c2-ae0d-4919-90af-8f33d59e4aa3', N'SucursalControllerModificarUnidadReserva', N'SUCURSALCONTROLLERMODIFICARUNIDADRESERVA', N'25587231-fc82-4b91-bc65-4f735c5c573d')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'f9ece714-b85f-45ad-8458-7bdfb5f5c62b', N'AccountControllerResetPassword', N'ACCOUNTCONTROLLERRESETPASSWORD', N'4a0ea04e-65cd-481d-888e-1d3d20093548')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'fc338aa5-c74a-4a69-9b9d-930c1339ad54', N'EmpresaControllerActualizarSaldoCliente', N'EMPRESACONTROLLERACTUALIZARSALDOCLIENTE', N'2928e7b5-9989-49b8-a870-8de0b5399b9f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'1f77c63d-9620-4730-be63-a325531f553a', N'7393ae5d-24c6-4353-8e02-04609780076f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'02825fdc-a26c-4477-86eb-b22e7fc5d798')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'073f7bb8-eeeb-4fe3-ad1c-2ecb21e4ff10')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'0873f142-95a3-4de8-80fa-e84cf5652d04')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'0db97655-cc2b-483b-a8c5-7e3b0557cd81')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'282c930e-690f-4304-9787-abc720f78bfd')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'2e2314f4-898a-4b53-9800-b14dfe3fc6b4')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'304832d6-9b4a-418d-9874-e3fa161b5dfa')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'307c18e0-64bc-49f6-956a-a646a87f7508')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'36001df7-2827-42dd-b25e-6b08abe2121b')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'3f8612c5-4d4f-423b-8ecc-1a73eeccf084')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'4014ef56-723b-4f89-a7bd-636d46ab45a4')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'404aca8a-b344-4266-9b86-7afd80cc3cde')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'41a6fac4-c40f-4d22-ab1b-11197918712a')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'451564f1-d00a-412e-9e5c-f8a8a176a360')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'49d2a712-528b-49f5-bc67-aad1a8530031')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'4d718d2e-da39-4ea5-a5b6-841d2110b46c')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'5115527d-5b50-4901-9b8d-25eed0c1f073')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'51b98fc8-186f-483d-850f-03c474d9d088')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'57a63675-ad46-480f-a337-1a50b20cfc5f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'5bc49e81-0390-42f4-b959-920f3e19587d')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'5c671aa4-a0d2-4b1e-9e6b-2e5c38d9f7c9')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'632058c4-4d7a-48f2-8fe3-af53267c3d23')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'67d70ca8-ecb6-4c9c-bca1-da5df9d67613')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'6c971b7a-5ce6-42f9-8f7c-b3086ef26484')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'6e258433-307b-4d81-8f9d-01c2795d2b90')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'7393ae5d-24c6-4353-8e02-04609780076f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'7658b525-5c6c-44cf-bad7-85b207d3d45e')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'7ea72615-21e9-45bd-bb62-b60e920214fd')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'835e38a6-64b6-4822-ba46-188f255eadb6')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'85463746-6899-47b8-93cc-60970a1c1cf8')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'88987f53-9b3e-4e72-b4a3-c018df0318ca')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'8bcb4e9a-4811-4329-8326-f930f6deec6a')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'8ceec5ce-40c1-4c31-97c3-ca353878802d')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'9000a680-7439-4f50-9b9e-d4245a67d48e')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'92abd6ae-9542-496c-bc4a-4d8f32602507')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'9b4e624e-379c-44fc-9b9a-07573ec8fcea')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'9c53184e-5b4c-4d21-ab1b-86c81ab257c6')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'9efb396d-b0dd-400c-ab90-5ebde347ba3b')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'ab8801de-623f-4bab-b8d8-4d1c773a5317')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'b0e8575a-9b86-4e2b-a640-739968d24b7e')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'b280033e-6e70-4157-8b8b-34a31badc483')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'b7369edf-22a6-4eb4-a748-13c93d99c1a3')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'b8264e63-cec9-4f41-a57a-56dda11dc4fe')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'bea0db2d-213f-4e1d-9d42-639282b27c00')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'c24b288c-673b-4887-98cc-a1ed87f8b30f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'c2e95810-d3aa-452f-a1e1-27836a705b3d')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'c528a391-f14f-4d95-a0b5-5b574f753325')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'c8aed49f-4217-44a5-8305-a95e61433e5f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'ca10e263-4562-41cb-83d3-f33c1f01aec2')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'cfabb23d-4a99-4dad-82d0-7d8bc98af645')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'd49f67b8-7b03-461f-a909-df3b29975a38')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'da735498-1865-49a8-88cd-3050da546679')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'ddc63f4f-1067-46b4-b7e9-3e92d9474d8f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'dde01428-32e9-45da-aeb6-65250470078c')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'e63782df-6049-447c-8177-248fa198017a')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'e85d4735-a615-4c80-83bb-61de0f2349f5')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'e9752aa7-c471-45c2-9921-9b15351f9dee')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'ebe5b90f-aced-438e-86a4-c5d3cb3ecc1a')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'ec7f7d63-908e-4317-a2e3-b84f290dadee')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'edcb48b1-dffa-49f0-9136-0b15a5b5797a')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'f3d52374-5bf8-48c1-a5ca-649bc431f70b')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'f65704c2-ae0d-4919-90af-8f33d59e4aa3')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'f9ece714-b85f-45ad-8458-7bdfb5f5c62b')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'fc338aa5-c74a-4a69-9b9d-930c1339ad54')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'3e5a46aa-acc9-43e6-a796-d563038a6bc0', N'7393ae5d-24c6-4353-8e02-04609780076f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'02825fdc-a26c-4477-86eb-b22e7fc5d798')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'073f7bb8-eeeb-4fe3-ad1c-2ecb21e4ff10')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'0873f142-95a3-4de8-80fa-e84cf5652d04')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'0db97655-cc2b-483b-a8c5-7e3b0557cd81')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'282c930e-690f-4304-9787-abc720f78bfd')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'2e2314f4-898a-4b53-9800-b14dfe3fc6b4')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'304832d6-9b4a-418d-9874-e3fa161b5dfa')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'307c18e0-64bc-49f6-956a-a646a87f7508')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'36001df7-2827-42dd-b25e-6b08abe2121b')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'3f8612c5-4d4f-423b-8ecc-1a73eeccf084')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'4014ef56-723b-4f89-a7bd-636d46ab45a4')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'404aca8a-b344-4266-9b86-7afd80cc3cde')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'41a6fac4-c40f-4d22-ab1b-11197918712a')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'451564f1-d00a-412e-9e5c-f8a8a176a360')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'49d2a712-528b-49f5-bc67-aad1a8530031')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'4d718d2e-da39-4ea5-a5b6-841d2110b46c')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'5115527d-5b50-4901-9b8d-25eed0c1f073')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'51b98fc8-186f-483d-850f-03c474d9d088')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'57a63675-ad46-480f-a337-1a50b20cfc5f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'5bc49e81-0390-42f4-b959-920f3e19587d')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'5c671aa4-a0d2-4b1e-9e6b-2e5c38d9f7c9')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'632058c4-4d7a-48f2-8fe3-af53267c3d23')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'67d70ca8-ecb6-4c9c-bca1-da5df9d67613')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'6c971b7a-5ce6-42f9-8f7c-b3086ef26484')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'6e258433-307b-4d81-8f9d-01c2795d2b90')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'7393ae5d-24c6-4353-8e02-04609780076f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'7658b525-5c6c-44cf-bad7-85b207d3d45e')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'7ea72615-21e9-45bd-bb62-b60e920214fd')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'835e38a6-64b6-4822-ba46-188f255eadb6')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'85463746-6899-47b8-93cc-60970a1c1cf8')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'88987f53-9b3e-4e72-b4a3-c018df0318ca')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'8bcb4e9a-4811-4329-8326-f930f6deec6a')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'8ceec5ce-40c1-4c31-97c3-ca353878802d')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'9000a680-7439-4f50-9b9e-d4245a67d48e')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'92abd6ae-9542-496c-bc4a-4d8f32602507')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'9b4e624e-379c-44fc-9b9a-07573ec8fcea')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'9c53184e-5b4c-4d21-ab1b-86c81ab257c6')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'9efb396d-b0dd-400c-ab90-5ebde347ba3b')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'ab8801de-623f-4bab-b8d8-4d1c773a5317')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'b0e8575a-9b86-4e2b-a640-739968d24b7e')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'b280033e-6e70-4157-8b8b-34a31badc483')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'b7369edf-22a6-4eb4-a748-13c93d99c1a3')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'b8264e63-cec9-4f41-a57a-56dda11dc4fe')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'bea0db2d-213f-4e1d-9d42-639282b27c00')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'c24b288c-673b-4887-98cc-a1ed87f8b30f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'c2e95810-d3aa-452f-a1e1-27836a705b3d')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'c528a391-f14f-4d95-a0b5-5b574f753325')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'c8aed49f-4217-44a5-8305-a95e61433e5f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'ca10e263-4562-41cb-83d3-f33c1f01aec2')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'cfabb23d-4a99-4dad-82d0-7d8bc98af645')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'd49f67b8-7b03-461f-a909-df3b29975a38')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'da735498-1865-49a8-88cd-3050da546679')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'ddc63f4f-1067-46b4-b7e9-3e92d9474d8f')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'dde01428-32e9-45da-aeb6-65250470078c')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'e63782df-6049-447c-8177-248fa198017a')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'e85d4735-a615-4c80-83bb-61de0f2349f5')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'e9752aa7-c471-45c2-9921-9b15351f9dee')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'ebe5b90f-aced-438e-86a4-c5d3cb3ecc1a')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'ec7f7d63-908e-4317-a2e3-b84f290dadee')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'edcb48b1-dffa-49f0-9136-0b15a5b5797a')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'f3d52374-5bf8-48c1-a5ca-649bc431f70b')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'f65704c2-ae0d-4919-90af-8f33d59e4aa3')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'f9ece714-b85f-45ad-8458-7bdfb5f5c62b')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'fc338aa5-c74a-4a69-9b9d-930c1339ad54')
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName]) VALUES (N'1f77c63d-9620-4730-be63-a325531f553a', N'ValidarSucursalesAlAutorizarUser', N'VALIDARSUCURSALESALAUTORIZARUSER', N'ValidarSucursalesAlAutorizarUser@gmail.com', N'VALIDARSUCURSALESALAUTORIZARUSER@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEJtXwNHzozHe1tPVkqcwPF9050r179a4nJbx+STPkufV/RoXs80xQ+rlNhsU2BG+YQ==', N'TAMF2JNVSI5SL673HTP5RRGM56W3NGIA', N'625db700-e467-4c84-b7bf-69ab639d3a44', NULL, 0, 0, NULL, 1, 0, N'ValidarSucursalesAlAutorizarUser', N'ValidarSucursalesAlAutorizarUser')
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName]) VALUES (N'38726c93-5f9c-4f7e-868c-9ad673be05fa', N'superadmin', N'SUPERADMIN', N'esteban.sileoni@gmail.com', N'ESTEBAN.SILEONI@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEAewCN6GxcPFKrHdAYKGmNoq69vaqHF0+6Ysz9bT+97lC8E0FsHQh31O+IOZEeNCYQ==', N'DVB3EBYHG5N4SS3MC2Q6A3MAKKVB7T6V', N'0104f76a-43fa-434b-8a89-3b38d0611388', NULL, 0, 0, NULL, 1, 0, N'Super', N'Admin')
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName]) VALUES (N'3e5a46aa-acc9-43e6-a796-d563038a6bc0', N'TestGroupSucursal', N'TESTGROUPSUCURSAL', N'Sucursal@hotmail.com', N'SUCURSAL@HOTMAIL.COM', 0, N'AQAAAAEAACcQAAAAEP//Ox3NAqyGaD43YQZh265slEYhb68b6gJSSLoF09J7cEGD+p1BefpV54YGMoiqGA==', N'7G2KTNQJ7ABZS6FVRY2OYXVT7JV4U2AG', N'84eafadd-e2bb-4ff7-8db5-965f3bdd8d91', NULL, 0, 0, NULL, 1, 0, N'TestGroup', N'Sucursal')
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [FirstName], [LastName]) VALUES (N'a57145ea-481f-451a-a33c-8ea0c088a06c', N'F.bravo', N'F.BRAVO', N'fernandobravomartinez@gmail.com', N'FERNANDOBRAVOMARTINEZ@GMAIL.COM', 0, N'AQAAAAEAACcQAAAAEIXIu2BSiSsTAD0FfMLcbw9f9xisaXTqQMc4d9ZFJnVMXHA27RAdbZl3AvqR0iv9tw==', N'ALLVK35T3L3S5ZIA3NXGF3WROPZJ2NZC', N'cad575eb-dd92-4dbe-bbd0-df4a1b5e1cd4', NULL, 0, 0, NULL, 1, 0, N'Fernando', N'Bravo')
GO
ALTER TABLE [dbo].[ApplicationGroupRole]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationGroupRole_ApplicationGroup_ApplicationGroupId] FOREIGN KEY([ApplicationGroupId])
REFERENCES [dbo].[ApplicationGroup] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ApplicationGroupRole] CHECK CONSTRAINT [FK_ApplicationGroupRole_ApplicationGroup_ApplicationGroupId]
GO
ALTER TABLE [dbo].[ApplicationUserGroup]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationUserGroup_ApplicationGroup_ApplicationGroupId] FOREIGN KEY([ApplicationGroupId])
REFERENCES [dbo].[ApplicationGroup] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ApplicationUserGroup] CHECK CONSTRAINT [FK_ApplicationUserGroup_ApplicationGroup_ApplicationGroupId]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
*/