﻿/*SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GroupUserSucursal](
	[ApplicationGroupId] [nvarchar](450) NOT NULL,
	[ApplicationUserId] [nvarchar](450) NOT NULL,
	[SucursalId] [int] NOT NULL,
 CONSTRAINT [PK_GroupUserSucursal] PRIMARY KEY CLUSTERED 
(
	[ApplicationGroupId] ASC,
	[ApplicationUserId] ASC,
	[SucursalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[GroupUserSucursal]  WITH CHECK ADD  CONSTRAINT [FK_GroupUserSucursal_ApplicationGroup_ApplicationGroupId] FOREIGN KEY([ApplicationGroupId])
REFERENCES [dbo].[ApplicationGroup] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[GroupUserSucursal] CHECK CONSTRAINT [FK_GroupUserSucursal_ApplicationGroup_ApplicationGroupId]
GO

ALTER TABLE [dbo].[GroupUserSucursal]  WITH CHECK ADD  CONSTRAINT [FK_GroupUserSucursal_AspNetUsers_ApplicationUserId] FOREIGN KEY([ApplicationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[GroupUserSucursal] CHECK CONSTRAINT [FK_GroupUserSucursal_AspNetUsers_ApplicationUserId]
GO


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GroupUserSucursal](
	[ApplicationGroupId] [nvarchar](450) NOT NULL,
	[ApplicationUserId] [nvarchar](450) NOT NULL,
	[SucursalId] [int] NOT NULL,
 CONSTRAINT [PK_GroupUserSucursal] PRIMARY KEY CLUSTERED 
(
	[ApplicationGroupId] ASC,
	[ApplicationUserId] ASC,
	[SucursalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[GroupUserSucursal]  WITH CHECK ADD  CONSTRAINT [FK_GroupUserSucursal_ApplicationGroup_ApplicationGroupId] FOREIGN KEY([ApplicationGroupId])
REFERENCES [dbo].[ApplicationGroup] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[GroupUserSucursal] CHECK CONSTRAINT [FK_GroupUserSucursal_ApplicationGroup_ApplicationGroupId]
GO

ALTER TABLE [dbo].[GroupUserSucursal]  WITH CHECK ADD  CONSTRAINT [FK_GroupUserSucursal_AspNetUsers_ApplicationUserId] FOREIGN KEY([ApplicationUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[GroupUserSucursal] CHECK CONSTRAINT [FK_GroupUserSucursal_AspNetUsers_ApplicationUserId]
GO


*/