﻿

namespace ReservasApi.Todo
{
    internal class Anotaciones
    {
        private readonly string todo = @"

        1) Los Roles tiene sucursal para validar
	        a) a que grupos del usuario pertenece el rol
	        b) Alguno de los grupos encontrados tiene permiso para sucursal de la consulta
	        c) listo

        2) Los grupos tienen que validar sucursal
	        a) el usuario tiene el rol, pero que grupos tiene que tengan ese rol
	        b) de esos grupos encontrados cuales tienen el cruce con el usuario y la sucursal
	        c) listo si existe al menos uno tiene permisos
            ¿Si tengo 2 grupos para un rol y uno requiere sucursal y otro no que hago?
                a) Tener cuidado al asignar crear grupos que van a contener roles que requieran sucursal
                b) Por reflection a la hora de asignar roles a un grupo, si ese grupo tiene la etiqueta del filtro de seguridad, deberia saberse para que el grupo tenga los como obligatorio.
                c) Si existe al menos un grupo que tenga ese rol y pida sucursal ya todos los grupos deberian pedir sucursal o actualizarse.

        Otro analisis
        3) simplificado un enpoint un rol
	        facil
        4) un endpoint mas de un rol
	        a) si el usuario tiene alguno de los multiples roles ingresa
		        se debe validar para los roles que pida el enpoint. Validar todo lo anterior 

            b) el usuario debe tener ambos roles para ingresar al endpoint

        buscar un grupo que tenga ambos roles.Y validar todo lo anterior del paso 2


            Probar la administracion de los grupos.
            
            Intentan crear un singleton para el GroupManager.

            Usar config completa en Swagger
            Agregar Https a todo el proyecto
            cambiar base de datos y migrar

            Crear login
                Mete usuario y pass, devuelve token, el token tiene informacion leerla y extraer los claims, la fecha de expiracion ext.
            Crear apartado de creacion de usuarios
            Crear un log de usuarios que se loguean con el deviceInfo
            
            Problemas Menores: 
            Swagger el icono del candado no serenderiza correctamente.


            Soluciones: recordar escribir las soluciones aca
            Buscar un DataAnnotation que tome cualquier excepcion y ejecute la misma funcion.
            Agregar los summary a los dbcontext y a los Factory de los dbcontext

            Swagger -> Falta modelos de salida

            EF INTENTAR CREAR UN REPOSITORIO/FACTORY PARA LOS DB CONTEXT E INYECTAR LA DEPENDENCIA -> LOS DBCONTEXT SIEMPRE EJECUTAR DISPOSING SON INYECCION SCOPE O TRANSENT
            EF Los metodos que de GetUserGroups - GetGroupUsers - GetGroupRoles tiene que traer la información justa.
            EF Agregar al startup el metodo que verifique que la base existe y sino que la cree. Despues habria que ver si existe un metodo similar para crear un base de datos adicional o actualizar la actual.
            
            Try catch verificar el funcionamiento del annotation , eliminar la base de datos para simular falla.
            Mapear Modelo de MVC A Negocio.
";
    }

}