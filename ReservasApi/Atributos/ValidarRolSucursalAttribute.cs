﻿using Identity.DbContexts;
using Identity.Entities;
using Identity.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Reservas.DbContexts;
using Reservas.Entidades.Empresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.Atributos
{
    /// <summary>
    /// Este atributo debe ser colocado encima de cada enpoint donde se requiera validacion adicional.
    /// </summary>
    public class ValidarRolSucursalAttribute : AuthorizeAttribute, IAsyncAuthorizationFilter
    {

        // Necesito obtener la sucursal de la URL
        // Necesito obtener el rol al que accedio (o al action y yo genero el nombre de rol)
        // Como valido. Si el rol requiere validar sucurral (hayQueValidarSucursal = true) y en la tabla Grupos/Sucursal/Usuario existe el rol para esa sucursal listo.
        // Si valido no ok, devuelvo un 403 Fobitten
        public async Task OnAuthorizationAsync(AuthorizationFilterContext authorizationFilterContext)
        {

            RoleManager<Microsoft.AspNetCore.Identity.IdentityRole> roleManager = authorizationFilterContext.HttpContext.RequestServices.GetService<RoleManager<Microsoft.AspNetCore.Identity.IdentityRole>>();
            UserManager<ApplicationUser> userManager = authorizationFilterContext.HttpContext.RequestServices.GetService<UserManager<ApplicationUser>>();
            EmpresaDbContext empresaDbContext = authorizationFilterContext.HttpContext.RequestServices.GetService<EmpresaDbContext>();
            ApplicationGroupDbContext applicationGroupDbContext = authorizationFilterContext.HttpContext.RequestServices.GetService<ApplicationGroupDbContext>();
            ApplicationGroupManager groupManager = new ApplicationGroupManager(userManager, roleManager, new ApplicationGroupStore(applicationGroupDbContext), applicationGroupDbContext);

            bool tieneElAtributoAllowAnonymous = false;

            string controllerName = (authorizationFilterContext.ActionDescriptor as ControllerActionDescriptor).ControllerTypeInfo.Name;
            string identificadorSucursal = (string)authorizationFilterContext.HttpContext.GetRouteData().Values["sucursal"];
            //IList<string> rolesAuthorizeAttribute =
            //    authorizationFilterContext.ActionDescriptor.EndpointMetadata
            //        .Where(metadata => metadata.GetType() == typeof(AuthorizeAttribute))
            //        .Select(metadata => (metadata as AuthorizeAttribute).Roles)
            //        .Where(roleName => roleName != controllerName)
            //    .ToList<string>();

            IList<string> rolesAuthorizeAttribute = new List<string>();

            IList<string> rolesAuthorizeAttributeSucios = (authorizationFilterContext.ActionDescriptor as ControllerActionDescriptor).MethodInfo.GetCustomAttributes(inherit: true).Where(customAttribute => customAttribute.GetType() == typeof(AuthorizeAttribute)).Select(authorizeAttribute => (authorizeAttribute as AuthorizeAttribute).Roles).ToList();
            tieneElAtributoAllowAnonymous = (authorizationFilterContext.ActionDescriptor as ControllerActionDescriptor).MethodInfo.GetCustomAttributes(inherit: true).Any(customAttribute => customAttribute.GetType() == typeof(AllowAnonymousAttribute));

            if (tieneElAtributoAllowAnonymous)
            {
                return;
            }

            foreach (string masDeUnRol in rolesAuthorizeAttributeSucios)
            {
                if (!String.IsNullOrWhiteSpace(masDeUnRol))
                {
                    rolesAuthorizeAttribute = rolesAuthorizeAttribute.Concat(masDeUnRol.Replace(" ", "").Split(',')).ToList();
                }
            }

            Sucursal sucursal = await empresaDbContext.Sucursales.Where(s => s.Id.ToString() == identificadorSucursal || s.Nombre == identificadorSucursal).FirstOrDefaultAsync();
            ApplicationUser user = await userManager.Users.Include(u => u.GroupsUsersSucursales).FirstOrDefaultAsync(u => u.UserName == authorizationFilterContext.HttpContext.User.Identity.Name);
            IEnumerable<Microsoft.AspNetCore.Identity.IdentityRole> applicationRolesAuthorizeAttribute = await roleManager.Roles.Where(rol => rolesAuthorizeAttribute.Contains(rol.NormalizedName)).ToListAsync();

            if(applicationRolesAuthorizeAttribute.Count() != 1)
            {
                throw new Exception("Los endpoint a validar sucursal sol pueden tener un rol asignado");
            }

            Microsoft.AspNetCore.Identity.IdentityRole applicationRolAuthorizeAttribute = applicationRolesAuthorizeAttribute.ElementAt(0);

            List<ApplicationGroup> applicationGroupsAuthorizeAttribute = await groupManager.Groups.Where(g => 
                g.ApplicationRoles.Any(ar => Predicado(ar.ApplicationRoleId, applicationRolAuthorizeAttribute.Id)

            )).ToListAsync(); ;
            

            // Aproach, mejor conviene decir si entre aca es por que necesito validar sucursal y tambien quiere decir que ese rol tiene un grupo asociado y es grupo que validar sucursal contra el usuario.
            foreach (ApplicationGroup applicationGroup in applicationGroupsAuthorizeAttribute)
            {
                bool elUsuarioTieneAccesoAEstaSucursal = user.GroupsUsersSucursales.Any(gus => gus.ApplicationUserId == user.Id && gus.ApplicationGroupId == applicationGroup.Id && gus.SucursalId == sucursal.Id);
                bool elUsuarioTieneAccesoAEseRolParaEsaSucursalRequerida = elUsuarioTieneAccesoAEstaSucursal;
                if (elUsuarioTieneAccesoAEseRolParaEsaSucursalRequerida)
                {
                    return;
                }
            }

            // Valida si el grupo requiere validar sucursal, el problema es que hay grupos que no requieren y eso puede traer aparejado un problema de seguridad si se carga mal el grupo.
            //foreach (ApplicationGroup applicationGroup in applicationGroupsAuthorizeAttribute)
            //{
            //    bool esNecesarioValidarSucursalParaElRol = applicationGroup.HayQueValidarSucursal;
            //    bool existeAlMenosUnGrupoQueNoValideSucursal = !esNecesarioValidarSucursalParaElRol;

            //    if (existeAlMenosUnGrupoQueNoValideSucursal)
            //    {
            //        return;
            //    }

            //    IEnumerable<Sucursal> sucursalesParaUnRolYUnUsuario = await empresaDbContext.Sucursales.Where(s => user.GroupsUsersSucursales.Any(gus => gus.SucursalId == s.Id)).ToListAsync();

            //    //IEnumerable<Sucursal> sucursalesParaUnRolYUnUsuario = await FuncionesCompartidas.ObtenerSucursalesParaUnRolYUnUsuario(user, applicationGroup, empresaDbContext.Sucursales);
            //    bool elUsuarioTieneAccesoAEseRolParaEsaSucursalRequerida = sucursalesParaUnRolYUnUsuario.Any(s => s.Id == sucursal.Id);

            //    if (elUsuarioTieneAccesoAEseRolParaEsaSucursalRequerida)
            //    {
            //        return;
            //    }
            //}

            var statusForbittenParaUsuarioQueRequierenSucursalParaUnRol = new StatusCodeResult(StatusCodes.Status403Forbidden);
            authorizationFilterContext.Result = statusForbittenParaUsuarioQueRequierenSucursalParaUnRol;

        }

        private bool Predicado(string arId, string applicationRolAuthorizeAttributeId)
        {
            bool rta = arId == applicationRolAuthorizeAttributeId;
            return rta;
        }
    }
}