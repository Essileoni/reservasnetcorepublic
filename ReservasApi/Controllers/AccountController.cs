﻿
using AutoMapper;
using Common.Atributos;
using Common.Extensions;
using Common.Utilidades;
using Identity.DbContexts;
using Identity.Entities;
using Identity.Managers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Reservas.DbContexts;
using Reservas.Entidades.Empresa;
using ReservasApi.Extensions;
using ReservasApi.Models.Account;
using ReservasApi.Servicios.Interfaces;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;


namespace ReservasApi.Controllers
{
    [Produces("application/json")]
    [Route("api/auth/[controller]/[action]")]
    [ApiController]
#if DEBUG
    [AllowAnonymous]
#else
    [Authorize]
#endif
    [ApiExceptionFilter]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<Microsoft.AspNetCore.Identity.IdentityRole> _roleManager;
        private readonly EmpresaDbContext _empresaDbContext;
        private readonly IConfiguration _configuration;
        private readonly ILogger<AccountController> _logger;
        private readonly ApplicationGroupManager _groupManager;
        private readonly IMapper _mapper;


        private readonly string UserNotFoundMsg = "El usuario no existe";
        private readonly string UserGroupsNotFound = "No fué posible obtener los grupos del usuario";
        private readonly string RolesAreNull = "Los roles son nulos";
        private readonly List<string> GruposNoAdministrables = new List<string>()
                {
                    "AccountManager",
                    "AdministradorSucursal"
                };

        public AccountController(
          UserManager<ApplicationUser> userManager,
          SignInManager<ApplicationUser> signInManager,
          RoleManager<Microsoft.AspNetCore.Identity.IdentityRole> roleManager,
          EmpresaDbContext empresaDbContext,
          IConfiguration configuration,
          ILogger<AccountController> logger,
          IAccountParametersServices accountParametersServices,
          ApplicationGroupDbContext dbContext,
          IMapper mapper
        )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _empresaDbContext = empresaDbContext;
            _configuration = configuration;
            _logger = logger;
            _mapper = mapper;

            bool existeBase = ValidarSiExisteBaseEnProduccion();

#if !DEBUG
            if (!existeBase)
            {
                throw new Exception("La base de datos de autenticación no existe en producción, por favor creela y corra el script de creación");
            }
#else
            dbContext.Database.EnsureCreated();
#endif

            if (existeBase)
            {
                _groupManager = new ApplicationGroupManager(_userManager, _roleManager, new ApplicationGroupStore(dbContext), dbContext);

                if (accountParametersServices.FirstRun)
                {
                    // Actualizar Roles Por Reflection en la primera consulta a este controller.
                    var result = UpdateRolesPrivate().Result;
                    accountParametersServices.FirstRun = false;
                }
            }
        }


        /// <summary>
        /// Devuelve un token de autorizacion para un usuario y contraseña validos.
        /// </summary>
        /// <param name="username">Nombre de usuario</param>
        /// <param name="password">Contraseña</param>
        /// <returns>string</returns>
        [HttpPost]

        [AllowAnonymous]
        public async Task<IActionResult> Login([FromBody] LoginModel loginModel)
        {
            //try
            //{
            if (ModelState.IsValid)
            {
                var loginResult = await _signInManager.PasswordSignInAsync(loginModel.Username, loginModel.Password, isPersistent: false, lockoutOnFailure: false);

                if (!loginResult.Succeeded)
                {
                    return BadRequest("No fué posible loguearse al sistema");
                }

                var user = await _userManager.FindByNameAsync(loginModel.Username);

                if (user == null)
                {
                    throw new Exception(UserNotFoundMsg);
                }

                string token = await GetToken(user);
                return Ok("bearer " + token);
            }
            return BadRequest(ModelState);
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}


        }

        /// <summary>
        /// Verifica si el token de autorización es válido y a la vez genera uno nuevo.
        /// </summary>
        /// <returns>string</returns>
        [HttpPost]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize]
#endif
        public async Task<IActionResult> RefreshToken()
        {
            //try
            //{
            var user = await _userManager.FindByNameAsync(
                User.Identity.Name ??
                User.Claims.Where(c => c.Properties.ContainsKey("unique_name")).Select(c => c.Value).FirstOrDefault()
            );

            if (user == null)
            {
                throw new Exception(UserNotFoundMsg);
            }

            string token = await GetToken(user);
            return Ok("bearer " + token);
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}


        }



        /// <summary>
        /// Registra un usuario
        /// </summary>
        /// <param name="Username">Nombre de usuario</param>
        /// <param name="Password">Contraseña (8 caracteres min, al menos una mayuscula, al menos una minuscula, al menos un numero, un caracter especial ej: ,.!@#$%^&* )</param>
        /// <param name="Email">Email</param>
        /// <param name="FirstName">Nombres</param>
        /// <param name="LastName">Apellidos</param>
        /// <param name="PasswordConfirmation">Confirmacion de la contraseña</param>
        /// <param name="SucursalPredeterminadaId">Id de Sucursal</param>
        /// <returns>string</returns>
        [HttpPost]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerRegister")]        
#endif
        public async Task<IActionResult> Register([FromBody] RegisterModel registerModel)
        {
            //try
            //{
            // El modelo valida por anotation antes de el siguiente if.
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    //TODO: Use Automapper instaed of manual binding  

                    UserName = registerModel.Username,
                    FirstName = registerModel.FirstName,
                    LastName = registerModel.LastName,
                    Email = registerModel.Email,
                    SucursalPredeterminadaId = registerModel.SucursalPredeterminadaId
                };

                var identityResult = await this._userManager.CreateAsync(user, registerModel.Password);

                if (identityResult.Succeeded)
                {
                    await _signInManager.SignInAsync(user, isPersistent: false);
                    string token = await GetToken(user);
                    return Ok("Usuario Creado");
                }
                else
                {
                    return BadRequest(identityResult.Errors);
                }
            }
            return BadRequest(ModelState);
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}

        }

        /// <summary>
        /// Devuelve el listado de todos los usuarios del sistema
        /// </summary>
        /// <returns>Listado de Usuarios</returns>
        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetUsers")]
#endif

        public IActionResult GetUsers()
        {
            //try
            //{
            return Ok(GetUsersPrivate());
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }


        /// <summary>
        /// Devuelve el listado de todos los usuarios que no administran las sucursales
        /// </summary>
        /// <returns>Listado de Usuarios</returns>
        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetUsersParaAdministradorSucursal")]
#endif
        [ProducesResponseType(typeof(List<EditUserModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetUsersParaAdministradorSucursal()
        {
            List<ApplicationUser> usuarios = new List<ApplicationUser>();

            List<ApplicationUser> administradoresSucursales = await ObtenerAdministradoresSucursalesAsync();

            if (await EsAccountManagerAsync())
            {
                usuarios = await _userManager.Users.ToListAsync();
            }
            else if (await EsAdministradorSucursalAsync())
            {
                usuarios = await _userManager.Users
                    .Where(us => !administradoresSucursales.Any(ua => ua.Id == us.Id))
                    .ToListAsync();
            }

            return Ok(
                usuarios.Select(u => new EditUserModel()
                    {
                        Id = u.Id,
                        Email = u.Email,
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        Username = u.UserName,
                        SucursalPredeterminadaId = u.SucursalPredeterminadaId
                })
                );
        }

        /// <summary>
        /// Edita los datos de un usuario registrado
        /// </summary>
        /// <param name="Username"> Nombre de usuario registrado</param>
        /// <param name="Email"> Email </param>
        /// <param name="LastName"> Apellidos </param>
        /// <param name="FirstName"> Nombres </param>
        /// <param name="SucursalPredeterminadaId"> Id Sucursal </param>
        /// <returns>string</returns>
        [HttpPatch]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerEditUser")]
#endif
        public async Task<IActionResult> EditUser([FromBody] EditUserModel usuario)
        {
            //try
            //{
            var user = await _userManager.FindByNameAsync(usuario.Username);
            if (user == null)
            {
                throw new Exception(UserNotFoundMsg);
            }
            user.Email = usuario.Email;
            user.FirstName = usuario.FirstName;
            user.LastName = usuario.LastName;
            user.SucursalPredeterminadaId = usuario.SucursalPredeterminadaId;
            await _userManager.UpdateAsync(user);
            return Ok("El usuario fue actualizado con éxito");
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }

        /// <summary>
        /// Elimina el usuario registrado
        /// </summary>
        /// <param name="username"> Nombre de usuario registrado</param>
        /// <returns>string</returns>
        [HttpDelete]
        [Route("{username}")]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerDeleteUser")]
#endif
        public async Task<IActionResult> DeleteUser(string username)
        {
            //try
            //{
            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
            {
                throw new Exception(UserNotFoundMsg);
            }
            await _userManager.DeleteAsync(user);
            return Ok("El usuario fué eliminado con éxito");
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }

        /// <summary>
        /// Resetea la contraseña a un usuario
        /// </summary>
        /// <param name="Username">Nombre de usuario</param>
        /// <param name="Password">Contraseña (8 caracteres min, al menos una mayuscula, al menos una minuscula, al menos un numero, un caracter especial ej: ,.!@#$%^&* )</param>
        /// <returns>string</returns>
        [HttpPatch]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerResetPassword")]
#endif
        public async Task<IActionResult> ResetPassword([FromBody] ResetPasswordModel resetPasswordModel)
        {

            //try
            //{
            ApplicationUser user = _userManager.Users.FirstOrDefault(u => u.UserName == resetPasswordModel.Username);
            if (user == null)
            {
                throw new Exception(UserNotFoundMsg);
            }

            string token = await _userManager.GeneratePasswordResetTokenAsync(user);

            IdentityResult identityResult = await _userManager.ResetPasswordAsync(user, token, resetPasswordModel.Password);
            if (!identityResult.Succeeded)
            {
                throw new Exception(identityResult.Errors.GetStringErrors());
            }
            return Ok("La contraseña se reseteó con éxito");
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}

        }

        /// <summary>
        /// Cambia la contraseña de un usuario por una distinta sin envío de email.
        /// </summary>
        /// <param name="Username">Nombre de usuario</param>
        /// <param name="Password">Contraseña actual</param>
        /// <param name="NewPassword">Contraseña (8 caracteres min, al menos una mayuscula, al menos una minuscula, al menos un numero, un caracter especial ej: ,.!@#$%^&* )</param>
        /// <param name="NewPasswordConfirmation">Confirmacion de la contraseña nueva</param>
        /// <returns>string</returns>
        [HttpPatch]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerChangePassword")]
#endif
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordModel changePasswordModel)
        {

            //try
            //{
            if (changePasswordModel.NewPassword != changePasswordModel.NewPasswordConfirmation)
            {
                throw new Exception("La nueva contraseña y su confirmación no coincide");
            }

            if (changePasswordModel.Password == changePasswordModel.NewPassword)
            {
                throw new Exception("La nueva contraseña es identica a la anterior");
            }

            ApplicationUser user = _userManager.Users.FirstOrDefault(u => u.UserName == changePasswordModel.Username);

            if (user == null)
            {
                throw new Exception(UserNotFoundMsg);
            }

            IdentityResult identityResult = await _userManager.ChangePasswordAsync(user, changePasswordModel.Password, changePasswordModel.NewPassword);

            if (!identityResult.Succeeded)
            {
                throw new Exception(identityResult.Errors.GetStringErrors());
            }

            return Ok("Cambio de contraseña correcto");
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);

            //}

        }

        /// <summary>
        /// Agrega el nombre de roles a la base por cada action nuevo que se agreguó a cada controlador.
        /// </summary>
        /// <returns>string</returns>
        [HttpPost]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerUpdateRoles")]
#endif

        public async Task<IActionResult> UpdateRoles()
        {
            //try
            //{
            return Ok(await UpdateRolesPrivate());
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }

        /// <summary>
        /// Agrega roles al usuario
        /// </summary>
        /// <param name="Username">Nombre de usuario</param>
        /// <param name="Roles">Arreglo de roles para agregar.</param>
        /// <returns>string</returns>
        [HttpPost]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerAddRolesToUser")]        
#endif
        public async Task<IActionResult> AddRolesToUser(AddRolesToUserModel addRolesToUserModel)
        {
            //try
            //{
            if (addRolesToUserModel.Roles == null)
            {
                throw new Exception(RolesAreNull);
            }

            var identityUser = _userManager.Users.FirstOrDefault(u => u.UserName == addRolesToUserModel.Username);

            if (identityUser == null)
            {
                throw new Exception(UserNotFoundMsg);
            }

            foreach (string r in addRolesToUserModel.Roles)
            {
                var identityRole = await _roleManager.FindByNameAsync(r);

                if (identityRole == null)
                {
                    //await CreateRolePrivate(r);
                    //Lo roles no deben ser creados en este momento.
                }
            }

            //IEnumerable<string> roles = addRolesToUserModel.Roles.Select<RolModel,string>(r => r.nombre);
            IEnumerable<string> roles = addRolesToUserModel.Roles;

            IEnumerable<string> rolesExistentes = await GetRolesForUserPrivate(addRolesToUserModel.Username);
            IEnumerable<string> rolesRestantes = roles.Where(r => !rolesExistentes.Contains(r)).AsEnumerable();

            if (rolesRestantes.Count() == 0)
            {
                return Ok("El usuario ya tiene todos esos roles");
            }

            var identityResult = await _userManager.AddToRolesAsync(identityUser, rolesRestantes);

            if (!identityResult.Succeeded)
            {
                throw new Exception(identityResult.Errors.GetStringErrors());
            }

            return Ok("Los roles fueron agregados con éxito");
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}

        }

        /// <summary>
        /// Elimina roles del usuario
        /// </summary>
        /// <param name="Username">Nombre de usuario</param>
        /// <param name="Roles">Arreglo de roles para eliminar.</param>
        /// <returns>string</returns>
        [HttpDelete]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerRemoveRolesToUser")]
#endif
        public async Task<IActionResult> RemoveRolesToUser(AddRolesToUserModel removeRolesToUserModel)
        {
            //try
            //{
            if (removeRolesToUserModel.Roles == null)
            {
                throw new Exception(RolesAreNull);
            }

            var identityUser = _userManager.Users.FirstOrDefault(u => u.UserName == removeRolesToUserModel.Username);

            if (identityUser == null)
            {
                throw new Exception(UserNotFoundMsg);
            }

            foreach (string r in removeRolesToUserModel.Roles)
            {
                var identityRole = await _roleManager.FindByNameAsync(r);

                if (identityRole == null)
                {
                    //CreateRolePrivate(r);
                    // ^ No debe crearse un roll
                }
            }

            //IEnumerable<string> roles = addRolesToUserModel.Roles.Select<RolModel,string>(r => r.nombre);
            IEnumerable<string> roles = removeRolesToUserModel.Roles;

            IEnumerable<string> rolesExistentes = await GetRolesForUserPrivate(removeRolesToUserModel.Username);
            IEnumerable<string> rolesRestantes = roles.Where(r => rolesExistentes.Contains(r)).AsEnumerable();

            if (rolesRestantes.Count() == 0)
            {
                return Ok("El usuario no tienen ninguno de los roles asignados");
            }

            var identityResult = await _userManager.RemoveFromRolesAsync(identityUser, rolesRestantes);

            if (!identityResult.Succeeded)
            {
                throw new Exception(identityResult.Errors.GetStringErrors());
            }

            return Ok("Los roles fueron desafectados con éxito");
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}

        }

        /// <summary>
        /// Agrega un rol manualmente a la base.
        /// </summary>
        /// <param name="role">Nombre del rol a agregar</param>
        /// <returns>string</returns>
        [HttpPost]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerCreateRole")]
#endif
        public async Task<IActionResult> CreateRole(string role)
        {
            //try
            //{
            var resultado = await CreateRolePrivate(role);
            return Ok("Rol creado correctamente");
            //}
            //catch (Exception e)
            //{
            //    if (e.Message == "YAEXISTE")
            //    {
            //        return Ok("El rol ya existe en la base");
            //    }
            //    return GetExcepcionFormateada(e);
            //}

        }

        /// <summary>
        /// Devuelve el listado de todos los roles y su asignacion a los usuarios
        /// </summary>
        /// <param name="Username">Nombre del usuario</param>
        /// <returns>Listado de roles</returns>
        /// 
        [HttpGet]
        [Route("{UserName}")]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetRolesForUser")]
#endif
        public async Task<IActionResult> GetRolesForUser(string UserName)
        {
            //try
            //{
            return Ok(await GetRolesForUserPrivate(UserName));
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }

        private async Task<IList<string>> GetRolesForUserPrivate(string UserName)
        {
            //try
            //{
            var user = await _userManager.FindByNameAsync(UserName);

            if (user == null)
            {
                throw new Exception("El usuario no existe");
            }

            var roles = await _userManager.GetRolesAsync(user);
            roles = roles == null ? new List<string>() : roles;

            return roles;
            //}
            //catch (Exception e)
            //{

            //    throw e;
            //}

        }

        /// <summary>
        /// Devuelve el listado de todos los roles y su asignacion a los usuarios
        /// </summary>
        /// <param name="Username">Nombre del usuario</param>
        /// <returns>Listado de roles</returns>
        /// 
        [HttpGet]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetAllRoles")]        
#endif
        public IActionResult GetAllRoles()
        {
            //try
            //{
            return Ok(GetAllRolesPrivate());
            //}
            //catch (Exception e)
            //{

            //    return GetExcepcionFormateada(e);
            //}

        }

        /// <summary>
        /// Devuelve el listado de grupos asignables según el grupo al que pertenece el usuario.
        /// </summary>
        /// <returns>Listado de grupos</returns>
        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetGroupsParaAdministradorSucursal")]
#endif
        [ProducesResponseType(typeof(List<GroupModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetGroupsParaAdministradorSucursal()
        {
            List<ApplicationGroup> grupos = new List<ApplicationGroup>();

            if (await EsAccountManagerAsync())
            {
                grupos = await _groupManager.Groups.ToListAsync();
            }
            else if (await EsAdministradorSucursalAsync())
            {
                grupos = await _groupManager.Groups
                    .Where(g => !GruposNoAdministrables.Any(gna => gna.ToLower() == g.Name.ToLower()))
                    .ToListAsync();
            }

            return Ok(
                grupos.Select(g => new GroupModel()
                {
                    Id = g.Id,
                    Description = g.Description,
                    Name = g.Name
                })
            );
        }




        /// <summary>
        /// Devuelve el listado completo de los grupos.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetAllGroups")]        
#endif
        public IActionResult GetAllGroups()
        {
            //try
            //{
            var result = _groupManager.Groups;
            if (result == null)
            {
                throw new Exception("No fué posible obtener la lista de grupos");
            }
            return Ok(result);
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }




        /// <summary>
        /// Crea un nuevo grupo sin roles vinculados. Un grupo relaciona roles con usuarios.
        /// </summary>
        /// <param name="Name"> Nombre del grupo</param>
        /// <param name="Descripcion"> Leve descripción de las responsabilidades del grupo</param>
        /// <returns>String</returns>
        [HttpPost]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerCreateGroup")]
#endif
        public async Task<IActionResult> CreateGroup(CreateGroupModel group)
        {
            //try
            //{
            var result = await _groupManager.CreateGroupAsync(group.Name, group.Description);
            if (!result.Succeeded)
            {
                throw new Exception(String.Concat(result.Errors));
            }
            return Ok("El grupo fué creado con exito");
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }

        /// <summary>
        /// Vincular una lista de roles con un grupo. Luego esos roles con sus usuarios. 
        /// </summary>
        /// <param name="GroupId">Id del grupo a vincular</param>
        /// <param name="Roles">Arreglo de nombre de roles</param>
        /// <returns>string</returns>
        [HttpPost]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerSetRolesToGroup")]
#endif
        public async Task<IActionResult> SetRolesToGroup(SetRolesToGroupModel setRolesToGroupModel)
        {
            //try
            //{
            var result = await _groupManager.SetGroupRolesAsync(setRolesToGroupModel.GroupId, setRolesToGroupModel.Roles);
            if (!result.Succeeded)
            {
                throw new Exception(String.Concat(result.Errors));
            }
            return Ok("Se agregaron todos los roles al grupo");
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }


        /// <summary>
        /// Vincula una lista de grupos con un usuario.
        /// </summary>
        /// <param name="GroupsIDs">Arreglo de IDs de grupos</param>
        /// <param name="Username">Nombre Usuario</param>
        /// <returns>string</returns>
        [HttpPost]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerSetUserGroups")]
#endif
        public async Task<IActionResult> SetUserGroups(SetUserGroupsModel setUserGroupsModel)
        {
            //try
            //{
            var identityUser = _userManager.Users.FirstOrDefault(u => u.UserName == setUserGroupsModel.Username);

            if (identityUser == null)
            {
                throw new Exception(UserNotFoundMsg);
            }

            var result = await _groupManager.SetUserGroupsAsync(identityUser.Id, setUserGroupsModel.GroupsIDs);
            if (!result.Succeeded)
            {
                throw new Exception("No fué posible asignar uno o más grupos al usuario");
            }
            return Ok("Se agregó el usuario a el/los grupos");
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }

        [HttpPatch]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerEditGroup")]
#endif
        public async Task<IActionResult> EditGroup(ApplicationGroup group)
        {
            //try
            //{
            var result = await _groupManager.UpdateGroupAsync(group);
            if (!result.Succeeded)
            {
                throw new Exception("No fué posible moditifcar el grupo");
            }
            return Ok("El grupo se modificó satisfactoriamente.");
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }


        /// <summary>
        /// Elimina un grupo y todas sus vinculaciones con usuarios y roles.
        /// </summary>
        /// <param name="Id"> Id del grupo a eliminar</param>
        /// <returns>string</returns>
        [HttpDelete]
        [Route("{Id}")]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerDeleteGroup")]
#endif
        public async Task<IActionResult> DeleteGroup(string Id)
        {
            //try
            //{
            var result = await _groupManager.DeleteGroupAsync(Id);
            if (!result.Succeeded)
            {
                throw new Exception("No fué posible eliminar el grupo");
            }
            return Ok("El grupo fué eliminado correctamente");

            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }

        /// <summary>
        /// Obtiene la listas de Grupos a los que pertence un usuario
        /// </summary>
        /// <param name="userName">Nombre del usuario</param>
        /// <returns>Array de grupos</returns>
        [HttpGet]
        [Route("{userName}")]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetUserGroups")]
#endif
        public async Task<IActionResult> GetUserGroups(string userName)
        {
            //try
            //{
            return Ok(await GetUserGroupsByNamePrivate(userName));

            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }

        /// <summary>
        /// Obtiene el usuario logueado
        /// </summary>
        /// <returns></returns>
        [HttpGet]

#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetUserLogin")]
#endif
        public async Task<IActionResult> GetUserLogin()
        {
            //try
            //{
            var identityUser = await GetUserLogin(_userManager);

            if (identityUser == null)
            {
                throw new Exception(UserNotFoundMsg);
            }

            return Ok(identityUser);

            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }



        /// <summary>
        /// Obtiene la listas de Grupos a los que pertence un usuario
        /// </summary>
        /// <param name="GroupId">Id de grupo</param>
        /// <returns>Array de grupos</returns>
        [HttpGet]
        [Route("{GroupId}")]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetGroupRoles")]
#endif
        public async Task<IActionResult> GetGroupRoles(string GroupId)
        {
            //try
            //{
            var result = await _groupManager.GetGroupRolesAsync(GroupId);
            if (result == null)
            {
                throw new Exception("No fué posible obtener los roles del grupo");
            }
            return Ok(result.Select<Microsoft.AspNetCore.Identity.IdentityRole, string>(r => r.Name));

            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }


        /// <summary>
        /// Obtiene la lista de usuarios del Grupo.
        /// </summary>
        /// <param name="GroupId">Id del grupo</param>
        /// <returns>Array de usuarios</returns>
        [HttpGet]
        [Route("{GroupId}")]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetGroupUsers")]
#endif
        public async Task<IActionResult> GetGroupUsers(string GroupId)
        {
            //try
            //{
            var result = await _groupManager.GetGroupUsersAsync(GroupId);
            if (result == null)
            {
                throw new Exception("No fué posible obtener los usuarios del grupo");
            }
            return Ok(result.Select<ApplicationUser, EditUserModel>(r => new EditUserModel() { Id = r.Id, Username = r.UserName, FirstName = r.FirstName, LastName = r.LastName, Email = r.Email, SucursalPredeterminadaId = r.SucursalPredeterminadaId }));
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }

        /// <summary>
        /// Devuelve el grupo para el id indicado
        /// </summary>
        /// <param name="Id">Id del grupo</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{GroupId}")]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetGroup")]
#endif
        public async Task<IActionResult> GetGroup(string GroupId)
        {
            //try
            //{
            var result = await _groupManager.FindByIdAsync(GroupId);
            if (result == null)
            {
                throw new Exception("No fué posible obtener el grupo");
            }
            return Ok(result);
            //}
            //catch (Exception e)
            //{
            //    return GetExcepcionFormateada(e);
            //}
        }

        //        [HttpGet]
        //        [Route("{groupName}")]
        //#if DEBUG
        //        [AllowAnonymous]
        //#else
        //        [Authorize(Roles = "AccountControllerBuscarSucursalesParaUnGrupo")]
        //#endif
        //        public async Task<IActionResult> BuscarSucursalesParaUnGrupo(string groupName)
        //        {
        //            ApplicationUser usuario = await ObtenerUsuarioLogueado();
        //            ApplicationGroup grupo = await ObtenerGrupoPorSuNombre(groupName);
        //            bool elUsuarioLogueadoPertenceAlGrupo = await ElUsuarioLogueadoPertenceAlGrupo(groupName);
        //            if (elUsuarioLogueadoPertenceAlGrupo)
        //            {
        //                IEnumerable<Sucursal> sucursales = await ObtenerSucursalesParaUnUsuarioGrupo(usuario.Id, grupo.Id);
        //                return Ok(_mapper.Map<List<SucursalModel>>(sucursales));
        //            }

        //            return Ok(new List<string>() { });
        //        }

        //        [HttpGet]
        //#if DEBUG
        //        [AllowAnonymous]
        //#else
        //        [Authorize(Roles = "AccountControllerObtenerSucursalesParaAdministrador")]
        //#endif
        //        public async Task<IActionResult> ObtenerSucursalesParaAdministrador()
        //        {
        //            ApplicationUser usuario = await ObtenerUsuarioLogueado();
        //            IEnumerable<ApplicationGroup> gruposUsuario = await GetUserGroupsByNamePrivate(usuario.UserName);

        //            bool esUsuarioAccountManager = gruposUsuario.Any(g => g.Name.ToLower() == "AccountManager".ToLower());
        //            if (esUsuarioAccountManager)
        //            {
        //                return Ok(_mapper.Map<List<SucursalModel>>(_empresaDbContext.Sucursales.ToList()));
        //            }

        //            ApplicationGroup grupoAdministradorSucursal = gruposUsuario.FirstOrDefault(g => g.Name.ToLower() == "AdministradorSucursal".ToLower());
        //            bool esUsuarioAdministradorSucursal = grupoAdministradorSucursal != null;
        //            if (esUsuarioAdministradorSucursal)
        //            {
        //                IEnumerable<Sucursal> sucursales = await ObtenerSucursalesParaUnUsuarioGrupo(usuario.Id, grupoAdministradorSucursal.Id);
        //                return Ok(_mapper.Map<List<SucursalModel>>(sucursales.ToList()));
        //            }

        //            throw new Exception("El usuario no tiene sucursales para administrar");
        //        }

        //        [HttpGet]
        //#if DEBUG
        //        [AllowAnonymous]
        //#else
        //        [Authorize(Roles = "AccountControllerObtenerGruposParaAdministradorSucursal")]
        //#endif
        //        public async Task<IActionResult> ObtenerGruposParaAdministradorSucursal()
        //        {
        //            ApplicationUser usuario = await ObtenerUsuarioLogueado();
        //            IEnumerable<ApplicationGroup> gruposUsuario = await GetUserGroupsByNamePrivate(usuario.UserName);

        //            bool esUsuarioAccountManager = gruposUsuario.Any(g => g.Name.ToLower() == "AccountManager".ToLower());
        //            if (esUsuarioAccountManager)
        //            {
        //                return Ok(_mapper.Map<List<GroupModel>>(_groupManager.Groups.ToList()));
        //            }

        //            ApplicationGroup grupoAdministradorSucursal = gruposUsuario.FirstOrDefault(g => g.Name.ToLower() == "AdministradorSucursal".ToLower());
        //            bool esUsuarioAdministradorSucursal = grupoAdministradorSucursal != null;
        //            if (esUsuarioAdministradorSucursal)
        //            {

        //                return Ok(_mapper.Map<List<GroupModel>>(_groupManager.Groups.Where(g => !GruposNoAdministrables.Any(gna => g.Name.ToLower() == gna.ToLower())).ToList()));
        //            }

        //            throw new Exception("El usuario no tiene grupos para administrar");
        //        }



        /// <summary>
        /// Devuelve un listado de sucursales correspondiente para un usuario y un rol
        /// </summary>
        /// <returns>Listado de Sucursales</returns>
        [HttpGet]
        [Route("{roleName}")]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetSucursalesParaUnRol")]
#endif
        [ProducesResponseType(typeof(List<Sucursal>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetSucursalesParaUnRolNotLoading(string roleName)
        {
            return await GetSucursalesParaUnRol(roleName);
        }

        /// <summary>
        /// Devuelve un listado de sucursales correspondiente para un usuario y un rol
        /// </summary>
        /// <returns>Listado de Sucursales</returns>
        [HttpGet]
        [Route("{roleName}")]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetSucursalesParaUnRol")]
#endif
        [ProducesResponseType(typeof(List<Sucursal>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetSucursalesParaUnRol(string roleName)
        {
            List<Sucursal> sucursales = await GetSucursalesParaUnUsuarioAsync(roleName);
            return Ok(
                sucursales.Select(s => new SucursalModel()
                {
                    Id = s.Id,
                    Nombre = s.Nombre
                })
            );
        }

        private async Task<List<Sucursal>> GetSucursalesParaUnUsuarioAsync(string roleName)
        {
            List<Sucursal> sucursales = new List<Sucursal>();

            ApplicationUser usuarioLogueado = await ObtenerUsuarioLogueado();

            if (usuarioLogueado != null)
            {
                Microsoft.AspNetCore.Identity.IdentityRole role = await _roleManager.FindByNameAsync(roleName);

                IEnumerable<ApplicationGroup> gruposUsuarioLogueado = await _groupManager.GetUserGroupsAsync(usuarioLogueado.Id);

                IEnumerable<ApplicationGroup> gruposParaUnRole = await _groupManager.Groups.Where(g => g.ApplicationRoles.Any(ar => ar.ApplicationRoleId == role.Id)).ToListAsync();
                
                IEnumerable<ApplicationGroup> gruposInterseccion = gruposUsuarioLogueado.Where( gul => gruposParaUnRole.Any(gprn => gprn.Id == gul.Id)).ToList();

                HashSet<GroupUserSucursal> groupsUsersSucursales = usuarioLogueado.GroupsUsersSucursales.Where(gus => gruposInterseccion.Any(gi => gi.Id == gus.ApplicationGroupId)).ToHashSet();

                sucursales = await _empresaDbContext.Sucursales.Where(s => groupsUsersSucursales.Any(gus => gus.SucursalId == s.Id)).ToListAsync();

            }
                       
            return sucursales;
        }

        /// <summary>
        /// Devuelve el listado de sucursales correspondiente para un administrador de sucursal
        /// </summary>
        /// <returns>Listado de sucursales</returns>
        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerGetSucursalesParaAdministradorSucursal")]
#endif
        [ProducesResponseType(typeof(List<SucursalModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetSucursalesParaAdministradorSucursal()
        {
            List<Sucursal> sucursales = await GetSucursalesParaAdministradorSucursalPrivateAsync();
            return Ok(
                    sucursales.Select(s => new SucursalModel()
                    {
                        Id = s.Id,
                        Nombre = s.Nombre
                    })
                );
        }


        /// <summary>
        /// Asigna a un usuario permisos para una sucursal y un grupo.
        /// </summary>
        /// <param name="ListadoSucursalGrupoUsuario"></param>
        /// <returns></returns>
        [HttpPost]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "AccountControllerAsignarSucursalesGruposUsuarios")]
#endif
        [ProducesResponseType(typeof(string) ,StatusCodes.Status200OK)]
        public async Task<IActionResult> AsignarSucursalesGruposUsuarios(List<SucursalGrupoUsuarioModel> ListadoSucursalGrupoUsuario)
        {
            HashSet<string> MensajesErrores = new HashSet<string>();
            List<Sucursal> sucursalesValidas = await GetSucursalesParaAdministradorSucursalPrivateAsync();

            //Recorrer uno a uno los permisos
            foreach (SucursalGrupoUsuarioModel sucursalGrupoUsuarioModel in ListadoSucursalGrupoUsuario)
            {
                // Validar Guid
                Guid fakeOutput = new Guid();
                bool losGUIDsonValidos = Guid.TryParse(sucursalGrupoUsuarioModel.ApplicationGroupId, out fakeOutput) && Guid.TryParse(sucursalGrupoUsuarioModel.ApplicationUserId, out fakeOutput);
                if (!losGUIDsonValidos)
                {
                    MensajesErrores.Add(" Advertencia: Algún grupo o usuario tiene un formato incorrecto");
                    continue;
                }

                // Validar existen usuario sucursal y grupo
                ApplicationUser usuario = await _userManager.Users.Include(u => u.GroupsUsersSucursales).FirstOrDefaultAsync( u => u.Id == sucursalGrupoUsuarioModel.ApplicationUserId);
                ApplicationGroup grupo = await _groupManager.FindByIdAsync(sucursalGrupoUsuarioModel.ApplicationGroupId);
                Sucursal sucursal = await _empresaDbContext.Sucursales.FirstOrDefaultAsync(s => s.Id == sucursalGrupoUsuarioModel.SucursalId);

                if (usuario == null || grupo == null || sucursal == null)
                {
                    MensajesErrores.Add(" Advertencia: Algún grupo, usuario, o sucursal no existe");
                    continue;
                }


                // Validar si el usuario actual puede administrar esa sucursal.
                bool puedeAdministrarLaSucursal = sucursalesValidas.Any(s => s.Id == sucursal.Id);
                if (!puedeAdministrarLaSucursal)
                {
                    MensajesErrores.Add(" Advertencia: Alguna sucursal no se puede administrar");
                    continue;
                }

                // Validar si el usuario pertenece al grupo.
                IEnumerable<ApplicationGroup> gruposUsuario = await _groupManager.GetUserGroupsAsync(usuario.Id);
                bool elUsuarioPerteneceAlGrupo = gruposUsuario.Any(gu => gu.Id == sucursalGrupoUsuarioModel.ApplicationGroupId);
                bool esRelevantePertencerAlGrupo = sucursalGrupoUsuarioModel.Activar.HasValue && sucursalGrupoUsuarioModel.Activar.Value;
                if (esRelevantePertencerAlGrupo && !elUsuarioPerteneceAlGrupo)
                {
                    MensajesErrores.Add(" Advertencia: Algún usuario no pertenece al grupo solicitado");
                    continue;
                }

                // Validar si ya tiene los permisos
                bool elUsuarioTieneEseGrupoSucursal = usuario.GroupsUsersSucursales.Any(gus => 
                    gus.ApplicationUserId == sucursalGrupoUsuarioModel.ApplicationUserId && 
                    gus.ApplicationGroupId == sucursalGrupoUsuarioModel.ApplicationGroupId && 
                    gus.SucursalId == sucursalGrupoUsuarioModel.SucursalId
                );

                if (!sucursalGrupoUsuarioModel.Activar.HasValue)
                {
                    MensajesErrores.Add(" Advertencia: Alguna Asignacion Tiene Un Estado Activar Nulo");
                    continue;
                }
                else if (sucursalGrupoUsuarioModel.Activar.Value)
                {
                    if (elUsuarioTieneEseGrupoSucursal)
                    {
                        MensajesErrores.Add(" Advertencia: Algún usuario ya tenia asignado la sucursal para un grupo"); 
                        continue;
                    }

                    await _groupManager.CreateGrupoUsuarioSucursalAsync(sucursal.Id, usuario.Id, grupo.Id);

                    //Crear los permisos
                    //usuario.GroupsUsersSucursales.Add(new GroupUserSucursal() { 
                    //    ApplicationGroupId = usuario.Id,
                    //    ApplicationUserId = grupo.Id,
                    //    SucursalId = sucursal.Id,
                    //});

                    //await _userManager.UpdateAsync(usuario);

                }
                else if (!sucursalGrupoUsuarioModel.Activar.Value)
                {
                    if (!elUsuarioTieneEseGrupoSucursal)
                    {
                        MensajesErrores.Add(" Advertencia: Algún usuario no tenia asignada la sucursal para algún grupo"); 
                        
                        continue;
                    }

                    await _groupManager.RemoveGrupoUsuarioSucursalAsync(sucursal.Id, usuario.Id, grupo.Id);

                    //GroupUserSucursal groupUserSucursalAEliminar = usuario.GroupsUsersSucursales.FirstOrDefault(gus => gus.ApplicationUserId == usuario.Id && gus.ApplicationGroupId == grupo.Id && gus.SucursalId == sucursal.Id);

                    //usuario.GroupsUsersSucursales.Remove(groupUserSucursalAEliminar);

                    //await _userManager.UpdateAsync(usuario);
                    //Eliminar los permisos
                }

            }

            



            if (MensajesErrores.Count() > 0)
            {
                return Ok("Se modificaron algunos de los permisos, pero no todos." + String.Join("", MensajesErrores));
            }

            return Ok("Se modificaron todos los permisos solicitados.");
        }


#if DEBUG
        /// <summary>
        /// Se utiliza para realizar test para la nueva entidad GroupUsersSucursales
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> TestGroupUsersSucursales()
        {
            var user = await _userManager.Users.Include(u => u.GroupsUsersSucursales).FirstOrDefaultAsync(u => u.UserName.ToLower() == User.Identity.Name.ToLower());
            var grupo = await _groupManager.Groups.Include(g => g.GroupsUsersSucursales).FirstOrDefaultAsync(g => g.Name.ToLower() == "AccountManager".ToLower());
            bool tieneElGrupo = (await _groupManager.GetUserGroupsAsync(user.Id)).Any(g => g.Name == grupo.Name);
            var sucursal = _empresaDbContext.Sucursales.FirstOrDefaultAsync(s => s.Id == grupo.GroupsUsersSucursales.First().SucursalId);
            var gsuU = user.GroupsUsersSucursales.First();
            var gsuG = grupo.GroupsUsersSucursales.First();
            var sonIguales = gsuU == gsuG;

            return Ok(user);

        }
#endif

#region Privates Methods

        /// <summary>
        /// Devuelve un grupo a partir de nombre
        /// </summary>
        /// <param name="groupName">Nombre del grupo</param>
        /// <returns></returns>
        private async Task<ApplicationGroup> ObtenerGrupoPorSuNombre(string groupName)
        {
            ApplicationGroup group = await _groupManager.Groups.Include(g => g.GroupsUsersSucursales).FirstOrDefaultAsync(g => g.Name.ToLower() == groupName.ToLower());

            if (group == null)
            {
                throw new Exception("El grupo no existe");
            }

            return group;
        }

        /// <summary>
        /// Devuelve el usuario logueado solo si existe un token
        /// </summary>
        /// <returns>Usuario Logueado</returns>
        private async Task<ApplicationUser> ObtenerUsuarioLogueado()
        {
            ApplicationUser user = await _userManager.Users.Include(u => u.GroupsUsersSucursales).FirstOrDefaultAsync(u => u.UserName.ToLower() == User.Identity.Name.ToLower());

            if (user == null)
            {
                throw new Exception(UserNotFoundMsg);
            }

            return user;
        }

        /// <summary>
        /// Devuelve el listado de grupo asociados a un usuario.
        /// </summary>
        /// <param name="userName">Nombre del usuario</param>
        /// <returns></returns>
        private async Task<IEnumerable<ApplicationGroup>> GetUserGroupsByNamePrivate(string userName)
        {
            ApplicationUser user = _userManager.Users.FirstOrDefault(u => u.UserName == userName);

            if (user == null)
            {
                throw new Exception(UserNotFoundMsg);
            }

            IEnumerable<ApplicationGroup> userGroups = await _groupManager.GetUserGroupsAsync(user.Id);

            if (userGroups == null)
            {
                throw new Exception(UserGroupsNotFound);
            }

            return userGroups;
        }

        /// <summary>
        /// Valida si el usuario logueado pertenece a un grupo.
        /// </summary>
        /// <param name="grupoName">Nombre del grupo</param>
        /// <returns></returns>
        private async Task<bool> ElUsuarioLogueadoPertenceAlGrupo(string grupoName)
        {
            ApplicationUser usuario = await ObtenerUsuarioLogueado();
            IEnumerable<ApplicationGroup> gruposParaUsuario = await _groupManager.GetUserGroupsAsync(usuario.Id);
            return gruposParaUsuario.Any(g => g.Name.ToLower() == grupoName.ToLower());
        }

        /// <summary>
        /// Devuelve para un usuario las sucursales para un grupo conocido.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        private async Task<IEnumerable<Sucursal>> ObtenerSucursalesParaUnUsuarioGrupo(string userId, string groupId)
        {
            ApplicationUser user = await _userManager.Users.Include(u => u.GroupsUsersSucursales).FirstOrDefaultAsync(u => u.Id == userId);
            List<GroupUserSucursal> groupUserList = user.GroupsUsersSucursales.Where(gus => gus.ApplicationGroupId == groupId && userId == gus.ApplicationUserId).ToList();
            List<Sucursal> sucursales = await _empresaDbContext.Sucursales
                .Where(s => groupUserList.Select(gu => gu.SucursalId).Any(sId => s.Id == sId)
                ).ToListAsync();
            return sucursales;
        }

        /// <summary>
        /// Genera el token para un usuario
        /// </summary>
        /// <param name="user"></param>
        /// <returns>bearer token</returns>
        private async Task<string> GetToken(ApplicationUser user)
        {

            var utcNow = DateTime.UtcNow;

            using (RSA privateRsa = RSA.Create())
            {
                //try
                //{
                privateRsa.FromXmlFile(Path.Combine(Directory.GetCurrentDirectory(),
                    "Keys",
                     this._configuration.GetValue<String>("Tokens:PrivateKey")
                     ));
                var privateKey = new RsaSecurityKey(privateRsa);
                SigningCredentials signingCredentials = new SigningCredentials(privateKey, SecurityAlgorithms.RsaSha256);

                ICollection<Claim> claims = new List<Claim>()
                        {
                        new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                        new Claim(JwtRegisteredClaimNames.UniqueName, user.UserName),
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                        new Claim(JwtRegisteredClaimNames.Iat, utcNow.ToString()),
                        new Claim("sucursalPredeterminadaId", user.SucursalPredeterminadaId.ToString())
                        };

                // El token no sabe por si mismo que roles tiene el usuario
                var roles = await _userManager.GetRolesAsync(user);
                roles.ToList().ForEach(r => claims.Add(new Claim(ClaimTypes.Role, r)));

                //var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.configuration.GetValue<String>("Tokens:Key")));
                //var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

                var jwt = new JwtSecurityToken(
                    signingCredentials: signingCredentials,
                    claims: claims.AsEnumerable(),
                    notBefore: utcNow,
                    expires: utcNow.AddSeconds(this._configuration.GetValue<int>("Tokens:Lifetime")),
                    audience: this._configuration.GetValue<String>("Tokens:Audience"),
                    issuer: this._configuration.GetValue<String>("Tokens:Issuer")
                    );

                return new JwtSecurityTokenHandler().WriteToken(jwt);
                //}
                //catch (Exception e)
                //{
                //    throw e;
                //}


            }

        }

        /// <summary>
        /// Devuelve un listado de todos los usuarios
        /// </summary>
        /// <returns>Listado de Usuarios</returns>
        private IEnumerable<EditUserModel> GetUsersPrivate()
        {
            //try
            //{
            return _userManager.Users.Select(u => new EditUserModel()
            {
                Id = u.Id,
                Username = u.UserName,
                Email = u.Email,
                FirstName = u.FirstName,
                LastName = u.LastName,
                SucursalPredeterminadaId = u.SucursalPredeterminadaId
            }).ToList();
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}
        }

        /// <summary>
        /// Actualiza el listado de todos los roles generados a partir de reflection.
        /// </summary>
        /// <returns></returns>
        private async Task<string> UpdateRolesPrivate()
        {
            try
            {
                IEnumerable<string> rolesReflection = GetControllersRolesNames();
                IEnumerable<string> rolesRegistrados = GetAllRolesPrivate();
                IEnumerable<string> rolesRestantes = rolesReflection.Where(r => !rolesRegistrados.Contains(r)).ToList();
                if (rolesRestantes.Count() > 0)
                {
                    await CreateRolesPrivate(rolesRestantes);
                    return "Se actualizaron todos los roles";
                }
                return "No existen nuevos roles";
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Devuelve un listado generado a partir de Reflection.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<string> GetControllersRolesNames()
        {
            //try
            //{
            //Assembly assem1 = Assembly.Load("ReservasApi");
            Assembly assem1 = Assembly.GetExecutingAssembly();

            //Get List of Class Name
            Type[] types = assem1.GetTypes();

            IList<string> roles = new List<string>();

            foreach (Type tc in types)
            {
                string module = Assembly.GetExecutingAssembly().FullName.Split(',')[0];
                string restoNameSpace = tc.Namespace.Replace(module + ".", "");

                if (restoNameSpace == "Controllers" && tc.IsPublic)
                {
                    string controllerName = tc.Name;
                    roles.Add(controllerName);
                    //Get List of Method Names of Class
                    MemberInfo[] methodName = tc.GetMethods();
                    foreach (MemberInfo method in methodName)
                    {
                        CustomAttributeData[] atributos = method.CustomAttributes.Where(
                                a => a.AttributeType.FullName.Contains("Microsoft.AspNetCore.Mvc.Http") ||
                                a.AttributeType.FullName.Contains("Microsoft.AspNetCore.Mvc.RouteAttribute") ||
                                a.AttributeType.FullName.Contains("Microsoft.AspNetCore.Authorization")
                            ).ToArray();

                        if (atributos.Count() > 0 && method.ReflectedType.IsPublic)
                        {
                            roles.Add(controllerName + method.Name);
                        }
                    }
                }
            }
            return roles;
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}
        }

        /// <summary>
        /// Crear un rol a partir de un nombre
        /// </summary>
        /// <param name="role"></param>
        /// <returns>Resultado IdentityResult</returns>
        private async Task<IdentityResult> CreateRolePrivate(string role)
        {
            //try
            //{
            var identityRole = await _roleManager.FindByNameAsync(role);
            IdentityResult idtRes = null;
            if (identityRole == null)
            {
                identityRole = new Microsoft.AspNetCore.Identity.IdentityRole(role);
                idtRes = await _roleManager.CreateAsync(identityRole);
                if (!idtRes.Succeeded)
                {
                    throw new Exception(idtRes.Errors.GetStringErrors());
                }
            }
            else
            {
                throw new Exception("El Rol Ya Existe");
            }
            return idtRes;
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}
        }

        /// <summary>
        /// Crea Roles a partir de un listado
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        private async Task CreateRolesPrivate(IEnumerable<string> roles)
        {
            //try
            //{
            List<IdentityResult> results = new List<IdentityResult>();

            foreach (var roleName in roles)
            {
                var roleExist = await _roleManager.RoleExistsAsync(roleName);
                if (!roleExist)
                {
                    //create the roles and seed them to the database: Question 1
                    var res = await _roleManager.CreateAsync(new Microsoft.AspNetCore.Identity.IdentityRole(roleName));
                }
            }

            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}

        }

        /// <summary>
        /// Devuelve el listado completo de nombres de roles
        /// </summary>
        /// <returns>Listado de nombres de Roles</returns>
        private IEnumerable<string> GetAllRolesPrivate()
        {
            //try
            //{
            return _roleManager.Roles.Select(r => r.Name).OrderBy(n => n).ToList();
            //}
            //catch (Exception e)
            //{
            //    throw e;
            //}
        }

        ///// <summary>
        ///// Devuelve una excepción formateada.
        ///// </summary>
        ///// <param name="e"></param>
        ///// <returns>Mensaje de Error</returns>
        //private IActionResult GetExcepcionFormateada(Exception e)
        //{
        //    return BadRequest(e.Message + (e.InnerException != null ? " " + e.InnerException : " "));
        //}

        /// <summary>
        /// Devuelve un usuario logueado o null en caso de encontrarlo.
        /// </summary>
        /// <param name="userManager"></param>
        /// <returns>Usuario ApplicationUser</returns>
        private async Task<ApplicationUser> GetUserLogin(UserManager<ApplicationUser> userManager)
        {
            //try
            //{
            var identityUser = await userManager.GetUserAsync(HttpContext.User);
            return identityUser;
            //}
            //catch (Exception e)
            //{
            //    throw new Exception("Error al intentar obtener el usuario logueado", e);
            //}
        }

        /// <summary>
        /// Valida si existe una base de datos para una sucursal.
        /// </summary>
        /// <param name="sucursal">Nombre de la sucursal</param>
        /// <returns>bool</returns>
        private bool ValidarSiExisteBaseEnProduccion()
        {
            string baseConnectionString = _configuration.GetConnectionString("AuthDatabaseConnection");
            string sucursalDataBaseName = _configuration.GetConnectionString("AuthDataBaseName");
            return Utilidades.CheckDatabaseExists(baseConnectionString, sucursalDataBaseName);
        }

        /// <summary>
        /// Devuelve el listado de los administradores de las sucursales y los admins del sistema
        /// </summary>
        /// <returns>Listado de usuarios</returns>
        private async Task<List<ApplicationUser>> ObtenerAdministradoresSucursalesAsync()
        {
            List<ApplicationUser> administradoresSucursales = new List<ApplicationUser>();
            List<ApplicationGroup> gruposAdministradores = await _groupManager.Groups.Where(g => GruposNoAdministrables.Contains(g.Name)).ToListAsync();

            foreach (ApplicationGroup group in gruposAdministradores)
            {
                administradoresSucursales.AddRange(await _groupManager.GetGroupUsersAsync(group.Id));
            }

            return administradoresSucursales;
        }

        /// <summary>
        /// Valida si el usuario logueado es Administrador de Sucursales.
        /// </summary>
        private async Task<bool> EsAdministradorSucursalAsync()
        {
            ApplicationUser usuarioLogueado = await ObtenerUsuarioLogueado();
            IEnumerable<ApplicationGroup> grupos = await _groupManager.GetUserGroupsAsync(usuarioLogueado.Id);
            bool esAdministradorSucursal = grupos.Any(g => g.Name.ToLower() == "AdministradorSucursal".ToLower());
            return esAdministradorSucursal;
        }

        /// <summary>
        /// Valida si el usuario logueado es administrador del sistema.
        /// </summary>
        private async Task<bool> EsAccountManagerAsync()
        {
            ApplicationUser usuarioLogueado = await ObtenerUsuarioLogueado();
            IEnumerable<ApplicationGroup> grupos = await _groupManager.GetUserGroupsAsync(usuarioLogueado.Id);
            bool esAccountManager = grupos.Any(g => g.Name.ToLower() == "AccountManager".ToLower());
            return esAccountManager;
        }

        /// <summary>
        /// Devuelve el listado de sucursales correspondiente para un administrador de sucursal
        /// </summary>
        /// <returns>Listado de sucursales</returns>
        private async Task<List<Sucursal>> GetSucursalesParaAdministradorSucursalPrivateAsync()
        {
            List<Sucursal> sucursales = new List<Sucursal>();

            if (await EsAccountManagerAsync())
            {
                sucursales = await _empresaDbContext.Sucursales.ToListAsync();
            }
            else if (await EsAdministradorSucursalAsync())
            {
                ApplicationUser usuarioLogueado = await ObtenerUsuarioLogueado();

                IEnumerable<ApplicationGroup> gruposUsuarioLogueado = await _groupManager.GetUserGroupsAsync(usuarioLogueado.Id);

                ApplicationGroup grupoAdministradorSucursal = gruposUsuarioLogueado.FirstOrDefault(gul => gul.Name == "AdministradorSucursal");

                List<GroupUserSucursal> groupUserSucursalParaAdmnistradoresSucursales = usuarioLogueado.GroupsUsersSucursales.Where(gus => gus.ApplicationGroupId == grupoAdministradorSucursal.Id).ToList();

                sucursales = await _empresaDbContext.Sucursales
                    .Where(s => groupUserSucursalParaAdmnistradoresSucursales.Any(gus => gus.SucursalId == s.Id))
                    .ToListAsync();
            }

            return sucursales;

        }

        #endregion

    }

}
