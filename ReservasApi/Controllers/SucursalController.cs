﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reservas.Data;
using Microsoft.AspNetCore.Authorization;
using Reservas.DbContexts;
using ReservasApi.Models.Sucursal;
using Common.Atributos;
using Reservas.Entidades.Sucursal;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using System.Security.Claims;
using AutoMapper;
using Identity.Entities;
using Microsoft.AspNetCore.Identity;
using Reservas.Entidades.Empresa;
using Common.Utilidades;
using Microsoft.Extensions.Configuration;
using System.Text;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ReservasApi.Models.Empresa;
using Microsoft.AspNetCore.Routing;
using ReservasApi.Atributos;

namespace ReservasApi.Controllers
{
    /// <summary>
    /// Controlador con acciones para cada sucursal
    /// </summary>
    [Produces("application/json")]
    [Route("api/reservas/[controller]/{sucursal}/[action]")]
    [ApiController]
#if DEBUG
    [AllowAnonymous]
#else
    [Authorize(Roles = "SucursalController")]
#endif
    [ApiExceptionFilter]
    // CUIDADO NO REFACTORIZAR ESTE CONTROLLER, ni su nombre si su firma
    public class SucursalController : ControllerBase, IDisposable
    {
        private SucursalDbContext _sucursalDbContext;
        private EmpresaDbContext _empresaDbContext;
        private UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IMapper _mapper;



        // CUIDADO NO REFACTORIZAR ESTE CONTROLLER, ni su nombre si su firma
        public SucursalController(IConfiguration configuration, UserManager<ApplicationUser> userManager, EmpresaDbContext empresaDbContext, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            _mapper = mapper;
            _userManager = userManager;
            _empresaDbContext = empresaDbContext;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;


            Sucursal sucursal = ObtenerSucursalDeUrl();

            _sucursalDbContext = ObtenerDbContext(sucursal.DBConnectionString);
        }


        [ApiExplorerSettings(IgnoreApi = true)]
        public void Dispose()
        {
            if (_sucursalDbContext != null)
            {
                _sucursalDbContext.Dispose();
            }
            _sucursalDbContext = null;
        }

        /// <summary>
        /// Metodo necesario para crear la base de datos basado en el nombre de la sucursal
        /// </summary>
        /// <param name = "sucursal" > Nombre normalizado de la base de datos de la sucursal</param>
        /// <returns>string</returns>


        [HttpPost]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerCrearBase")]
#endif
        public IActionResult CrearBase(string sucursal)
        {
            sucursal = sucursal.Replace(" ", "");
            if (sucursal.Length > 16)
            {
                throw new Exception("Ingrese un valor máximo de 16 caracteres");
            }
            bool existeBase = ValidarSiExisteBaseEnProduccion(sucursal);
#if !DEBUG
            if (!existeBase)
            {
                throw new Exception("La base de datos no existe en producción, por favor creela");
            }
#endif
            
            _sucursalDbContext.Database.EnsureCreated();

            return Ok("Base de datos creada");

        }



        [HttpPost]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerCrearReserva")]
#endif
        public async Task<IActionResult> CrearReserva(string sucursal, [FromBody] ReservaModel reserva)
        {
            //limpiarModelo
            reserva.Motivo = null;
            reserva.UnidadDeReserva = null;
            reserva.Usuario = null;
            reserva.HistorialReservas = new List<ReservaModel>(); //TODO agregar uno nuevo.

            reserva.Id = 0;

            if (reserva.ClientesReserva.Count() > 1)
            {
                foreach (var cR in reserva.ClientesReserva)
                {
                    cR.EsReservaFija = false;
                }
            }

            IList<string> errores = ValidarReserva(reserva);

            if (errores.Count > 0)
            {
                throw new Exception(string.Join(". ", errores));
            }

            //Mapear
            Reserva reservaDto = _mapper.Map<Reserva>(reserva);
            reservaDto.ClientesReserva = _mapper.Map<ClienteReserva[]>(reserva.ClientesReserva);

            IList<Reserva> reservasSuperpuestas = await BuscarReservasSuperpuestas(reservaDto);
            if (reservasSuperpuestas.Count() > 0)
            {
                throw new Exception("La reserva se superpone con otra/s");
            }

            //¿Que usuario hizo la reserva?
            reservaDto.UsuarioId = null;
            var usuarioLogueado = await GetUserLogin(_userManager);
            if (usuarioLogueado != null)
            {
                reservaDto.UsuarioId = usuarioLogueado.Id;
            }

            //Guardar
            EntityEntry<Reserva> result = await _sucursalDbContext.Reservas.AddAsync(reservaDto);
            await result.Context.SaveChangesAsync();
            return Ok(result.Entity.Id);
        }



        [HttpPatch]
        [Route("{id}")]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerCambiarEstadoReserva")]
#endif
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ValidarRolSucursal]
        /// <summary>
        /// Modificar una reserva.
        /// </summary>
        /// <param name="sucursal"></param>
        /// <param name="id"></param>
        /// <param name="reserva"></param>
        public async Task<IActionResult> CambiarEstadoReserva(int sucursal, long id, [FromBody] ReservaModel reserva)
        {
            return await this.ModificarReservaPrivateAsync(id, reserva);
        }

        [HttpPatch]
        [Route("{id}")]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerModificarReserva")]
#endif
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        /// <summary>
        /// Modificar una reserva.
        /// </summary>
        /// <param name="sucursal"></param>
        /// <param name="id"></param>
        /// <param name="reserva"></param>
        public async Task<IActionResult> ModificarReserva(int sucursal, long id, [FromBody] ReservaModel reserva)
        {
            return await this.ModificarReservaPrivateAsync(id, reserva);
        }    
        
        [HttpPost]
        [Route("{id}")]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerEliminarReserva")]
#endif
        public async Task<IActionResult> EliminarReserva(string sucursal, [FromBody] ReservaModel reserva)
        {
            Reserva reservaDto = await _sucursalDbContext.Reservas.Include(r => r.ClientesReserva).FirstOrDefaultAsync(r => r.Id == reserva.Id);
            if (reservaDto != null)
            {
                reservaDto.Estado = "eliminada";

                bool esBajaDeSucursal = reserva.ClientesReserva.FirstOrDefault(cR => cR.ComentarioBaja != null) != null;
                bool esBajaDeCliente = !esBajaDeSucursal;

                foreach (ClienteReservaModel cR in reserva.ClientesReserva)
                {
                    ClienteReserva clienteReservaDto = reservaDto.ClientesReserva.FirstOrDefault(cRDto => cRDto.Id == cR.Id);
                    if (clienteReservaDto != null)
                    {
                        if (esBajaDeSucursal)
                        {
                            clienteReservaDto.ComentarioBaja = cR.ComentarioBaja;
                        }
                        else if (esBajaDeCliente)
                        {
                            clienteReservaDto.EstadoCumplimiento = cR.EstadoCumplimiento;
                        }
                    }
                }

                _sucursalDbContext.Reservas.Update(reservaDto);
                await _sucursalDbContext.SaveChangesAsync();

                return Ok("La reserva numero " + reserva.Id + " fué eliminada con existo");
            }
            else
            {
                return BadRequest("La reserva no existe o ya fué eliminada");
            }

        }

        /// <summary>
        /// Retorna una reserva de un sucursal conocida.
        /// </summary>
        /// <param name="sucursal"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerGetReserva")]
#endif
        [Route("{id}")]
        [ProducesResponseType(typeof(ReservaModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetReserva(string sucursal, int id)
        {

            //Mapear
            Reserva reservaDto = await _sucursalDbContext.Reservas.Include(r => r.ClientesReserva).FirstOrDefaultAsync(r => id == r.Id);

            ReservaModel reserva = _mapper.Map<ReservaModel>(reservaDto);

            foreach (ClienteReservaModel clienteReserva in reserva.ClientesReserva)
            {
                Cliente clienteDto = await _empresaDbContext.Clientes.FirstOrDefaultAsync(c => clienteReserva.ClienteId == c.Id);
                clienteReserva.Cliente = _mapper.Map<ClienteModel>(clienteDto);
            }

            return Ok(reserva);
        }

        /// <summary>
        /// Devuelve las reservas para un fecha.
        /// </summary>
        /// <param name="sucursal"></param>
        /// <param name="fecha"></param>
        /// <returns>Reservas</returns>
        [HttpPost]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerGetReservasParaUnaFecha")]
#endif
        [ProducesResponseType(typeof(ReservaModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetReservasParaUnaFechaNotLoading([FromBody] DateTime fecha)
        {
            return await GetReservasParaUnaFecha(fecha);
        }


        /// <summary>
        /// Devuelve las reservas para un fecha.
        /// </summary>
        /// <param name="sucursal"></param>
        /// <param name="fecha"></param>
        /// <returns>Reservas</returns>
        [HttpPost]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerGetReservasParaUnaFecha")]
#endif
        [ProducesResponseType(typeof(ReservaModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetReservasParaUnaFecha([FromBody] DateTime fecha)
        {

            IList<Reserva> reservasDto = await buscarReservasParaFecha(_sucursalDbContext, fecha).
                Where(r => r.Estado != "eliminada").
                Where(r => r.UnidadDeReserva.Activo).
                ToListAsync();

            IList<ReservaModel> reservas = await mapearReservas(reservasDto);

            return Ok(reservas);

        }

        /// <summary>
        /// Devuelve las reservas Eliminadas para una fecha.
        /// </summary>
        /// <param name="sucursal"></param>
        /// <param name="fecha"></param>
        /// <returns>Reservas</returns>
        [HttpPost]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerGetReservasEliminadasParaUnaFecha")]
#endif
        [ProducesResponseType(typeof(ReservaModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetReservasEliminadasParaUnaFecha([FromBody] DateTime fecha)
        {

            IList<Reserva> reservasDto = await buscarReservasParaFecha(_sucursalDbContext, fecha).
                Where(r => r.Estado == "eliminada").
                Where(r => r.UnidadDeReserva.Activo).
                ToListAsync();

            
            IList<ReservaModel> reservas = await mapearReservas(reservasDto);

            return Ok(reservas);

        }


        /// <summary>
        /// Valida si ya existen reservas que se superpongan en una cancha para una fecha y hora.
        /// </summary>
        /// <param name="fechaHoraComienzo"></param>
        /// <param name="fechaHoraFin"></param>
        /// <param name="unidadDeReservaId"></param>
        /// <returns></returns>
        //[HttpPost]
        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerGetReservaEstaLiberadaParaUnaFechaHoraCancha")]
#endif
        [Route("{fechaHoraComienzo}/{fechaHoraFin}/{unidadDeReservaId}")]
        [ProducesResponseType(typeof(bool), StatusCodes.Status200OK)]
        //public async Task<IActionResult> GetReservaEstaLiberadaParaUnaFechaHoraCancha(string sucursal, ReservaEstaLiberadaParaUnaFechaHoraCanchaModel body)
        public async Task<IActionResult> GetReservaEstaLiberadaParaUnaFechaHoraCancha(string sucursal, DateTime fechaHoraComienzo, DateTime fechaHoraFin, int unidadDeReservaId)
        {
            //_sucursalDbContext = SucursalDbContextFactory.CreateOnlyDbContext(sucursal);
            IList<Reserva> reservasSuperpuestas = await BuscarReservasParaFechaHoraUnidadReserva(fechaHoraComienzo, fechaHoraFin, unidadDeReservaId);
            //IList<Reserva> reservasSuperpuestas = await BuscarReservasParaFechaHoraUnidadReserva(body.FechaHoraComienzo, body.FechaHoraFin, body.UnidadDeReservaId);
            return Ok(reservasSuperpuestas.Count == 0);
        }

        /// <summary>
        /// Permite Crear una reserva para una sucursal.
        /// </summary>
        /// <param name="sucursal"></param>
        /// <param name="unidadReserva"></param>
        /// <returns></returns>
        [HttpPost]
//#if DEBUG
//        [AllowAnonymous]
//#else
        [Authorize(Roles = "SucursalControllerCrearUnidadReserva")]
//#endif
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ValidarRolSucursal]
        public async Task<IActionResult> CrearUnidadReserva(string sucursal, [FromBody] UnidadDeReserva unidadReserva)
        {
            unidadReserva.Id = 0;
            var result = await _sucursalDbContext.UnidadesDeReserva.AddAsync(unidadReserva);
            await result.Context.SaveChangesAsync();
            return Ok(result.Entity.Id);
        }

        /// <summary>
        /// Permite Crear una reserva para una sucursal.
        /// </summary>
        /// <param name="sucursal"></param>
        /// <param name="unidadReserva"></param>
        /// <returns></returns>
        [HttpPatch, Route("{id}")]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerModificarUnidadReserva")]
#endif
        [ProducesResponseType(typeof(UnidadDeReservaModel), StatusCodes.Status200OK)]
        [ValidarRolSucursal]
        public async Task<IActionResult> ModificarUnidadReserva(string sucursal, int id, [FromBody] UnidadDeReservaModel unidadReserva)
        {
            if (id != unidadReserva.Id)
            {
                throw new Exception("La unidad de reserva ingresada y el id de la url no coincide");
            }

            UnidadDeReserva unidadReservaDto = await _sucursalDbContext.UnidadesDeReserva.Include(ur => ur.PreciosPorHora).FirstOrDefaultAsync(c => c.Id == id);
            if (unidadReservaDto == null)
            {
                throw new Exception("Unidad de reserva no exite o fué eliminada");
            }

            _mapper.Map(unidadReserva, unidadReservaDto);

            _sucursalDbContext.UnidadesDeReserva.Update(unidadReservaDto);
            await _sucursalDbContext.SaveChangesAsync();
            return Ok(_mapper.Map<UnidadDeReservaModel>(unidadReservaDto));

        }


        /// <summary>
        /// Devuelve la unidad de reserva con su listado de precios por hora.
        /// </summary>
        /// <param name="sucursal"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerObtenerUnidadReserva")]
#endif
        [ProducesResponseType(typeof(IEnumerable<UnidadDeReservaModel>), StatusCodes.Status200OK)]
        [Route("{id}")]
        [ValidarRolSucursal]
        public async Task<IActionResult> ObtenerUnidadReserva(string sucursal, int id)
        {
            UnidadDeReserva unidadDeReservaDto = await _sucursalDbContext.UnidadesDeReserva.
                Include(ur => ur.PreciosPorHora).
                FirstOrDefaultAsync(ur => ur.Id == id);

            unidadDeReservaDto.PreciosPorHora = unidadDeReservaDto.PreciosPorHora.Where(pph => pph.Eliminado == null).ToList();
            
            UnidadDeReservaModel unidadDeReserva = _mapper.Map<UnidadDeReservaModel>(unidadDeReservaDto);

            return Ok(unidadDeReserva);
        }

        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerObtenerUnidadesReservaList")]
#endif
        // [ValidarRolSucursal] - NOOOO: Porque la van a consultar en la grilla de reservas todo el mundo.
        // CUIDADO NO REFACTORIZAR ESTE METODO, ni su nombre ni su firma
        public IActionResult ObtenerUnidadesReservaList()
        {
            var result = _sucursalDbContext.UnidadesDeReserva;
            //var result = _dbContext.Set<Reservas.Entidades.Sucursal.UnidadDeReserva>();
            return Ok(result);
        }




        [HttpPost]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerCrearMotivoReserva")]
#endif
        [ValidarRolSucursal]
        public async Task<IActionResult> CrearMotivoReserva(string sucursal, [FromBody] Reservas.Entidades.Sucursal.MotivoReserva motivoReserva)
        {
            var result = await _sucursalDbContext.MotivosReserva.AddAsync(motivoReserva);
            //var result = await _dbContext.Set<Reservas.Entidades.Sucursal.MotivoReserva>().AddAsync(motivoReserva);
            await result.Context.SaveChangesAsync();
            return Ok(result.Entity.Id);
        }



        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerObtenerMotivosReserva")]
#endif
        // [ValidarRolSucursal] - NOOOO: Porque la van a consultar en la grilla de reservas todo el mundo.
        public IActionResult ObtenerMotivosReserva(string sucursal)
        {
            // [ValidarRolSucursal] - NOOOO: Porque la van a consultar en la grilla de reservas todo el mundo.
            var result = _sucursalDbContext.Set<Reservas.Entidades.Sucursal.MotivoReserva>();
            return Ok(result);
        }

        /// <summary>
        /// Permite crear un precio para una franja horaria en un día de la semana o un feriado.
        /// </summary>
        /// <param name="sucursal"></param>
        /// <param name="precioPorHora"></param>
        /// <returns></returns>
        [HttpPost]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerCrearPrecioPorHora")]
#endif
        
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ValidarRolSucursal]
        public async Task<IActionResult> CrearPrecioPorHora(string sucursal, [FromBody] PrecioPorHoraModel precioPorHora)
        {

            if (precioPorHora.HoraFinAdaptada <= precioPorHora.HoraInicioAdaptada)
            {
                throw new Exception("La hora final ingresada es menor al la hora iniciar");
            }

            IList<PrecioPorHora> precioPorHoraSuperpuestas = await BuscarPreciosPorHoraSuperpuestos(precioPorHora);

            if (precioPorHoraSuperpuestas.Count() > 0)
            {
                throw new Exception("El franja horaria se superpone con otra en el mismo día, el siguiente o el anterior ");
            }

            PrecioPorHora precioPorHoraDto = _mapper.Map<PrecioPorHora>(precioPorHora);

            EntityEntry<PrecioPorHora> result = await _sucursalDbContext.PreciosPorHora.AddAsync(precioPorHoraDto);
            //var result = await _sucursalDbContext.Set<Reservas.Entidades.Sucursal.PrecioPorHora>().AddAsync(precioPorHoraDto);
            await result.Context.SaveChangesAsync();
            return Ok(result.Entity.Id);
        }


        /// <summary>
        /// Retorna un listado de precios por hora para todos los dias de la semana y feriados que estén vigentes.
        /// </summary>
        /// <param name="sucursal"></param>
        /// <param name="idUnidadReserva"></param>
        /// <returns></returns>
        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerObtenerPreciosPorHora")]
#endif
        [Route("{idUnidadReserva}")]
        [ProducesResponseType(typeof(IEnumerable<PrecioPorHoraModel>), StatusCodes.Status200OK)]
        // [ValidarRolSucursal] - NOOOO: Porque la van a consultar en la grilla de reservas todo el mundo.
        public async Task<IActionResult> ObtenerPreciosPorHora(string sucursal, int idUnidadReserva)
        {
            UnidadDeReserva unidadDeReservaDto = await _sucursalDbContext.UnidadesDeReserva
                .Include(udr => udr.PreciosPorHora)
                .FirstOrDefaultAsync(udr => udr.Id == idUnidadReserva);

            IEnumerable<PrecioPorHora> preciosPorHoraDto = unidadDeReservaDto.PreciosPorHora;

            IEnumerable<PrecioPorHoraModel> precioPorHora = _mapper.Map<IEnumerable<PrecioPorHoraModel>>(preciosPorHoraDto);

            IEnumerable<PrecioPorHoraModel> precioPorHoraVigentes = precioPorHora.Where(pph => pph.Eliminado == null);

            return Ok(precioPorHoraVigentes);
        }

        /// <summary>
        /// Modifica un Precio por hora para una cancha.
        /// </summary>
        /// <param name="sucursal"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPatch]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerModificarPrecioPorHora")]
#endif
        [Route("{id}")]
        [ProducesResponseType(typeof(IEnumerable<PrecioPorHoraModel>), StatusCodes.Status200OK)]
        [ValidarRolSucursal]
        public async Task<IActionResult> ModificarPrecioPorHora(string sucursal, int id, [FromBody] PrecioPorHoraModel precioPorHora)
        {
            if (id != precioPorHora.Id)
            {
                throw new Exception("La entidad ingresada y el id de la url no coinciden");
            }

            if (precioPorHora.HoraFinAdaptada <= precioPorHora.HoraInicioAdaptada)
            {
                throw new Exception("La hora final ingresada es menor al la hora iniciar");
            }

            IList<PrecioPorHora> precioPorHoraSuperpuestas = await BuscarPreciosPorHoraSuperpuestos(precioPorHora);

            if (precioPorHoraSuperpuestas.Count() > 0)
            {
                throw new Exception("El franja horaria se superpone con otra en el mismo día. ");
            }

            PrecioPorHora precioPorHoraDto = await _sucursalDbContext.Set<Reservas.Entidades.Sucursal.PrecioPorHora>().FirstOrDefaultAsync(c => c.Id == id);

            if (precioPorHoraDto == null)
            {
                throw new Exception("El item no exite o fué eliminada");
            }

            _mapper.Map(precioPorHora, precioPorHoraDto);

            _sucursalDbContext.Set<Reservas.Entidades.Sucursal.PrecioPorHora>().Update(precioPorHoraDto);
            await _sucursalDbContext.SaveChangesAsync();

            return Ok(_mapper.Map<PrecioPorHoraModel>(precioPorHoraDto));

        }

        /// <summary>
        /// Elimina un Precio por hora para una cancha.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "SucursalControllerEliminarPrecioPorHora")]
#endif
        [Route("{id}")]
        [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
        [ValidarRolSucursal]
        public async Task<IActionResult> EliminarPrecioPorHora(string sucursal, int id)
        {
 
            PrecioPorHora precioPorHoraDto = await _sucursalDbContext.Set<Reservas.Entidades.Sucursal.PrecioPorHora>().FirstOrDefaultAsync(c => c.Id == id);

            if (precioPorHoraDto == null)
            {
                throw new Exception("El item no exite o fué eliminada");
            }

            precioPorHoraDto.Eliminado = new DateTime();

            _sucursalDbContext.Set<Reservas.Entidades.Sucursal.PrecioPorHora>().Update(precioPorHoraDto);
            await _sucursalDbContext.SaveChangesAsync();

            return Ok();

        }

        #region Private
        private SucursalDbContext ObtenerDbContext(string connectionString)
        {
            //El dbContext se crea durante la ejecución
            _sucursalDbContext = SucursalDbContextFactory.CreateOnlyDbContext(connectionString);  // <- Se setea para dispose
            return _sucursalDbContext;
        }

        private async Task<IList<Reserva>> ObtenerReservasParaUnaFecha(DateTime date)
        {
            return await _sucursalDbContext.Reservas.Where(r => r.FechaHoraComienzo.Date.Ticks == date.Date.Ticks).ToListAsync();
        }

        /// <summary>
        /// Devuelve las reservas para un fecha y una unidad de reserva.
        /// </summary>
        /// <param name="date">fecha de las reservas</param>
        /// <param name="unidadDeReservaId">unidad de reserva para las reservas</param>
        /// <returns>List<Reserva></returns>
        private async Task<IList<Reserva>> ObtenerReservasParaFechaUnidadReserva(DateTime date, int? unidadDeReservaId)
        {
            if (!unidadDeReservaId.HasValue)
            {
                throw new Exception("La unidad de reserva es nula, no fue posible devolver las reservas para una fecha y una unidad");
            }
            return await _sucursalDbContext.Reservas.Where(r => r.FechaHoraComienzo.Date.Ticks == date.Date.Ticks && r.UnidadDeReservaId == unidadDeReservaId).ToListAsync();
        }

        /// <summary>
        /// Devuelve un usuario logueado o null en caso de encontrarlo.
        /// </summary>
        /// <param name="userManager"></param>
        /// <returns>ApplicationUser</returns>
        private async Task<ApplicationUser> GetUserLogin(UserManager<ApplicationUser> userManager)
        {
            try
            {
                var identityUser = await userManager.GetUserAsync(HttpContext.User);
                return identityUser;
            }
            catch (Exception e)
            {
                throw new Exception("Error al intentar obtener el usuario logueado", e);
            }
        }

        /// <summary>
        /// Valida que la reserva sea correcta antes de crearse o modificarse
        /// </summary>
        /// <param name="reserva"></param>
        /// <returns>List<string>errores</returns>
        private IList<string> ValidarReserva(ReservaModel reserva)
        {
            IList<string> errores = new List<string>();

            //Validar modelo
            if (reserva.MotivoId == null || reserva.MotivoId < 1) { errores.Add("La reserva no tiene motivo"); }
            if (reserva.SeniaTotal < 0) { errores.Add("La SeniaTotal tiene que ser un número positivo"); }
            if (reserva.DeudaTotal < 0) { errores.Add("La DeudaTotal tiene que ser un número positivo"); }
            if (reserva.ImporteUnidadReservada < 0) { errores.Add("El ImporteUnidadReservada tiene que ser un número positivo"); }
            if (!reserva.UnidadDeReservaId.HasValue || reserva.UnidadDeReservaId.Value < 1) { errores.Add("La reserva no tiene unidad de reserva"); }
            if (reserva.ClientesReserva == null || reserva.ClientesReserva.Count() < 1) { errores.Add("La reserva no tiene ningún clienteReserva asociado"); throw new Exception(string.Join(". ", errores)); }
            foreach (var cR in reserva.ClientesReserva)
            {
                string msjBase = "Al menos un clienteReserva ";

                if (cR.ClienteId == null || cR.ClienteId < 1) { errores.Add(msjBase + "no tiene un cliente asociado"); }
                if (cR.Senia < 0) { errores.Add(msjBase + "tiene seña negativa y debería ser positiva"); }
                //if (cR.SeniaSaldo < 0) { errores.Add(msjBase + "tiene saldo negativo"); }
                //if (cR.SeniaAdicional < 0) { errores.Add(msjBase + "tiene una seña adicional negativa"); }
                if (cR.Deuda > 0) { errores.Add(msjBase + "tiene una deuda positiva y deberia ser negativa"); }
                if (cR.ImporteUnidadReservaParte < 0) { errores.Add(msjBase + "tiene un importe de reserva menor a 0"); }
            }
            return errores;
        }

        /// <summary>
        /// Devuelve un listado de las reservas que se superponen.
        /// </summary>
        /// <param name="reservaDto"></param>
        /// <returns></returns>
        private async Task<IList<Reserva>> BuscarReservasSuperpuestas(Reserva reservaDto)
        {
            IList<Reserva> reservasSuperpuestas = await BuscarReservasParaFechaHoraUnidadReserva(reservaDto.FechaHoraComienzo, reservaDto.FechaHoraFin, reservaDto.UnidadDeReservaId.Value);

            //¿La reserva actual ya existe?
            if (reservaDto.Id > 0)
            {
                reservasSuperpuestas.Remove(reservasSuperpuestas.FirstOrDefault(r => r.Id == reservaDto.Id));
            }

            return reservasSuperpuestas;
        }


        /// <summary>
        /// Devuelve un listado de las reservas que se superponen.
        /// </summary>
        /// <param name="reservaDto"></param>
        /// <returns></returns>
        private async Task<IList<Reserva>> BuscarReservasParaFechaHoraUnidadReserva(DateTime fechaHoraComienzo, DateTime fechaHoraFin, int unidadDeReservaId)
        {
            //Validar Si existe Reserva Vigente

            IList<Reserva> reservasParaMismaFechaUnidadDeReserva = await ObtenerReservasParaFechaUnidadReserva(fechaHoraComienzo.Date, unidadDeReservaId);

            // Superpuesta si |-------FI-----[]   o   []------FF-------|
            // Superpuesta si FI-------------[]   o   []--------------FF
            // Superpuesta si FI-------------FF
            // Superpuesta si FI----|-----------|----FF
            // Libre si:      |--------------FI   y   FF---------------|
            IList<Reserva> reservasSuperpuestas = reservasParaMismaFechaUnidadDeReserva
                .Where(r => r.Estado != "eliminada")
                .Where(r =>
                    (fechaHoraComienzo >= r.FechaHoraComienzo && fechaHoraComienzo < r.FechaHoraFin) ||

                    (fechaHoraFin > r.FechaHoraComienzo && fechaHoraFin <= r.FechaHoraFin) ||

                    (fechaHoraComienzo <= r.FechaHoraComienzo && fechaHoraFin >= r.FechaHoraFin)

                ).ToList();

            return reservasSuperpuestas;
        }


        /// <summary>
        /// Devuelve un listado de las reservas que se superponen.
        /// </summary>
        /// <param name="reservaDto"></param>
        /// <returns></returns>
        private async Task<IList<PrecioPorHora>> BuscarPreciosPorHoraSuperpuestos(PrecioPorHoraModel precioPorHora)
        {
            // Busco el día anterior y el posterior.
            List<string> dias = new List<string>() { "lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo" };
            string diaActual = precioPorHora.DiaSemana;

            int indiceAnterior = dias.IndexOf(precioPorHora.DiaSemana) - 1;
            indiceAnterior = indiceAnterior < 0 ? dias.Count - 1 : indiceAnterior;
            string diaAnterior = dias[indiceAnterior];

            int indiceSiguiente = dias.IndexOf(precioPorHora.DiaSemana) + 1;
            indiceSiguiente = indiceSiguiente > 6 ? 0 : indiceSiguiente;
            string diaSiguiente = dias[indiceSiguiente];

            // Necesito trabajar con horarios adaptados si o si. Y almacenar horarios adaptados. Pero al mostrarlos mostrar horarios distintos.
            // Primero lo trabajo en FrontEnd Luego en Backend.

            List<PrecioPorHora> preciosPorHoraSuperPuestos = new List<PrecioPorHora>();
            if (precioPorHora.HoraFinAdaptada >= 24)
            {
                IList<PrecioPorHora> listaSuperPuestaDiaSiguiente = await _sucursalDbContext.PreciosPorHora
                    .Where(pPH => pPH.Eliminado == null)
                    .Where(
                    pPH => pPH.UnidadDeReservaId == precioPorHora.UnidadDeReservaId &&
                    pPH.DiaSemana == diaSiguiente &&
                    pPH.HoraInicio < precioPorHora.HoraFin

                    ).ToListAsync();

                preciosPorHoraSuperPuestos.AddRange(listaSuperPuestaDiaSiguiente);
            }

            if (precioPorHora.HoraFinAdaptada < 24)
            {
                IList<PrecioPorHora> listaSuperpuestaElDiaAnterior = await _sucursalDbContext.PreciosPorHora
                    .Where(pPH => pPH.Eliminado == null)
                    .Where(
                    pPH => pPH.UnidadDeReservaId == precioPorHora.UnidadDeReservaId &&
                    pPH.DiaSemana == diaAnterior &&
                    (pPH.HoraFinAdaptada - 24) > precioPorHora.HoraInicio

                    ).ToListAsync();

                preciosPorHoraSuperPuestos.AddRange(listaSuperpuestaElDiaAnterior);
            }
           
            //Falta el caso donde se compara con si misma.
            IList<PrecioPorHora> listaSuperpuestaEnElDia = await _sucursalDbContext.PreciosPorHora
                .Where(pPH => pPH.Eliminado == null)
                .Where(pPH => pPH.UnidadDeReservaId == precioPorHora.UnidadDeReservaId && pPH.DiaSemana == diaActual && pPH.Id != precioPorHora.Id)
                .Where(pPH =>
                    (precioPorHora.HoraInicioAdaptada >= pPH.HoraInicioAdaptada && precioPorHora.HoraInicioAdaptada < pPH.HoraFinAdaptada) ||

                    (precioPorHora.HoraFinAdaptada > pPH.HoraInicioAdaptada && precioPorHora.HoraFinAdaptada <= pPH.HoraFinAdaptada) ||

                    (precioPorHora.HoraInicioAdaptada <= pPH.HoraInicioAdaptada && precioPorHora.HoraFinAdaptada >= pPH.HoraFinAdaptada)
                ).ToListAsync();

            preciosPorHoraSuperPuestos.AddRange(listaSuperpuestaEnElDia);

            return preciosPorHoraSuperPuestos;

        }

        /// <summary>
        /// Valida si existe una base de datos para una sucursal.
        /// </summary>
        /// <param name="sucursal">Nombre de la sucursal</param>
        /// <returns>bool</returns>
        private bool ValidarSiExisteBaseEnProduccion(string sucursalName)
        {
            //string baseConnectionString = _configuration.GetConnectionString("SucursalDatabaseConnection");
            //string sucursalDataBaseName = _configuration.GetConnectionString("SucursalDataBaseName");

            //StringBuilder sbDataBaseName = new StringBuilder();
            //sbDataBaseName.AppendFormat(sucursalDataBaseName, sucursal);
            //sucursalDataBaseName = sbDataBaseName.ToString();

            //StringBuilder sbConnectionString = new StringBuilder();
            //sbConnectionString.AppendFormat(baseConnectionString, sucursal);
            //baseConnectionString = sbConnectionString.ToString();

            Sucursal sucursal = _empresaDbContext.Sucursales.FirstOrDefault(s => s.Nombre == sucursalName);

            if (sucursal == null)
            {
                throw new Exception("No existe la sucursal");
            }


            return Utilidades.CheckDatabaseExists(sucursal.DBConnectionString);
        }

        /// <summary>
        /// Estandariza el proceso de busquedas de reservas para una fecha.
        /// </summary>
        /// <param name="sucursalDbContext"></param>
        /// <param name="fecha"></param>
        /// <returns></returns>
        private IQueryable<Reserva> buscarReservasParaFecha(SucursalDbContext sucursalDbContext, DateTime fecha)
        {
            return _sucursalDbContext.Reservas.
                Include(r => r.UnidadDeReserva).
                Include(r => r.Motivo).
                Include(r => r.ClientesReserva).
                Where(r => r.FechaHoraComienzo.Date.Ticks == fecha.Date.Ticks);
        }

        /// <summary>
        /// Mapea reservas del modelo de datos al modelo de servicio web.
        /// </summary>
        /// <param name="reservasDto"></param>
        /// <returns></returns>
        private async Task<IList<ReservaModel>> mapearReservas(IList<Reserva> reservasDto)
        {
            // Evitar System.ArgumentException por recursividad de Reserva.ClienteReserva[i].Reserva....
            IList<ReservaModel> reservas = _mapper.Map<IList<ReservaModel>>(reservasDto);
            foreach (ReservaModel reserva in reservas)
            {
                foreach (ClienteReservaModel cR in reserva.ClientesReserva)
                {
                    if (cR.ClienteId > 0)
                    {
                        cR.Cliente = _mapper.Map<ClienteModel>(await _empresaDbContext.FindAsync<Cliente>(cR.ClienteId));
                    }
                }
            }
            return reservas;
        }

        /// <summary>
        /// Obtiene la sucursal según los parametros de la url
        /// </summary>
        /// <returns></returns>
        private Sucursal ObtenerSucursalDeUrl()
        {
            int sucursalId = -1;
            string sucursalName = null;

            string sucursalParameter = (string)_httpContextAccessor.HttpContext.GetRouteData().Values["sucursal"];
            bool esSucursalId = int.TryParse(sucursalParameter, out sucursalId);
            if (!esSucursalId)
            {
                sucursalName = sucursalParameter;
            }

            Sucursal sucursal = _empresaDbContext.Sucursales.FirstOrDefault(s => s.Id == sucursalId || s.Nombre == sucursalName);

            if (sucursal == null)
            {
                throw new Exception("No existe la sucursal");
            }

            return sucursal;
        }

        /// <summary>
        /// Valida si el usuario puede modificar o cambiar de estado de una reserva.
        /// </summary>
        /// <param name="reserva"></param>
        /// <returns>true|false</returns>
        private bool ValidarModificarReservaOCambioEstado(ReservaModel reserva)
        {
            bool esCambiarEstadoReserva = ControllerContext.ActionDescriptor.MethodInfo.Name == "CambiarEstadoReserva";
            bool esModificarReserva = ControllerContext.ActionDescriptor.MethodInfo.Name == "ModificarReserva";
            bool elNuevoEstadoEsPendiente = reserva.FechaHoraComienzoReal == null && reserva.FechaHoraFin != null;

            if (elNuevoEstadoEsPendiente && esModificarReserva)
            {
                return true;
            }

            if (esCambiarEstadoReserva && !elNuevoEstadoEsPendiente)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Modificar una reserva.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="reserva"></param>
        private async Task<IActionResult> ModificarReservaPrivateAsync(long id, ReservaModel reserva)
        {

            if (!ValidarModificarReservaOCambioEstado(reserva))
            {
                string MethodName = ControllerContext.ActionDescriptor.MethodInfo.Name;
                return BadRequest("No está permitido " + MethodName);
            }

            // Limpiar Modelo
            reserva.Motivo = null;
            reserva.UnidadDeReserva = null;
            reserva.Usuario = null;

            if (reserva.ClientesReserva.Count() > 1)
            {
                foreach (var cR in reserva.ClientesReserva)
                {
                    cR.EsReservaFija = false;
                }
            }

            IList<string> errores = ValidarReserva(reserva);
            if (errores.Count > 0)
            {
                throw new Exception(string.Join(". ", errores));
            }

            //Mapear
            Reserva reservaDto = await _sucursalDbContext.Reservas.FirstOrDefaultAsync(r => reserva.Id == r.Id);

            if (reservaDto.Id != id)
            {
                return BadRequest("No coincide el id de la url con la reserva.Id enviada en el body");
            }
            //NO USAR .Include(r => r.ClientesReserva).FirstOrDefaultAsync(r => reserva.Id == r.Id); Problemas automapper con IList y HashSet

            if (reservaDto.Estado == "cerrada")
            {
                return BadRequest("No podés modificar una reserva cerrada");
            }

            IList<ClienteReservaModel> clientesReserva = reserva.ClientesReserva; //Backupear

            reserva.ClientesReserva = null; // Evitar error mapeo

            _mapper.Map(reserva, reservaDto);

            IList<ClienteReserva> clientesReservaDto = await _sucursalDbContext.ClientesReserva.Where(cr => cr.Reserva.Id == reserva.Id).ToListAsync();

            //Agregar y Actualizar Clientes Reserva
            foreach (ClienteReservaModel clienteReserva in clientesReserva)
            {
                ClienteReserva clienteReservaDto = clientesReservaDto.FirstOrDefault(crDto => crDto.Id == clienteReserva.Id);
                if (clienteReservaDto == null)
                {
                    clienteReservaDto = _mapper.Map<ClienteReserva>(clienteReserva);
                    clienteReservaDto.ReservaId = reservaDto.Id;
                    _sucursalDbContext.ClientesReserva.Add(clienteReservaDto);
                }
                else
                {
                    _mapper.Map(clienteReserva, clienteReservaDto);
                    // Guarda solo el cliente reserva.
                    _sucursalDbContext.ClientesReserva.Update(clienteReservaDto);
                }
            }

            //Eliminar
            foreach (ClienteReserva clienteReservaDto in clientesReservaDto)
            {
                ClienteReservaModel clienteReserva = clientesReserva.FirstOrDefault(cr => cr.Id == clienteReservaDto.Id);
                if (clienteReserva == null)
                {
                    _sucursalDbContext.ClientesReserva.Remove(clienteReservaDto);
                }
            }

            IList<Reserva> reservasSuperpuestas = await BuscarReservasSuperpuestas(reservaDto);
            if (reservasSuperpuestas.Count() > 0)
            {
                throw new Exception("La reserva se superpone con otra/s");
            }

            //¿Que usuario modificó la reserva? - El que la creo está en erl historial
            reservaDto.UsuarioId = null;
            var usuarioLogueado = await GetUserLogin(_userManager);
            if (usuarioLogueado != null)
            {
                reservaDto.UsuarioId = usuarioLogueado.Id;
            }

            //Guardar
            _sucursalDbContext.Reservas.Update(reservaDto);

            await _sucursalDbContext.SaveChangesAsync();

            return Ok();
        }

        #endregion

        #region CodigoDebug
#if DEBUG
        [HttpGet]
        public IActionResult TestThrowUnhandled()
        {
            throw new InvalidOperationException("This is an unhandled exception");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult TestInclude(string sucursal)
        {

            
            return Ok(_sucursalDbContext.UnidadesDeReserva.Include(r => r.PreciosPorHora).FirstOrDefault(ur => ur.Id == 2));
        }
#endif
        #endregion










    }
}
