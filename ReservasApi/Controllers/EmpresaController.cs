﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Reservas.Data;
using Reservas.DbContexts;
using Reservas.Entidades.Empresa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.Extensions;
using Microsoft.EntityFrameworkCore;
using Common.Atributos;
using Microsoft.Extensions.Configuration;
using Common.Utilidades;
using ReservasApi.Models.Sucursal;
using AutoMapper;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using ReservasApi.Models.Empresa;
using Microsoft.AspNetCore.Http;
using ReservasApi.Models.Account;

namespace ReservasApi.Controllers
{
    [Produces("application/json")]
    [Route("api/reservas/[controller]/[action]")]
    [ApiController]
    [Authorize]
    [ApiExceptionFilter]
    public class EmpresaController : ControllerBase
    {
        private readonly EmpresaDbContext _dbContext;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        public EmpresaController(IConfiguration configuration, IMapper mapper, EmpresaDbContext dbContext)
        {
            _configuration = configuration;
            _mapper = mapper;
            _dbContext = dbContext;
            bool existeBase = validarSiExisteBaseEnProduccion();

            if (!existeBase)
            {
#if !DEBUG
                throw new Exception("La base de datos de la empresa no existe en producción, por favor creela");
#endif
                _dbContext.Database.EnsureCreated();
            }

        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Test()
        {
            return Ok("ApiOk");
        }


        /// <summary>
        /// Devuelve como resultado de una cadena de busqueda clientes que coincidan en documento, telefono, nombreCompleto, email
        /// </summary>
        /// <param name="datoCliente"></param>
        /// <returns></returns>
        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "EmpresaControllerBuscarCliente")]
#endif
        [Route("{datoCliente}")]
        public async Task<IActionResult> BuscarCliente(string datoCliente)
        {
            if (string.IsNullOrWhiteSpace(datoCliente.Trim()) || datoCliente.Trim().Length < 3)
            {
                return Ok(new List<Cliente> { });
            }

            List<Cliente> clientes = await _dbContext.Clientes.Where(c => c.Estado != "eliminado").Where(c =>
                    (c.EsTelefonoFijo && ("0" + c.TelefonoArea.Trim() + c.TelefonoNumero.Trim()).Contains(datoCliente)) ||
                    (!c.EsTelefonoFijo &&
                        (
                             (c.TelefonoArea.Trim() + "15" + c.TelefonoNumero.Trim()).Contains(datoCliente) ||
                             (c.TelefonoArea.Trim() + c.TelefonoNumero.Trim()).Contains(datoCliente)
                        )
                    ) ||
                    c.Documento.Contains(datoCliente) ||
                    c.NombreYApellido.Trim().ToLower().Replace(" ", "").Contains(datoCliente) ||
                    c.Email.Trim().ToLower().Replace(" ", "").Contains(datoCliente)
                ).ToListAsync();

            return Ok(clientes);
        }

        /// <summary>
        /// Devuelve como resultado un cliente
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>Cliente</returns>
        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "EmpresaControllerGetCliente")]
#endif
        [Route("{Id}")]
        public async Task<IActionResult> GetCliente(int Id)
        {
            if (Id <= 0)
            {
                throw new Exception("El id debe ser mayor a 0.");
            }

            ClienteModel cliente = null;
            try
            {
                cliente = _mapper.Map<ClienteModel>(await _dbContext.Clientes.FirstOrDefaultAsync(c => c.Id == Id));
            }
            catch (Exception ex)
            {
                throw new Exception("No fué posible recuperar el cliente. Tal vez no exista.", ex);
            }

            return Ok(cliente);
        }

        /// <summary>
        /// Permite crear un cliente a travez de su modelo.
        /// </summary>
        /// <returns>El id(int) del cliente </returns>
        /// 
        [HttpPost]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "EmpresaControllerCrearCliente")]
#endif
        public async Task<IActionResult> CrearCliente([FromBody] ClienteModel cliente)
        {
            try
            {
                Cliente clienteDto = _mapper.Map<Cliente>(cliente);
                clienteDto.Id = 0;
                EntityEntry<Cliente> result = await _dbContext.Clientes.AddAsync(clienteDto);
                await result.Context.SaveChangesAsync();
                return Ok(result.Entity.Id);
            }
            catch (Exception e)
            {
                throw new Exception("No fué posible crear el cliente", e);
            }
        }

        /// <summary>
        /// Permite editar un cliente a travez de su modelo y su id.
        /// </summary>
        /// <returns>cliente editado</returns>
        /// 
        [HttpPatch]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "EmpresaControllerEditarCliente")]
#endif
        [Route("{Id}")]
        public async Task<IActionResult> EditarCliente(int Id, [FromBody] ClienteModel cliente)
        {
            if (cliente.Id != Id)
            {
                throw new Exception("El id de la url y del modelo no coinciden");
            }

            try
            {
                Cliente clienteDto = await _dbContext.Clientes.FirstOrDefaultAsync(c => c.Id == Id);
                if (clienteDto == null)
                {
                    throw new Exception("Cliente no exite o fué eliminado");
                }

                _mapper.Map(cliente, clienteDto);
                _dbContext.Clientes.Update(clienteDto);
                await _dbContext.SaveChangesAsync();

                return Ok(cliente);
            }
            catch (Exception e)
            {
                throw new Exception("No fué posible modificar el cliente", e);
            }
        }


        /// <summary>
        /// Permite eliminar un cliente a travez de su id
        /// </summary>
        /// <returns>mensaje string</returns>
        /// 
        [HttpDelete]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "EmpresaControllerEliminarCliente")]
#endif
        [Route("{Id}")]
        public async Task<IActionResult> EliminarCliente(int Id)
        {
            Cliente cliente = await _dbContext.Clientes.FirstOrDefaultAsync(c => c.Id == Id);
            if (cliente == null)
            {
                throw new Exception("Cliente no exite o fúe eliminado");
            }
            cliente.Estado = "eliminado";
            _dbContext.Clientes.Update(cliente);
            await _dbContext.SaveChangesAsync();

            return Ok("El cliente " + cliente.NombreYApellido + " fué eliminado con éxito.");
        }

        /// <summary>
        /// Actualiza los semaforos de un cliente a travez de su id.
        /// </summary>
        /// <param name="id">Id del cliente</param>
        /// <param name="semaforo"> [ Infracción | Infracción Grave | Cumplió | No Aplica ] </param>
        /// <returns>string: mensaje</returns>

        [HttpPatch]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "EmpresaControllerActualizarSemaforoCliente")]
#endif
        [Route("{id}/{semaforo}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public async Task<IActionResult> ActualizarSemaforoCliente(int id, string semaforo)
        {
            if (semaforo == "No Aplica")
            {
                return Ok("No se actualizaro ningún semaforo por que N/A");
            }

            Cliente cliente = await _dbContext.Clientes.FirstOrDefaultAsync(c => c.Id == id);

            if (cliente == null)
            {
                throw new Exception("Cliente no exite o fúe eliminado");
            }

            if (semaforo == "Infracción")
            {
                cliente.Infracciones++;
            }
            if (semaforo == "Infracción Grave")
            {
                cliente.InfraccionesGraves++;
            }
            if (semaforo == "Cumplió")
            {
                cliente.Cumplimientos++;
            }

            _dbContext.Clientes.Update(cliente);

            await _dbContext.SaveChangesAsync();

            return Ok("Se actualizó el semaforo correctamente");
        }


        /// <summary>
        /// Actualiza el saldo de un cliente a travez de su id.
        /// </summary>
        /// <param name="id">Id del cliente</param>
        /// <param name="saldo">Saldo actualizado del cliente</param>
        /// <returns>string: mensaje</returns>

        [HttpPatch]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "EmpresaControllerActualizarSaldoCliente")]
#endif
        [Route("{id}/{saldo}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]

        public async Task<IActionResult> ActualizarSaldoCliente(int id, decimal saldo)
        {

            Cliente cliente = await _dbContext.Clientes.FirstOrDefaultAsync(c => c.Id == id);

            if (cliente == null)
            {
                throw new Exception("Cliente no exite o fúe eliminado");
            }

            cliente.Saldo = saldo;

            _dbContext.Clientes.Update(cliente);

            await _dbContext.SaveChangesAsync();

            return Ok("Se actualizó el saldo correctamente");
        }


        /// <summary>
        /// Devuelve un listado de todas las sucursales.
        /// </summary>
        /// <returns> Listado de sucursales</returns>
        [HttpGet]
#if DEBUG
        [AllowAnonymous]
#else
        [Authorize(Roles = "EmpresaControllerGetSucursales")]
#endif
        [ProducesResponseType(typeof(IEnumerable<SucursalModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> GetSucursales()
        {
            IEnumerable<SucursalModel> sucursales = await _dbContext.Sucursales.Select( s => new SucursalModel() { 
                Id = s.Id,
                Calle = s.Calle,
                ComoLlegar = s.ComoLlegar,
                Coordenadas = s.Coordenadas,
                Nombre = s.Nombre,
                Numero = s.Numero,
                Telefonos = s.Telefonos
            }).ToListAsync();

            return Ok(sucursales);
        }


        #region private
        /// <summary>
        /// Valida si existe una base de datos para una sucursal.
        /// </summary>
        /// <param name="sucursal">Nombre de la sucursal</param>
        /// <returns>bool</returns>
        private bool validarSiExisteBaseEnProduccion()
        {
            string baseConnectionString = _configuration.GetConnectionString("EmpresaDatabaseConnection");
            string sucursalDataBaseName = _configuration.GetConnectionString("EmpresaDataBaseName");
            return Utilidades.CheckDatabaseExists(baseConnectionString, sucursalDataBaseName);
        }

        private string validarClienteAntesDeGuardar(Cliente cliente)
        {
            string mensaje = "";

            TryValidateModel(cliente);

            return mensaje;
        }
        #endregion

    }
}
