﻿using System;
using System.IO;
using System.Security.Claims;
using System.Security.Cryptography;
using Common.Extensions;
using Identity.DbContexts;
using Identity.Entities;
using Reservas.DbContexts;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using ReservasNetCore.Extensions;
using Common.Atributos;
using AutoMapper;
using Newtonsoft.Json;
using ReservasApi.Servicios.Clases;
using ReservasApi.Servicios.Interfaces;

namespace ReservasApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region Add CORS  
            services.AddCors(options => options.AddPolicy("Cors", builder =>
            {
                builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader();
            }));
            #endregion

            #region Add Entity Framework and Identity Framework  

            services.AddDbContext<ApplicationUserDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("AuthDatabaseConnection")));

            services.AddDbContext<ApplicationGroupDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("AuthDatabaseConnection")));

            services.AddDbContext<EmpresaDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("EmpresaDatabaseConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationUserDbContext>()
                .AddDefaultTokenProviders();

            #endregion

            #region Add Authentication  

            //var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Tokens:Key"]));

            RSA publicRsa = RSA.Create();
            publicRsa.FromXmlFile(Path.Combine(Directory.GetCurrentDirectory(),
                "Keys",
                 this.Configuration.GetValue<String>("Tokens:PublicKey")
                 ));

            RsaSecurityKey signingKey = new RsaSecurityKey(publicRsa);

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme; // <- Proyecto de martin
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(config =>
            {
                config.RequireHttpsMetadata = false;
                config.SaveToken = true;
                config.TokenValidationParameters = new TokenValidationParameters()
                {
                    //Editado como proyecto martin
                    IssuerSigningKey = signingKey,
                    ValidateAudience = true,
                    RoleClaimType = ClaimTypes.Role, // nuevo
                    ClockSkew = TimeSpan.Zero, // nuevo remove delay of token when expire
                    ValidAudience = this.Configuration["Tokens:Audience"],
                    ValidateIssuer = true,
                    ValidIssuer = this.Configuration["Tokens:Issuer"],
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true
                };

            });


            #endregion

            #region AutoMapper
            services.AddAutoMapper(typeof(StartupFolder.MapperProfile));
            #endregion

            services.AddSingleton<IAccountParametersServices, AccountParametersServices>();

            services.AddMvc(options =>
                {
                    options.Filters.Add(new ApiExceptionFilterAttribute());
                }
            )
            .AddJsonOptions(options =>
            {
                options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerDocumentation();

            services.AddHttpContextAccessor();



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            app.UseCors("Cors");
            app.UseAuthentication();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseSwaggerDocumentation();

            //// Enable middleware to serve generated Swagger as a JSON endpoint.
            //app.UseSwagger();

            //// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            //// specifying the Swagger JSON endpoint.
            //app.UseSwaggerUI(config =>
            //{
            //    config.SwaggerEndpoint("/swagger/v1/swagger.json", "Reservas API");
            //    config.DocumentTitle = "Reservas";
            //    config.RoutePrefix = string.Empty;
            //    config.DocExpansion(DocExpansion.None);
            //});

            app.UseMvc();
        }
    }



}
