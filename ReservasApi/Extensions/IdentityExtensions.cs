﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReservasApi.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetStringErrors(this IEnumerable<IdentityError> errors)
        {
            if (errors.Count() > 0)
            {
                StringBuilder sb = new StringBuilder();

                errors.ToList().ForEach(e => sb.AppendLine($"Code: {e.Code} - Description: {e.Description}"));

                return sb.ToString();
            }
            else throw new ApplicationException("UNKNOWN_ERROR");
        }
    }
}
