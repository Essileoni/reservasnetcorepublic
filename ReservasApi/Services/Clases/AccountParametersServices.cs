﻿
using ReservasApi.Services.Interfaces;

namespace ReservasApi.Services.Clases
{
    public class AccountParametersServices : IAccountParametersServices
    {
        public bool FirstRun { get; set; } = true;
    }
}
