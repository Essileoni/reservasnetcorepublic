﻿using AutoMapper;
using Reservas.Entidades.Empresa;
using Reservas.Entidades.Sucursal;
using ReservasApi.Models.Account;
using ReservasApi.Models.Empresa;
using ReservasApi.Models.Sucursal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReservasApi.StartupFolder
{
    public class MapperProfile : Profile
    {
        /// <summary>
        /// Perfil de mapeo para reservas
        /// </summary>
        public MapperProfile()
        {
            //CreateMap<ReservaModel, Reserva>();

            CreateMap<Reserva, Reserva>();

            CreateMap<Reserva, ReservaModel>().ForMember(x => x.Usuario, opt => opt.Ignore());

            CreateMap<ReservaModel, Reserva>().ForMember(x => x.UsuarioId, opt => opt.Ignore());//.ForMember(x => x.ClientesReserva, opt => opt.ConvertUsing<ConverterTest, IList<ClienteReservaModel>>());
            CreateMap<ClienteReserva, ClienteReservaModel>();
            //CreateMap<ClienteReserva, ClienteReservaModel>().ForMember(x => x.ImporteUnidadReservaParte, opt => opt.Ignore());
            CreateMap<ClienteReservaModel, ClienteReserva>().ForMember(x => x.Reserva, opt => opt.Ignore());
            CreateMap<ClienteModel, Cliente>();
            CreateMap<UnidadDeReservaModel, UnidadDeReserva>().ForMember(x => x.PreciosPorHora, opt => opt.Ignore());
            CreateMap<Sucursal, SucursalModel>();

        }
    }
    
}
