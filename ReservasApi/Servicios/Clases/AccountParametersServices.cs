﻿using ReservasApi.Servicios.Interfaces;

namespace ReservasApi.Servicios.Clases
{
    public class AccountParametersServices : IAccountParametersServices
    {
        public bool FirstRun { get; set; } = true;
    }
}
