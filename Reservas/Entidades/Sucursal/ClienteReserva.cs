﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Reservas.Entidades.Sucursal
{
    /// <summary>
    /// Entidad que relaciona a un cliente en una reserva
    /// </summary>
    public class ClienteReserva
    {
        public int Id { get; set; }
        /// <summary>
        /// Id del cliente vinculado a la reserva, existe en otra base de datos
        /// </summary>
        public int ClienteId { get; set; }
        /// <summary>
        /// Cliente vinculado a la reserva, existe en otra base de datos
        /// </summary>
        //public int Cliente { get; set; }
        /// <summary>
        /// Estado de asistencia del cliente vinculado a una reserva
        /// </summary>
        public string EstadoCumplimiento { get; set; }
        /// <summary>
        /// Comentario cargado por el usuario al cancelar una reserva por motivos ajenos al cliente.
        /// </summary>
        public string ComentarioBaja { get; set; }
        /// <summary>
        /// La reserva puede marcarse como fija para ofrecer ofrecer en el futuro otra reserva a la misma hora.
        /// </summary>
        public bool EsReservaFija { get; set; }
        /// <summary>
        /// Sumatoria de seniaSaldo + seniaAdicional.
        /// </summary>
        public decimal Senia { get; set; }
        /// <summary>
        /// Senia adicional ingresada manualmente.
        /// </summary>
        public decimal SaldoAFavor { get; set; }
        /// <summary>
        /// Saldo a favor usado en la seña.
        /// </summary>
        //public decimal SeniaSaldo { get; set; }
        /// <summary>
        /// Senia adicional ingresada manualmente.
        /// </summary>
        //public decimal SeniaAdicional { get; set; }
        /// <summary>
        /// Saldo negativo a pagar al momento del cierre de la reserva.
        /// </summary>
        public decimal Deuda { get; set; }
        /// <summary>
        /// Registra si el usuario el sistema decide o no usar el saldo. Por defecto false.
        /// </summary>
        public bool UsarSaldo { get; set; } = false;
        /// <summary>
        /// Id Reserva
        /// </summary>
        public long ReservaId { get; set; }
        /// <summary>
        /// Reserva para uno o más clientes.
        /// </summary>
        public Reserva Reserva { get; set; }
        /// <summary>
        /// Fracción del importe a pagar por el cliente
        /// </summary>
        public decimal ImporteUnidadReservaParte { get; set; }
        /// <summary>
        /// Dinero o billete fisico que presentó el cliente al momento de pagar.
        /// </summary>
        public decimal ClienteAbonoCon { get; set; }

        /// <summary>
        /// Fracción del descuento que beneficia al cliente.
        /// </summary>
        public decimal DescuentoReservaParte { get; set; }
    }
}
