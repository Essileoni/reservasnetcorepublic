﻿
using Reservas.Entidades.Empresa;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Reservas.Entidades.Sucursal
{
    /// <summary>
    /// Registro de una reserva
    /// </summary>
    public class Reserva
    {
        public long Id { get; set; }
        /// <summary>
        /// Motivo
        /// </summary>
        public MotivoReserva Motivo { get; set; }
        /// <summary>
        /// Motivo
        /// </summary>
        public int? MotivoId { get; set; }
        /// <summary>
        /// Momento en que se crea la reserva.
        /// </summary>
        public DateTime FechaHoraCreacion { get; set; }
        /// <summary>
        /// Momento en el que debería comenzar la reserva.
        /// </summary>
        public DateTime FechaHoraComienzo { get; set; }
        /// <summary>
        /// Momento en el que debería finalizar la reserva.
        /// </summary>
        public DateTime FechaHoraFin { get; set; }
        /// <summary>
        /// Registro del momento en el que comenzó realmente una reserva.
        /// </summary>
        public DateTime? FechaHoraComienzoReal { get; set; }
        /// <summary>
        /// Registro del momento en el que finalizó una reserva y se desocupó la unidad de reserva.
        /// </summary>
        public DateTime? FechaHoraFinReal { get; set; }
        /// <summary>
        /// Registro del momento en el que cerró la reserva, y ya no va a tener más cambios, ni consumos.
        /// </summary>
        public DateTime? FechaHoraCierre { get; set; }
        /// <summary>
        /// Monto con el que se seño la unidad de reserva para una fecha y hora. Debe descontarse en el pago final
        /// </summary>
        public decimal SeniaTotal { get; set; }
        /// <summary>
        /// Deuda total a saldar por los integrantes de la reserva al pago de la misma.
        /// </summary>
        public decimal DeudaTotal { get; set; }
        /// <summary>
        /// Descuento aplicado al momento de crear la reserva. Se resta del total a pagar por la misma.
        /// </summary>
        public decimal Descuento { get; set; }
        /// <summary>
        /// Importe al momento de crear la reserva. Depende de la duracion y el monto de la cancha para la hora solicitada.
        /// </summary>
        public decimal ImporteUnidadReservada { get; set; }
        /// <summary>
        /// Representa el monto real a abonar al momento de la reserva. Se descuentan senias, saldos, descuentos, etc.
        /// </summary>
        public decimal TotalReserva { get; set; }
        /// <summary>
        /// Anotación que puede dejar un usuario del sistema al momento de reservar.
        /// </summary>
        public string Comentario { get; set; }
        /// <summary>
        /// Indica si el registro de la reserva fue para un día feriado
        /// </summary>
        public bool EsFeriado { get; set; }
        //La reserva no puede tener un usuario, por que los usuarios pertenecen a otro contexto.
        ///// <summary>
        ///// Usuario que creó la reserva.
        ///// </summary>
        //public ApplicationUser Usuario { get; set; }
        /// <summary>
        /// Id del usuario que creo la reserva. Existe en otra base de datos.
        /// </summary>
        public string UsuarioId { get; set; }

        // public Consumos IEnumerable<Consumo> {get;set;}
        /// <summary>
        /// Personas que forman parte o responden por la reserva
        /// </summary>
        public IList<ClienteReserva> ClientesReserva { get; set; }
        /// <summary>
        /// Unidad Reservada por un usuario para una fecha y hora dada. No se guarda en el historico.
        /// </summary>
        public UnidadDeReserva UnidadDeReserva { get; set; }
        /// <summary>
        /// Id de la unidad de reserva. Se guarda en el historial.
        /// </summary>
        public int? UnidadDeReservaId { get; set; }
        /// <summary>
        /// Colección de los cambios de una reserva. Incluye la creación de la primera reserva. 
        /// </summary>
        public IEnumerable<HistorialReserva> HistorialReservas { get; set; }
        /// <summary>
        /// Estado de la reserva. [eliminada | cerrada]
        /// </summary>
        [RegularExpression("(eliminada|cerrada)", ErrorMessage = "Solo puede ingresar eliminada, cerrada")]
        public string Estado { get; set; }
    }

    /// <summary>
    /// Entidad que representa que es lo que se va a hacer con la unidad de reserva.
    /// </summary>
    public class MotivoReserva
    {
        public int Id { get; set; }
        /// <summary>
        /// Descripcion del motivo
        /// </summary>
        public string Descripcion { get; set; }
        /// <summary>
        /// Permite ponerle precio a la reserva y en algunos casos seleccionar multiples canchas en una misma reserva.
        /// </summary>
        public bool EsReservaCustom { get; set; }
        /// <summary>
        /// Tiempo, en minutos, inicial probable para el tipo motivo de reserva seleccionado
        /// </summary>
        public int TiempoEstandar { get; set; }
        /// <summary>
        /// Color Css para identificar el motivo de la reserva
        /// </summary>
        public string Color { get; set; }
    }



}
