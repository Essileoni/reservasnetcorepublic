﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Reservas.Entidades.Sucursal

{
    public class HistorialReserva
    {
        public long Id { get; set; }
        /// <summary>
        /// Json que contiene solo los cambios realizados del objeto y el usuario que los realizo. 
        /// </summary>
        public string ReservaJson { get; set; }      
        /// <summary>
        /// Momento en el que se realizó el cambio.
        /// </summary>
        public string FechaHoraModificacion { get; set; }
    }
}
