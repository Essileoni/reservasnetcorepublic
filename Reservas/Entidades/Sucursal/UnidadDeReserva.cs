﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Reservas.Entidades.Sucursal
{
    /// <summary>
    /// Unidad Mínima con los datos de la reserva
    /// </summary>
    public class UnidadDeReserva
    {
        public int Id { get; set; }
        public IEnumerable<PrecioPorHora> PreciosPorHora { get; set; }
        /// <summary>
        /// Leve descripcion representativa que identifique visualmente a las unidad
        /// </summary>
        [MaxLength(128)]
        public string Descripcion { get; set; }
        /// <summary>
        /// True: Unidad Disponible. False: Unidad Suspendida
        /// </summary>
        public bool Activo { get; set; }
        /// <summary>
        /// Describe el tipo de unidad que se reserva. Cancha, Consultorio, Oficina. Etc.
        /// </summary>
        [MaxLength(128)]
        public string Tipo { get; set; }
    }

    /// <summary>
    /// Registro de un precio para un rango de tiempo. 
    /// </summary>
    [Table("PrecioPorHora")]
    public class PrecioPorHora
    {
        public int Id { get; set; }
        /// <summary>
        /// Cota inicial con su valor incluido
        /// </summary>
        public decimal HoraInicio { get; set; }
        /// <summary>
        /// Cota final sin incluir
        /// </summary>
        public decimal HoraFin { get; set; }
        /// <summary>
        /// Hora inicial: Cantidad de horas desde la 00:00 del dia indicado. Ej: 18,5 = 18:30; Ej2: 27,25 = 3:00 pasada las 00:00 del día siguiente.
        /// </summary>
        public decimal HoraInicioAdaptada { get; set; }
        /// <summary>
        /// Hora final: Cantidad de horas desde la 00:00 del dia indicado. Ej: 18,5 = 18:30; Ej2: 27,25 = 3:00 pasada las 00:00 del día siguiente.
        /// </summary>
        public decimal HoraFinAdaptada { get; set; }
        /// <summary>
        /// Fecha en caso de eliminación.
        /// </summary>
        public DateTime? Eliminado { get; set; }
        /// <summary>
        /// Posibilidades - 'lunes','martes','miercoles','jueves','viernes','sabado','domingo','feriado'
        /// </summary>
        [RegularExpression("(lunes|martes|miercoles|jueves|viernes|sabado|domingo|feriado)", ErrorMessage = "Solo puede ingresar lunes, martes, miercoles, jueves, viernes, sabado, domingo, feriado")]
        public string DiaSemana { get; set; }
        public decimal PrecioUnitarioPorHora { get; set; }

        //public UnidadDeReserva UnidadDeReserva { get; set; }
        public int UnidadDeReservaId { get; set; }

    }
}

