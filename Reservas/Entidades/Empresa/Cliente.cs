﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Reservas.Entidades.Empresa
{
    public class Cliente
    {
        public int Id { get; set; }
        /// <summary>
        /// Nombre y/o Apellido
        /// </summary>
        [MaxLength(256)]
        public string NombreYApellido { get; set; }
        [MaxLength(128)]
        public string Email { get; set; }
        /// <summary>
        /// Estado del cliente. [eliminado]
        /// </summary>
        public string Estado { get; set; }
        /// <summary>
        /// Si no es un celular y es un telefono particular sin 15 delante.
        /// </summary>
        public bool EsTelefonoFijo { get; set; }
        /// <summary>
        /// Número de telefono o celular sin el 15
        /// </summary>
        [MaxLength(32)]
        public string TelefonoNumero { get; set; }
        /// <summary>
        /// Código del area sin el 0
        /// </summary>
        [MaxLength(16)]
        public string TelefonoArea { get; set; }
        [MaxLength(32)]
        public string Documento{ get; set; }
        [MaxLength(32)]
        public string TipoDocumento { get; set; }
        /// <summary>
        /// Infracciones por faltar a la reserva o cancelar sin anticipación.
        /// </summary>
        public int InfraccionesGraves { get; set; }
        /// <summary>
        /// Infracciones por cancelar la reserva con poca anticipación
        /// </summary>
        public int Infracciones { get; set; }
        /// <summary>
        /// Cantidad de veces que asistió la reserva (Y pagó por ella)
        /// </summary>
        public int Cumplimientos { get; set; }
        /// <summary>
        /// Saldo a favor o en contra que le quedó al cliente despues del pago de una reserva.
        /// </summary>
        public decimal Saldo { get; set; }

    }

}
