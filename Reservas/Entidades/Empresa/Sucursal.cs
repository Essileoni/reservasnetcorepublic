﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Reservas.Entidades.Empresa
{
    [Table("Sucursal")]
    public class Sucursal
    {
        public int Id { get; set; }
        /// <summary>
        /// Nombre de la sucursal, editable
        /// </summary>
        [MaxLength(128)]
        public string Nombre { get; set; }
        [MaxLength(256)]
        public string Calle { get; set; }
        [MaxLength(64)]
        public string Numero { get; set; }
        [MaxLength(2048)]
        public string ComoLlegar { get; set; }
        /// <summary>
        /// Telefonos separados por una pipe/pipa/barra |
        /// </summary>
        public string Telefonos { get; set; }
        [MaxLength(256)]
        public string Coordenadas { get; set; }
        /// <summary>
        /// Nombre de la base de datos que va a contener los datos de la sucursal. 
        /// Basado en el Nombre de la sucursal durante su creación (Camelcase Alfabetico Sin acentos ni ñ)
        /// </summary>
        //[MaxLength(128)]
        //public string DataBaseCatalogName { get; set; }

        public string DBConnectionString { get; set; }

        public Empresa Empresa { get; set; }
    }
}
