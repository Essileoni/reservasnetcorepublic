﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Reservas.Entidades.Empresa
{
    public class Empresa
    {
        public int Id { get; set; }
        [MaxLength(256)]
        /// <summary>
        /// Nombre y/o Apellido
        /// </summary>
        public string NombreCompleto { get; set; }
        [MaxLength(64)]
        public string CUIT { get; set; }
        [MaxLength(32)]
        public string Documento { get; set; }
        [MaxLength(32)]
        public string TipoDocumento { get; set; }
        /// <summary>
        /// Actividad comercial que realizan las empresas.
        /// </summary>
        public string Actividad { get; set; }

        public IEnumerable<Cliente> Clientes { get; set; }
        public IEnumerable<Sucursal> Sucursales { get; set; }
    }
}
