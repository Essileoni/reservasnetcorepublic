﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Reservas.Migrations.SucursalDb
{
    public partial class PrecioPorHora : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MotivosReserva",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Descripcion = table.Column<string>(nullable: true),
                    EsReservaCustom = table.Column<bool>(nullable: false),
                    TiempoEstandar = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MotivosReserva", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UnidadesDeReserva",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Descripcion = table.Column<string>(maxLength: 128, nullable: true),
                    Activo = table.Column<bool>(nullable: false),
                    Tipo = table.Column<string>(maxLength: 128, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UnidadesDeReserva", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "PrecioPorHora",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    HoraInicio = table.Column<decimal>(nullable: false),
                    HoraFin = table.Column<decimal>(nullable: false),
                    DiaSemana = table.Column<string>(nullable: true),
                    PrecioUnitarioPorHora = table.Column<decimal>(nullable: false),
                    UnidadDeReservaId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PrecioPorHora", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PrecioPorHora_UnidadesDeReserva_UnidadDeReservaId",
                        column: x => x.UnidadDeReservaId,
                        principalTable: "UnidadesDeReserva",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Reservas",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MotivoId = table.Column<int>(nullable: true),
                    FechaHoraCreacion = table.Column<DateTime>(nullable: false),
                    FechaHoraComienzo = table.Column<DateTime>(nullable: false),
                    FechaHoraFin = table.Column<DateTime>(nullable: false),
                    FechaHoraComienzoReal = table.Column<DateTime>(nullable: false),
                    FechaHoraFinReal = table.Column<DateTime>(nullable: false),
                    Senia = table.Column<decimal>(nullable: false),
                    DeudaTotal = table.Column<decimal>(nullable: false),
                    Descuentos = table.Column<decimal>(nullable: false),
                    ImporteUnidadReservada = table.Column<decimal>(nullable: false),
                    TotalReserva = table.Column<decimal>(nullable: false),
                    Comentario = table.Column<string>(maxLength: 2048, nullable: true),
                    UsuarioId = table.Column<string>(nullable: true),
                    UnidadDeReservaId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reservas", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Reservas_MotivosReserva_MotivoId",
                        column: x => x.MotivoId,
                        principalTable: "MotivosReserva",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Reservas_UnidadesDeReserva_UnidadDeReservaId",
                        column: x => x.UnidadDeReservaId,
                        principalTable: "UnidadesDeReserva",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ClientesReserva",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EstadoCumplimiento = table.Column<string>(nullable: true),
                    EsReservaFija = table.Column<bool>(nullable: false),
                    ClienteId = table.Column<int>(nullable: false),
                    ReservaId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClientesReserva", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ClientesReserva_Reservas_ReservaId",
                        column: x => x.ReservaId,
                        principalTable: "Reservas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HistorialesReserva",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ReservaJson = table.Column<string>(nullable: true),
                    FechaHoraModificacion = table.Column<string>(nullable: true),
                    ReservaId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HistorialesReserva", x => x.Id);
                    table.ForeignKey(
                        name: "FK_HistorialesReserva_Reservas_ReservaId",
                        column: x => x.ReservaId,
                        principalTable: "Reservas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClientesReserva_ReservaId",
                table: "ClientesReserva",
                column: "ReservaId");

            migrationBuilder.CreateIndex(
                name: "IX_HistorialesReserva_ReservaId",
                table: "HistorialesReserva",
                column: "ReservaId");

            migrationBuilder.CreateIndex(
                name: "IX_PrecioPorHora_UnidadDeReservaId",
                table: "PrecioPorHora",
                column: "UnidadDeReservaId");

            migrationBuilder.CreateIndex(
                name: "IX_Reservas_MotivoId",
                table: "Reservas",
                column: "MotivoId");

            migrationBuilder.CreateIndex(
                name: "IX_Reservas_UnidadDeReservaId",
                table: "Reservas",
                column: "UnidadDeReservaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClientesReserva");

            migrationBuilder.DropTable(
                name: "HistorialesReserva");

            migrationBuilder.DropTable(
                name: "PrecioPorHora");

            migrationBuilder.DropTable(
                name: "Reservas");

            migrationBuilder.DropTable(
                name: "MotivosReserva");

            migrationBuilder.DropTable(
                name: "UnidadesDeReserva");
        }
    }
}
