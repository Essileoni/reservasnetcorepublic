﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Reservas.Migrations
{
    public partial class EmpresaSucursalCliente : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Empresas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NombreCompleto = table.Column<string>(maxLength: 256, nullable: true),
                    CUIT = table.Column<string>(maxLength: 64, nullable: true),
                    Documento = table.Column<string>(maxLength: 32, nullable: true),
                    TipoDocumento = table.Column<string>(maxLength: 32, nullable: true),
                    Actividad = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Empresas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Clientes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NombreCompleto = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 128, nullable: true),
                    Telefono = table.Column<string>(maxLength: 64, nullable: true),
                    Documento = table.Column<string>(maxLength: 32, nullable: true),
                    TipoDocumento = table.Column<string>(maxLength: 32, nullable: true),
                    InfraccionesGraves = table.Column<int>(nullable: false),
                    Infracciones = table.Column<int>(nullable: false),
                    Cumplimientos = table.Column<int>(nullable: false),
                    Saldo = table.Column<decimal>(nullable: false),
                    EmpresaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clientes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Clientes_Empresas_EmpresaId",
                        column: x => x.EmpresaId,
                        principalTable: "Empresas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Sucursal",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Calle = table.Column<string>(maxLength: 256, nullable: true),
                    Numero = table.Column<string>(maxLength: 64, nullable: true),
                    ComoLlegar = table.Column<string>(maxLength: 2048, nullable: true),
                    Telefonos = table.Column<string>(nullable: true),
                    Coordenadas = table.Column<string>(maxLength: 256, nullable: true),
                    ConnectionString = table.Column<string>(maxLength: 2048, nullable: true),
                    EmpresaId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sucursal", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sucursal_Empresas_EmpresaId",
                        column: x => x.EmpresaId,
                        principalTable: "Empresas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Clientes_EmpresaId",
                table: "Clientes",
                column: "EmpresaId");

            migrationBuilder.CreateIndex(
                name: "IX_Sucursal_EmpresaId",
                table: "Sucursal",
                column: "EmpresaId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Clientes");

            migrationBuilder.DropTable(
                name: "Sucursal");

            migrationBuilder.DropTable(
                name: "Empresas");
        }
    }
}
