﻿using Microsoft.EntityFrameworkCore;
using Reservas.Entidades.Empresa;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reservas.DbContexts
{
    public class EmpresaDbContext : CustomDbContext
    {
        
        public EmpresaDbContext(DbContextOptions<EmpresaDbContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Sucursal> Sucursales { get; set; }

    }
}