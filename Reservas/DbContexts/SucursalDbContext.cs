﻿using Microsoft.EntityFrameworkCore;
using Reservas.Entidades.Sucursal;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reservas.DbContexts
{
    public class SucursalDbContext : CustomDbContext
    {
        public SucursalDbContext(DbContextOptions<SucursalDbContext> options)
            : base(options)
        { }

        public DbSet<HistorialReserva> HistorialesReserva { get; set; }
        public DbSet<Reserva> Reservas { get; set; }
        public DbSet<UnidadDeReserva> UnidadesDeReserva { get; set; }
        public DbSet<ClienteReserva> ClientesReserva { get; set; }
        public DbSet<MotivoReserva> MotivosReserva { get; set; }
        public DbSet<PrecioPorHora> PreciosPorHora { get; set; }



    }

}