﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Reservas.DbContexts
{
    /// <summary>
    /// Herencia que extiende el funcionamiento del DbContext
    /// </summary>
    public class CustomDbContext : DbContext
    {
        // Fué Necesario Sobreescribir el constructor.
        public CustomDbContext(DbContextOptions options)
            : base(options)
        { }

        /// <summary>
        /// Esta propiedad nos permite saber si un contexto esta disposeado.
        /// </summary>
        public bool IsDisposed { get; set; } = false;

        /// <summary>
        /// Este metodo fue sobreescrito para extender la funcionalidad de la clase
        /// </summary>
        public override void Dispose()
        {
            IsDisposed = true;
            base.Dispose();
        }

    }
}
