﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;
using Reservas.DbContexts;
using Microsoft.EntityFrameworkCore;
using Common.Extensions;

namespace Reservas.Data
{
    /// <summary>
    /// Permite la migración por comandos de PackageManagerConsole
    /// </summary>
    public class SucursalDbContextFactory : IDesignTimeDbContextFactory<SucursalDbContext>
    {
        //private static SucursalDbContext _dbContext;

        public SucursalDbContext CreateDbContext(string[] args)
        {
            var dbContext = new SucursalDbContext(new DbContextOptionsBuilder<SucursalDbContext>().UseSqlServer(
               new ConfigurationBuilder()
                   .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), $"appsettings.json"))
                   .Build()
                   .GetConnectionString("EmpresaDatabaseConnection")
               ).Options);

            dbContext.Database.Migrate();
            return dbContext;
        }

        public static SucursalDbContext CreateOnlyDbContext(string connectionString)
        {
            //Las siguientes lineas se comentaron debido a que devuelve una instancia que se disposea en algún momento durante su uso.
            //if (_dbContext != null && !_dbContext.IsDisposed)
            //{
            //    return _dbContext;
            //}

            //string connectionString = new ConfigurationBuilder()
            //    .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), $"appsettings.json"))
            //    .Build()
            //    .GetConnectionString("SucursalDatabaseConnection"); // <- Requiere un nombre de base de datos

            //StringBuilder sbConnectionString = new StringBuilder();
            //sbConnectionString.AppendFormat(connectionString, dataBaseCatalogName);

            var dbContext = new SucursalDbContext(
                new DbContextOptionsBuilder<SucursalDbContext>()
                    .UseSqlServer(connectionString).Options
            );

            //Las siguientes lineas se comentaron debido a que devuelve una instancia que se disposea en algún momento durante su uso.
            //_dbContext = dbContext;

            return dbContext;
        }
    }
}
