﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System.IO;
using Reservas.DbContexts;
using Microsoft.EntityFrameworkCore;
using Common.Extensions;

namespace Reservas.Data
{
    /// <summary>
    /// Permite la migración por comandos de PackageManagerConsole
    /// </summary>
    public class EmpresaDbContextFactory : IDesignTimeDbContextFactory<EmpresaDbContext>
    {
        private static EmpresaDbContext _dbContext;

        public EmpresaDbContext CreateDbContext(string[] args)
        {
            var dbContext = new EmpresaDbContext(new DbContextOptionsBuilder<EmpresaDbContext>().UseSqlServer(
               new ConfigurationBuilder()
                   .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), $"appsettings.json"))
                   .Build()
                   .GetConnectionString("EmpresaDatabaseConnection")
               ).Options);

            dbContext.Database.Migrate();
            return dbContext;
        }

        public static EmpresaDbContext CreateOnlyDbContext()
        {
            if (_dbContext != null && !_dbContext.IsDisposed)
            {
                return _dbContext;
            }

            var dbContext = new EmpresaDbContext(new DbContextOptionsBuilder<EmpresaDbContext>().UseSqlServer(
               new ConfigurationBuilder()
                   .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), $"appsettings.json"))
                   .Build()
                   .GetConnectionString("EmpresaDatabaseConnection")
               ).Options);

            _dbContext = dbContext;

            return dbContext;
        }
    }
}
